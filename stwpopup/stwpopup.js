/*
        stwpopup v1.0
        Released under a creative commons Attribution-ShareAlike 2.5 license (http://creativecommons.org/licenses/by-sa/2.5/)

        You are free:
        * to copy, distribute, display, and perform the work
        * to make derivative works
        * to make commercial use of the work

        Under the following conditions:
                by Attribution.
                --------------
                You must attribute the work in the manner specified by the author or licensor.
                Share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.

        * For any reuse or distribution, you must make clear to others the license terms of this work.
        * Any of these conditions can be waived if you get permission from the copyright holder.

        References:      
		Brian: http://www.frequency-decoder.com/2006/10/25/link-preview-v2
        Dustan Diaz: http://www.dustindiaz.com/sweet-titles-finalized
        Arc90: http://lab.arc90.com/2006/07/link_thumbnail.php
*/
/*
   CONFIGURATION START - Fill in the variables for this code to work properly
   
   userid = Your User Key.  You can find this by Logging in -> Find "Your Secret Access Key: <This is what you need>" at the bottom of the page.
   stwaccesskeyid = Your STW Access key id.  You can find this by Logging in -> Find "Your Access Key ID: <This is what you need>" at the bottom of the page.
   
   // 2nd stwaccesskeyid: 047923934eb9b57
*/
var stwaccesskeyid = "94255b12e6920d2";
var size = "xlg";
// sm=120x90, lg=200x150, xlg=320x240

var stwpopup = {
        x:0,
        y:0,
        obj:{},
        img:null,
        lnk:null,
		ind:null,
        timer:null,
		containerid:null,
        opacityTimer:null,
        errorTimer:null,
        hidden:true,
        linkPool: {},							// an object literal;
        baseURI: "clientscript/stwpopup/",
        imageCache: [],							// an array literal;
        init: function() {
				var lnks = null;
				if (stwpopup.containerid == null)
					lnks = document.getElementsByTagName('a');
				else
					lnks = document.getElementById(stwpopup.containerid).getElementsByTagName('a');
                var i = lnks.length || 0;
                var cnt = 0;			// NOTE: must add EventListeners to the links even for ajax calls;
				while(i--) {
                        if(lnks[i].className && lnks[i].className.indexOf('webshot') != -1) {
                                stwpopup.addEvent(lnks[i], ["focus", "mouseover"], stwpopup.initThumb);
                                stwpopup.addEvent(lnks[i], ["blur",  "mouseout"],  stwpopup.hideThumb);
                                stwpopup.linkPool[lnks[i].href] = cnt++;
						}
                }
                if(cnt) {
                        stwpopup.preloadImages();
                        stwpopup.obj = document.createElement('div');
                        stwpopup.ind = document.createElement('div');
                        stwpopup.ind.className= "imageLoaded";
                        
						stwpopup.img = document.createElement('img');
                        stwpopup.img.alt = "preview";
                        stwpopup.img.id = "fdImage";
                        stwpopup.addEvent(stwpopup.img, ["load"], stwpopup.imageLoaded);
                        stwpopup.addEvent(stwpopup.img, ["error"], stwpopup.imageError);
                        
						stwpopup.obj.id = "fdImageThumb";
                        stwpopup.obj.style.visibility = "hidden";
                        stwpopup.obj.appendChild(stwpopup.ind);
                        stwpopup.obj.appendChild(stwpopup.img);
                        stwpopup.addEvent(stwpopup.obj, ["mouseout"], stwpopup.hideThumb);
                        document.getElementsByTagName('body')[0].appendChild(stwpopup.obj);
                }
        },
        preloadImages: function() {
                var imgList = ["lt.png", "lb.png", "rt.png", "rb.png", "error.gif", "loading.gif"];
                var imgObj  = document.createElement('img');
				
                for(var i = 0; i < imgList.length; i++) {
                        stwpopup.imageCache[i] = imgObj.cloneNode(false);
                        stwpopup.imageCache[i].src = stwpopup.baseURI + imgList[i];
                }
        },
        imageLoaded: function() {
                if(stwpopup.errorTimer) clearTimeout(stwpopup.errorTimer);
                if(!stwpopup.hidden) stwpopup.img.style.visibility = "visible";
                stwpopup.ind.className= "imageLoaded";
                stwpopup.ind.style.visibility = "hidden";
        },
        imageError: function(e) {
                if(stwpopup.errorTimer) clearTimeout(stwpopup.errorTimer);
                stwpopup.ind.className= "imageError";
                stwpopup.errorTimer = window.setTimeout("stwpopup.hideThumb()",2000);
        },
        initThumb: function(e) {
                e = e || window.event;

                stwpopup.lnk       = this;
                var positionClass  = "left";

                var heightIndent;
                var indentX = 0;
                var indentY = 0;
                var trueBody = (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;

                if(e.type.toLowerCase().indexOf('mouseover') != -1) {
                        if (document.captureEvents) {
                                stwpopup.x = e.pageX;
                                stwpopup.y = e.pageY;
                        } else if ( window.event.clientX ) {
                                stwpopup.x = window.event.clientX+trueBody.scrollLeft;
                                stwpopup.y = window.event.clientY+trueBody.scrollTop;
                        }
                        indentX = 10;
                        heightIndent = parseInt(stwpopup.y-(stwpopup.obj.offsetHeight))+'px';
                } else {
                        var obj = this;
                        var curleft = curtop = 0;
                        if (obj.offsetParent) {
                                curleft = obj.offsetLeft;
                                curtop = obj.offsetTop;
                                while (obj = obj.offsetParent) {
                                        curleft += obj.offsetLeft;
                                        curtop += obj.offsetTop;
                                }
                        }
                        curtop += this.offsetHeight;

                        stwpopup.x = curleft;
                        stwpopup.y = curtop;

                        heightIndent = parseInt(stwpopup.y-(stwpopup.obj.offsetHeight)-this.offsetHeight)+'px';
                }
                
                if ( parseInt(trueBody.clientWidth+trueBody.scrollLeft) < parseInt(stwpopup.obj.offsetWidth+stwpopup.x) + indentX) {
                        stwpopup.obj.style.left = parseInt(stwpopup.x-(stwpopup.obj.offsetWidth+indentX))+'px';
                        positionClass = "right";
                } else {
                        stwpopup.obj.style.left = (stwpopup.x+indentX)+'px';
                }
                if ( parseInt(trueBody.clientHeight+trueBody.scrollTop) < parseInt(stwpopup.obj.offsetHeight+stwpopup.y) + indentY ) {
                        stwpopup.obj.style.top = heightIndent;
                        positionClass += "Top";
                } else {
                        stwpopup.obj.style.top = (stwpopup.y + indentY)+'px';
                        positionClass += "Bottom";
                }

                stwpopup.obj.className = positionClass;
                stwpopup.timer = window.setTimeout("stwpopup.showThumb()",500);
        },
        showThumb: function(e) {
                stwpopup.hidden = false;
                stwpopup.obj.style.visibility = stwpopup.ind.style.visibility = 'visible';
                stwpopup.obj.style.opacity = stwpopup.ind.style.opacity = '0';
                stwpopup.img.style.visibility = "hidden";
                
                var addy = String(stwpopup.lnk.href);
                stwpopup.errorTimer = window.setTimeout("stwpopup.imageError()",15000);
				stwpopup.img.src = 'http://www.shrinktheweb.com/xino.php?embed=1' +'&STWAccessKeyId='+ stwaccesskeyid +'&rndm='+ parseInt(stwpopup.linkPool[stwpopup.lnk.href]) +'&Size='+ size +'&Url='+ encodeURIComponent(addy);

                /*@cc_on@*/
                /*@if(@_win32)
                return;
                /*@end@*/
               stwpopup.fade(10);
        },
        hideThumb: function(e) {
                // Don't mouseout if over the bubble
                e = e || window.event;

                // Check if mouse(over|out) are still within the same parent element
                if(e.type == "mouseout") {
                        var elem = e.relatedTarget || e.toElement;
                        if(elem.id && elem.id.indexOf('fdImage') != -1) return false;
                }

                stwpopup.hidden = true;
                if(stwpopup.timer) clearTimeout(stwpopup.timer);
                if(stwpopup.errorTimer) clearTimeout(stwpopup.errorTimer);
                if(stwpopup.opacityTimer) clearTimeout(stwpopup.opacityTimer);
                stwpopup.obj.style.visibility = 'hidden';
                stwpopup.ind.style.visibility = 'hidden';
                stwpopup.img.style.visibility = 'hidden';
                stwpopup.ind.className= "imageLoaded";
        },
        fade: function(opac) {
                var passed  = parseInt(opac);
                var newOpac = parseInt(passed+10);
                if ( newOpac < 90 ) {
                        stwpopup.obj.style.opacity = stwpopup.ind.style.opacity = '.'+newOpac;
                        stwpopup.opacityTimer = window.setTimeout("stwpopup.fade('"+newOpac+"')",20);
                } else {
                        stwpopup.obj.style.opacity = stwpopup.ind.style.opacity = '1';
                }
        },
        addEvent: function( obj, types, fn ) {
                var type;
                for(var i = 0; i < types.length; i++) {
                        type = types[i];
                        if ( obj.attachEvent ) {
                                obj['e'+type+fn] = fn;
                                obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
                                obj.attachEvent( 'on'+type, obj[type+fn] );
                        } else obj.addEventListener( type, fn, false );
                }
        }
}

stwpopup.addEvent(window, ['load'], stwpopup.init);
