/*
        tippopup v1.0
        Released under a creative commons Attribution-ShareAlike 2.5 license (http://creativecommons.org/licenses/by-sa/2.5/)

        You are free:
        * to copy, distribute, display, and perform the work
        * to make derivative works
        * to make commercial use of the work

        Under the following conditions:
                by Attribution.
                --------------
                You must attribute the work in the manner specified by the author or licensor.
                Share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.

        * For any reuse or distribution, you must make clear to others the license terms of this work.
        * Any of these conditions can be waived if you get permission from the copyright holder.

        References:
		Brian: http://www.frequency-decoder.com/2006/10/25/link-preview-v2
        Dustan Diaz: http://www.dustindiaz.com/sweet-titles-finalized
        Arc90: http://lab.arc90.com/2006/07/link_thumbnail.php

		// IMPORTANT NOTES:
		1. an image used ONLY FOR tooltips must be loaded on focus/ mouseover since only an onload event can cause IE7 to show the tooltips
		2. IE7 && FF3 have different methods to attach event handlers and to receive event objects in the functions (use the custom addEvent function)
		3. the hasAttribute function DOES NOT work in IE7!!!!!!!!!!!!
*/

var isLinkedTip = false;
var tippopup = {
        x:0,
        y:0,
        obj:{},
        img:null,
        abbr:null,
		div1:null,
        timer:null,
		containerid:null,
        opacityTimer:null,
        hidden:true,
        baseURI: "clientscript/tippopup/",
        imageCache: [],
        init: function() {
				var lnks = null;
				if (tippopup.containerid == null)
					lnks = document.getElementsByTagName(isLinkedTip ? 'a' : 'abbr');
                else
					lnks = document.getElementById(tippopup.containerid).getElementsByTagName(isLinkedTip ? 'a' : 'abbr');
				var i = lnks.length || 0;
                while(i--) {
                        if(lnks[i].className && lnks[i].className.indexOf('ttip') != -1) {
							if (lnks[i].title) {
								tippopup.addEvent(lnks[i], ["mouseover"], tippopup.initThumb);
								tippopup.addEvent(lnks[i], ["mouseout"],  tippopup.hideThumb);
								
								lnks[i].tooltip = '  ' + lnks[i].title;
								lnks[i].removeAttribute('title');
							}
						}	// must check if 'title' exists since it's removed when init() is called by onLoad and ajax editing/ posting will call init() again; the browser's tooltip covers up (completely for long text) the balloon's text, so must remove the link's title;
                }
                if(lnks.length) {
                        tippopup.preloadImages();
                        tippopup.obj = document.createElement('div');
                        tippopup.obj.style.visibility = "hidden";
                        tippopup.obj.id = isLinkedTip ? "fdTipbox3" : "fdTipbox2";
                        
						tippopup.img = document.createElement('img');
                        tippopup.addEvent(tippopup.img, ["load"], tippopup.imageLoaded);
                        tippopup.addEvent(tippopup.img, ["error"], tippopup.imageError);
						
						// append img THEN text node since functions call tippopup.div1.lastChild
                        tippopup.div1 = document.createElement('div');
                        tippopup.div1.appendChild(tippopup.img);
						tippopup.div1.appendChild(document.createTextNode('Preview:'));
                        tippopup.div1.id = isLinkedTip ? "fdTiptext3" : "fdTiptext2";
						
						tippopup.obj.appendChild(tippopup.div1);
                        tippopup.addEvent(tippopup.obj, ["mouseout"], tippopup.hideThumb);
                        document.getElementsByTagName('body')[0].appendChild(tippopup.obj);
                }
        },
        preloadImages: function() {
                var imgList = ["lt2.png", "lb2.png", "rt2.png", "rb2.png", "lt3.png", "lb3.png", "rt3.png", "rb3.png"];
                var imgObj  = document.createElement('img');
				
                for(var i = 0; i < imgList.length; i++) {
                        tippopup.imageCache[i] = imgObj.cloneNode(false);
                        tippopup.imageCache[i].src = tippopup.baseURI + imgList[i];
                }
        },
        imageLoaded: function() {
			if (!tippopup.abbr)
                tippopup.div1.style.visibility = "hidden";
			else if (tippopup.abbr.tooltip && !tippopup.hidden) {
				tippopup.div1.lastChild.nodeValue = tippopup.abbr.tooltip;
				tippopup.img.style.visibility = "visible";
			}
		},
		imageError: function() {
			if (!tippopup.abbr)
                tippopup.div1.style.visibility = "hidden";
			else if (tippopup.abbr.tooltip && !tippopup.hidden)
				tippopup.div1.lastChild.nodeValue = tippopup.abbr.tooltip;
			// show the tooltip but DON'T show the image (tippopup.img)
		},
        showThumb: function(e) {
                e = e || window.event;

                tippopup.hidden = false;
                tippopup.obj.style.visibility = tippopup.div1.style.visibility = 'visible';
                tippopup.obj.style.opacity = tippopup.div1.style.opacity = '0';
                tippopup.img.style.visibility = "hidden";
                tippopup.img.src = 'http://localhost/forum/images/misc/openbook.png';
				
                /*@cc_on@*/
                /*@if(@_win32)
                return;
                /*@end@*/
                tippopup.fade(10);
        },
        hideThumb: function(e) {
                e = e || window.event;

                // Check if mouse(over|out) are still within the same parent element
                if(e.type == "mouseout") {
                        var elem = e.relatedTarget || e.toElement;
                        if (elem.id && elem.id.indexOf('fdTipbox') != -1) return false;
                }
                tippopup.hidden = true;
                if (tippopup.timer) clearTimeout(tippopup.timer);
                if (tippopup.opacityTimer) clearTimeout(tippopup.opacityTimer);
                tippopup.obj.style.visibility = 'hidden';
                tippopup.div1.style.visibility = 'hidden';
                tippopup.img.style.visibility = 'hidden';
				if (tippopup.abbr.tooltip) tippopup.div1.lastChild.nodeValue = '';
        },
        initThumb: function(e) {
                e = e || window.event;
                tippopup.abbr = this;
                var position;
                var indentH = indentX = indentY = 0;
				
                var trueBody = (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;
                if(e.type.toLowerCase().indexOf('mouseover') != -1) {
                        if (document.captureEvents) {
                            tippopup.x = e.pageX;
                            tippopup.y = e.pageY;
                        } else if ( window.event.clientX ) {
                            tippopup.x = window.event.clientX+trueBody.scrollLeft;
                            tippopup.y = window.event.clientY+trueBody.scrollTop;
                        }
                        indentX = 10;
                        indentH = parseInt(tippopup.y-(tippopup.obj.offsetHeight))+'px';
                } else {
                        var obj = this;
                        var curleft = curtop = 0;
                        if (obj.offsetParent) {
                            curleft = obj.offsetLeft;
                            curtop = obj.offsetTop;
                            while (obj = obj.offsetParent) {
                                curleft += obj.offsetLeft;
                                curtop += obj.offsetTop;
                            }
                        }
                        curtop += this.offsetHeight;
                        tippopup.x = curleft;
                        tippopup.y = curtop;
                        indentH = parseInt(tippopup.y-(tippopup.obj.offsetHeight)-this.offsetHeight)+'px';
                }
                
                if ( parseInt(trueBody.clientWidth+trueBody.scrollLeft) < parseInt(tippopup.obj.offsetWidth+tippopup.x) + indentX ) {
                        tippopup.obj.style.left = parseInt(tippopup.x-(tippopup.obj.offsetWidth+indentX))+'px';
                        position = "right";
                } else {
                        tippopup.obj.style.left = (tippopup.x+indentX)+'px';
                		position = "left";
                }
                if ( parseInt(trueBody.clientHeight+trueBody.scrollTop) < parseInt(tippopup.obj.offsetHeight+tippopup.y) + indentY ) {
                        tippopup.obj.style.top = indentH;
                        position += (isLinkedTip ? 'Top3' : 'Top2');
                } else {
                        tippopup.obj.style.top = (tippopup.y + indentY)+'px';
                        position += (isLinkedTip ? 'Bottom3' : 'Bottom2');
                }
                tippopup.obj.className = position;
                tippopup.timer = window.setTimeout("tippopup.showThumb()",500);
        },
        fade: function(opac) {
                var passed  = parseInt(opac);
                var newOpac = parseInt(passed+10);
                if ( newOpac < 90 ) {
                        tippopup.obj.style.opacity = tippopup.div1.style.opacity = '.'+newOpac;
                        tippopup.opacityTimer = window.setTimeout("tippopup.fade('"+newOpac+"')",20);
                } else {
                        tippopup.obj.style.opacity = tippopup.div1.style.opacity = '1';
                }
        },
        addEvent: function( obj, types, fn ) {
                var type;
                for(var i = 0; i < types.length; i++) {
                        type = types[i];
                        if ( obj.attachEvent ) {
                                obj['e'+type+fn] = fn;
                                obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
                                obj.attachEvent( 'on'+type, obj[type+fn] );
                        } else obj.addEventListener( type, fn, false );
                }
        }
}

tippopup.addEvent(window, ['load'], tippopup.init);
