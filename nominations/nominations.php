<?php

// nomination_tpid contains the nomination threadid if entry is nominated, or the pollid if entry won;
// nomination_rank == -1 if the entry is nominated, nomination_rank > 0 if the entyr won, else it's 0;
// topics can not be nominated (-1) if it already won (lines 235-37), but vip's can delete their awards;

// set php environment
error_reporting(E_ALL & ~E_NOTICE);

// get special phrase groups
$phrasegroups = array('poll', 'cron');

// get special data templates from the datastore
$specialtemplates = array('smiliecache', 'bbcodecache');

// pre-cache templates used by all actions
$globaltemplates = array('nomination_icon');

// pre-cache templates used by specific actions
$actiontemplates = array();

// ######################### REQUIRE BACK-END ############################
require_once('./global.php');
require_once(DIR . '/includes/class_bbcode_alt.php');
require_once(DIR . '/includes/local_links_init.php');			// required for THIS_TABLE CONSTANT;

// -------------------------------------		INCLUDED FOR TESTING		---------------------------
require_once(DIR . '/includes/functions_cron.php');
$nextitem = array();
$nextitem['loglevel'] = 1;
$nextitem['varname'] = 'nomination';

$now = time();
$day = date("j");
$day_of_week = date("w");
$days_in_month = date("t");
$createNewThread = 0;
$week_number = '';		// leave blank for bi-monthly and monthly contests;

$contesttime = $vbulletin->options['nomination_contest_time'];
if ($contesttime == 1) {
	$week_number = strftime("%V", $now) .', ';
	$month_name = strftime("%B", $now);
	$year_number = strftime("%Y", $now);
	$contest_period = 'weekly';
	if ($day_of_week == 1)
		$createNewThread = 1;
}
elseif ($contesttime == 2) {
	$twoWeeks = $now + (15 * 24 * 60 * 60);
	$month_name = strftime("%B", $twoWeeks);
	$year_number = strftime("%Y", $twoWeeks);
	$contest_period = 'bi-monthly';
	if ($day == 1 || $day == 16)
		$createNewThread = 1;
}
else {
	$month_name = strftime("%B", $now);
	$year_number = strftime("%Y", $now);
	$contest_period = 'monthly';
	if ($day == 1)
		$createNewThread = 1;
}

$nomination_forum = $vbulletin->options['nomination_receiving_forum'];
$foruminfo = fetch_foruminfo($nomination_forum);
if (!$foruminfo) {
	log_cron_action($vbphrase['nomination_noforumid'], $nextitem);
	echo $vbphrase['nomination_noforumid'];
	exit;
}
$threadOwnerID = $vbulletin->options['nomination_thread_ownerid'];
$winner_reppoints = $vbulletin->options['nomination_winner_reppoints'];
$userinfo = fetch_userinfo($threadOwnerID);
if (!$userinfo)
	$threadOwnerID = 1;
if ($winner_reppoints)
	$winner_reppoints = explode(',', $winner_reppoints);

// nomination_thread fields: postid, threadid, pollid, polltype, ismanual;
$nominated_thread = $vbulletin->db->query_read("SELECT * FROM ". TABLE_PREFIX ."nomination_thread WHERE pollid > 0");
$nominated_topics = array();
while ($ntopicthread = $vbulletin->db->fetch_array($nominated_thread)) {
	$nominated_topics[] = $ntopicthread;
}
// 		----------------	TO TEST: comment out the polltype not wanted below		------------------------
if (empty($nominated_topics)) {
	$nominated_topics[] = array('polltype' => 'album');
	$nominated_topics[] = array('polltype' => 'event');
	$nominated_topics[] = array('polltype' => 'group');
	$nominated_topics[] = array('polltype' => 'thread');
	$nominated_topics[] = array('polltype' => 'user');
	$nominated_topics[] = array('polltype' => 'media');
	$createNewThread = 1;			// initial installation;
}	// to simplify code and not record calendarid, ALL NOMINATED EVENTS MUST COME FROM THE SAME CALENDAR (default id: 1);

foreach($nominated_topics as $ntopicthread) {
	$ismanual = $ntopicthread['ismanual'];
	$poll_threadid = $ntopicthread['threadid'];
	$poll_postid = $ntopicthread['postid'];
	$polltype = $ntopicthread['polltype'];
	$pollid = $ntopicthread['pollid'];
	
	$contestname = $polltype == 'thread' ? $vbulletin->options['nomination_contest_name'] : $polltype;
	$tablename = $polltype == 'media' ? THIS_TABLE . 'linkslink' : TABLE_PREFIX . ($polltype == 'group' ? 'socialgroup' : $polltype);
	$entryid = $polltype == 'media' ? 'linkid' : $polltype . 'id';
	$createThread = $createNewThread;
	if ($ismanual)					// run the cron manually;
		$createThread = 1;
	if ($createThread == 0) {
		log_cron_action('No new thread created for ' . $polltype, $nextitem);
		echo 'No new thread created for ' . $polltype;
		continue;					// manual nominations not selected and not time yet for auto nominations;
	}
	
	$pollid = empty($pollid) ? '0' : "$pollid";		// set $pollid to '0' if it's empty;
	$pollstatus = $vbulletin->db->query_first("SELECT active FROM " .TABLE_PREFIX. "poll WHERE pollid = $pollid");
	if ($pollid && $pollstatus['active']) {
		// Close contest thread, close poll, and get poll results;
		
		$pollinfo = $vbulletin->db->query_first("SELECT * FROM " . TABLE_PREFIX . "poll WHERE `pollid` = $pollid");
		$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "poll SET `active`='0' WHERE `pollid` = $pollid");
		$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "thread SET `open`='0' WHERE `threadid` = $poll_threadid");
		
		// $bbcode_parser =& new vB_BbCodeParser($vbulletin, fetch_tag_list());		// NOT NEEDED;
		// $pollinfo['question'] = $bbcode_parser->parse(unhtmlspecialchars($pollinfo['question']), $nomination_forum, 1);
		$splitoptions = explode('|||', $pollinfo['options']);
		$splitvotes = explode('|||', $pollinfo['votes']);
		$pollinfo['numbervotes'] = $pollinfo['multiple'] ? $pollinfo['voters'] : array_sum($splitvotes);
		
		$nomids = array();
		$winning_options = array();
		foreach ($splitvotes AS $index => $value) {		// set the values for each nominee;
			$option['votes'] = $value;
			$option['title'] = $splitoptions[$index];
			// $option['question'] = $bbcode_parser->parse($splitoptions[$index], $nomination_forum, true);
			$option['percent'] = $option['votes'] <= 0 || $pollinfo['numbervotes'] == 0 ? '0' : 
				vb_number_format(($option['votes'] < $pollinfo['numbervotes']) ? $option['votes'] / $pollinfo['numbervotes'] * 100 : 100, 2);
			
			// IMPORTANT: FINDING $topicid && $topicuserid REQUIRES [b] && [i] tags in poll title phrases;
			$optiontext = $splitoptions[$index];
			$needle = "[$polltype=";
			$startpos = strpos($optiontext, $needle);
			$endpos = strpos($optiontext, '][b]');		// MUST USE [b] to identify the $polltype tag's closing ']';
			$topicid = intval(substr($optiontext, $startpos + strlen($needle), $endpos - $startpos));
			if ($topicid <= 0 || $startpos === false || $endpos <= $startpos)
				continue;							// poll option's topicid is invalid;
			$nomids[] = $topicid;
			
			if ($polltype == 'user')
				$topicuserid = $topicid;
			else {
				$needle = '[user=';						// MUST USE [i] to identify the user tag's closing ']';
				$startpos = strpos($optiontext, $needle);
				$endpos = strpos($optiontext, '][i]');
				$topicuserid = intval(substr($optiontext, $startpos + strlen($needle), $endpos - $startpos));
				if ($topicuserid <= 0 || $startpos === false || $endpos <= $startpos)
					$topicuserid = 0;				// userid is invalid; set to 0, but finish executing nominations code;
			}
			$nomination = $vbulletin->db->query_first("
				SELECT username, usergroupid, displaygroupid, infractiongroupid, reputation
				FROM " . TABLE_PREFIX . "user WHERE userid = $topicuserid
			");
			if ($nomination) {						// set user info to save users' reputations;
				$option['userid']  = $topicuserid;
				$option['userrep'] = $nomination['reputation'];
				$option['musername'] = fetch_musername($nomination);
			}
			$option['polltype'] = $polltype;		// set poll info to save topics' awards;
			$option['pollid'] = $pollid;
			$option['topicid'] = $topicid;
			
			// [0][0]['votes'] == votes of 1st user in 1st place;
			if ($option['votes'] > 0) {
				if ($option['votes'] >= $winning_options[0][0]['votes'])
					$winning_options[0][] = $option;
				elseif ($option['votes'] >= $winning_options[1][0]['votes'])
					$winning_options[1][] = $option;
				elseif ($option['votes'] >= $winning_options[2][0]['votes'])
					$winning_options[2][] = $option;
			}
		}	// END OF foreach loop;
		if (count($nomids)) {
			$ids = implode(',', $nomids);
			$vbulletin->db->query_write(	// reset nominee's rank to 0; $tablename already has prefix set;
				"UPDATE $tablename SET nomination_tpid = 0, nomination_rank = 0 WHERE $entryid IN ($ids)");
		}
		
		$counter = 0;
		$winners = array();
		$winning_titles = array();
		for ($j=0; $j<3; $j++) {
			if ($counter == 3)
				break;
			elseif (!empty($winning_options[$j])) {
				for ($k=0; $k<3; $k++) {
					$option = $winning_options[$j][$k];
					if ($counter == 3)
						break;
					elseif (!empty($option)) {
						$needle = '[/user]';
						$endpos = strpos($option['title'], $needle);
						$votestr = " ($option[votes] votes, $option[percent]%)";
						$winning_titles[$j][] = $endpos === false ? $option['title'] . $votestr : substr($option['title'], 0, $endpos + strlen($needle)) . $votestr;
					
						// +1 because default rank is 0; +$j (NOT $k or $counter) due to possible ties (same placed winners);
						$assigned_rank = 3*($contesttime - 1) + $j + 1;
						$vbulletin->db->query_write(	// save winner's rank; $tablename already has prefix set;
							"UPDATE $tablename SET nomination_tpid = $option[pollid], nomination_rank = $assigned_rank WHERE $entryid = $option[topicid]");
						
						// MUST RESET $set_user_award for each iteration; postid can not be topicid since reputation table requires post's id;
						$set_user_award = 'nomination_awards = nomination_awards + 1';
						if ($option['userid'] && $winner_reppoints) {		// use $j (NOT $k or $counter) due to possible ties;
							$repphrase = construct_phrase($vbphrase['nomination_winner_reputation'], ucfirst($contestname), $j == 0 ? '1st' : ($j == 1 ? '2nd' : '3rd'));
							$vbulletin->db->query_write("
								INSERT IGNORE INTO " .TABLE_PREFIX. "reputation
								(postid, userid, reputation, whoadded, reason, dateline)
								VALUES ($poll_postid, $option[userid], " . $winner_reppoints[$counter] . ", $threadOwnerID, '$repphrase', $now)");
					
							if ($vbulletin->db->affected_rows() != 0)	{
								$replevel = $vbulletin->db->query_first_slave("
									SELECT reputationlevelid FROM " .TABLE_PREFIX. "reputationlevel
									WHERE " . (intval($option['userrep']) + $winner_reppoints[$counter]) . " >= minimumreputation
									ORDER BY minimumreputation DESC LIMIT 1
								");
								$set_user_award .= ", reputation = " . $winner_reppoints[$counter] . " + reputation, reputationlevelid = " . intval($replevel['reputationlevelid']);
							}
						}
						if ($option['userid']) {		// save user award and reputation;
							$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "user SET $set_user_award WHERE userid = $option[userid]");
							$winners[$option['userid']] = $option['musername'];
						}
						$counter++;
						// -----------------------------		INSERT PM CODE HERE				---------------------------
					}
				}
			}
		}	// end of outer for loop;
		$first_place = is_array($winning_titles[0]) ? implode('; ', $winning_titles[0]) : 'N/A';
		$second_place= is_array($winning_titles[1]) ? implode('; ', $winning_titles[1]) : 'N/A';
		$third_place = is_array($winning_titles[2]) ? implode('; ', $winning_titles[2]) : 'N/A';
		$newPostTitle = construct_phrase($vbphrase['nomination_winners_title'], ucfirst($contestname), $year_number, $month_name, $week_number);
		$newPostText = construct_phrase($vbphrase['nomination_winners_message'], $first_place, $second_place, $third_place);
		
		$threaddm =& datamanager_init('Post', $vbulletin, ERRTYPE_ARRAY, 'threadpost');
		$threaddm->set('threadid', $poll_threadid);
		$threaddm->set('userid', $threadOwnerID);
		$threaddm->set('title', $newPostTitle);
		$threaddm->set('pagetext', $newPostText);
		$threaddm->set('allowsmilie', 1);
		$threaddm->set('visible', 1);
		$threaddm->set('dateline', $now);
		$threaddm->save();
		
		$winnerids = array_keys($winners);
		$winnerids = implode(',', $winnerids);
		$winners = implode(',', $winners);
		$logphrase = "Winners saved: $winners (ids: $winnerids); ";
		log_cron_action($logphrase, $nextitem);
		echo $logphrase;
	}	// -------------------------	end of $pollid condition	--------------------------------------------
	
	// if ($ismanual)
	//	continue;		// 	----------------------------	UNCOMMENT TO TEST!		----------------------------
	
	$validtime = strtotime($contesttime == 1 ? '-1 week' : ($contesttime == 2 ? '-15 days' : '-1 month'), $now);
	$serviceforums = $vbulletin->options['nomination_service_forums'];
	if ($polltype == 'thread')			// $validtime ensures votecount >= 1 for selected entries;
		$query = $vbulletin->db->query_read(
			"SELECT threadrate.threadid AS topicid, SUM(vote) AS votetotal, COUNT(vote) AS votecount, 
			SUM(vote) / COUNT(vote) AS voteavg FROM " .TABLE_PREFIX. "threadrate AS threadrate
			LEFT JOIN " .TABLE_PREFIX. "thread AS thread ON (thread.threadid = threadrate.threadid)
			WHERE vbb_date >= $validtime AND nomination_rank = 0 AND thread.forumid IN ($serviceforums) GROUP BY thread.threadid ORDER BY voteavg DESC");
	else {								// get recent votes; $tablename already has prefix set;
		$rate_tablename = $polltype == 'media' ? THIS_TABLE . 'filerate' : $tablename . 'rate';
		$query = $vbulletin->db->query_read(
			"SELECT profileid AS topicid, SUM(vote) AS votetotal, COUNT(vote) AS votecount,
			SUM(vote) / COUNT(vote) AS voteavg FROM $rate_tablename AS ratetable
			LEFT JOIN $tablename AS maintable ON (ratetable.profileid = maintable.{$entryid})
			WHERE ratetable.dateline >= $validtime AND nomination_rank = 0 GROUP BY profileid ORDER BY voteavg DESC");
	}
	$maxcount = min($vbulletin->options['maxpolloptions'], $vbulletin->options['nomination_nominees_count']);
	
	$nominees = array();
	$titles = array();
	while ($nominated = $vbulletin->db->fetch_array($query)) {
		if (count($nominees) == $maxcount)
			break;
		$nominees[$nominated['topicid']] = $nominated;		// set nominated entry;
	}
	$vbulletin->db->free_result($query);
	$nomids = array_keys($nominees);
	$ids = implode(',', $nomids);		// must reset BOTH $nomids && $ids;
	
	if (count($nomids)) {
		$usernames = array();
		$usercol = $polltype == 'thread' ? 'postuserid' : ($polltype == 'group' ? 'creatoruserid' : ($polltype == 'media' ? 'linkuserid' : 'userid'));
		$query = $vbulletin->db->query_read("SELECT user.userid, user.username FROM " .TABLE_PREFIX. "user AS user " .
			($polltype == 'user' ? '' : " LEFT JOIN $tablename ON (user.userid = $tablename.{$usercol}) ") .
			" WHERE $tablename.{$entryid} IN ($ids)");		// get users' names; $tablename already has prefix set;
		while ($user = $vbulletin->db->fetch_array($query)) {
			$usernames[$user['userid']] = $user['username'];
		}	// use an extra query to get usernames since it enables cleaner queries below;
		
		if ($polltype == 'user')
			$query = $vbulletin->db->query_read("SELECT userid AS topicid, userid, username AS title FROM " 
			.TABLE_PREFIX. "user WHERE userid IN ($ids) ORDER BY title");
		elseif ($polltype == 'thread')
			$query = $vbulletin->db->query_read("SELECT threadid AS topicid, postuserid AS userid, title FROM " 
			.TABLE_PREFIX. "thread WHERE threadid IN ($ids) ORDER BY title");
		elseif ($polltype == 'group')
			$query = $vbulletin->db->query_read("SELECT groupid AS topicid, creatoruserid AS userid, name AS title FROM " 
			.TABLE_PREFIX. "socialgroup WHERE groupid IN ($ids) ORDER BY title");
		elseif ($polltype == 'media')
			$query = $vbulletin->db->query_read("SELECT linkid AS topicid, linkuserid AS userid, linkname AS title FROM " 
			.THIS_TABLE. "linkslink WHERE linkid IN ($ids) ORDER BY title");
		elseif ($polltype == 'event')
			$query = $vbulletin->db->query_read("SELECT eventid AS topicid, userid, title FROM " 
			.TABLE_PREFIX. "event WHERE eventid IN ($ids) ORDER BY title");
		elseif ($polltype == 'album')
			$query = $vbulletin->db->query_read("SELECT albumid AS topicid, userid, title FROM " 
			.TABLE_PREFIX. "album WHERE albumid IN ($ids) ORDER BY title");
		
		while ($nominated = $vbulletin->db->fetch_array($query)) {
			$thisid = $nominated['topicid'];
			$thisuserid = $nominated['userid'];
			$nominees[$thisid]['userid'] = $thisuserid;			// add userid to $nominees;
			$thisrating = vb_number_format($nominees[$thisid]['votetotal'] / $nominees[$thisid]['votecount'], 2);
			
			// IMPORTANT: FINDING $topicid && $topicuserid REQUIRES [b] && [i] tags in poll title phrases;
			$titles[] = "[$polltype=$thisid][b]" . $vbulletin->db->escape_string(unhtmlspecialchars($nominated['title'])) . "[/b][/$polltype]" .
			($polltype == 'user' ? ' ' : " by [user=$thisuserid][i]{$usernames[$thisuserid]}[/i][/user] ") .
			construct_phrase($vbphrase['nomination_ave_ratings'], $contest_period, $thisrating, $nominees[$thisid]['votecount']);
		}
		$vbulletin->db->free_result($query);
	}
	$numberoptions = count($titles);
	if ($numberoptions == 0) {
		$options = construct_phrase($vbphrase['nomination_no_nominees'], $contestname . ($polltype == 'media' ? '' : 's'), $contest_period);
		$votes = '0';
		$active = 0;
	}
	else {
		$options = implode('|||', $titles);
		$votes = implode('|||', array_fill(0, $numberoptions, '0'));
		$active = 1;
	}
	$timeout = $contesttime == 1 ? 7 : ($contesttime == 2 ? 15 : $days_in_month);
	$public = $vbulletin->options['nomination_public_poll'];
	$voters = 0;
	
	$question = construct_phrase($vbphrase['nomination_contest_question'], $contestname, $contest_period, $year_number, $month_name, $week_number);
	$newThreadTitle = construct_phrase($vbphrase['nomination_contest_title'], ucfirst($contestname), $year_number, $month_name, $week_number);
	$newThreadText = construct_phrase($vbphrase['nomination_thread_message'], $contestname . ($polltype == 'media' ? '' : 's'), $contest_period, $maxcount);
	
	$vbulletin->db->query_write("INSERT INTO " . TABLE_PREFIX . "poll 
		(question, public, options, votes, active, numberoptions, timeout, voters, dateline) 
		VALUES ('$question', '$public', '$options', '$votes', $active, $numberoptions, $timeout, $voters, $now)");
	$new_pollid = $vbulletin->db->insert_id();
	$threaddm =& datamanager_init('Thread_FirstPost', $vbulletin, ERRTYPE_ARRAY, 'threadpost');
	
	$threaddm->set_info('forum', $foruminfo);
	$threaddm->set('forumid', $nomination_forum);
	$threaddm->set('pollid', $new_pollid);
	$threaddm->set('userid', $threadOwnerID);
	$threaddm->set('title', $newThreadTitle);
	$threaddm->set('pagetext', $newThreadText);
	$threaddm->set('allowsmilie', 1);
	$threaddm->set('visible', 1);
	$threaddm->set('dateline', $now);
	if ($numberoptions == 0)
		$threaddm->set('open', 0);
	
	$new_threadid = $threaddm->save();
	$thisthread = $vbulletin->db->query_first("SELECT firstpostid FROM " .TABLE_PREFIX. "thread WHERE threadid = $new_threadid");
	$new_postid = $thisthread['firstpostid'];
	
	// REQUIRES $new_pollid && $new_threadid, so must be AFTER poll and thread creation;
	if (count($nomids))
		$vbulletin->db->query_write(		// set nominee's rank to nominated; $tablename already has prefix set;
		"UPDATE $tablename SET nomination_tpid = $new_threadid, nomination_rank = -1 WHERE $entryid IN ($ids)");
	foreach($nominees as $nominated) {		// save new nominee and user's nominations;
		$nominated['pollid'] = $new_pollid;
		$vbulletin->db->query_write("INSERT INTO " .TABLE_PREFIX. "nomination_topic 
			(pollid, userid, topicid, votetotal, votecount)
			VALUES ($nominated[pollid], $nominated[userid], $nominated[topicid], $nominated[votetotal], $nominated[votecount])");
		
		$vbulletin->db->query_write("UPDATE " .TABLE_PREFIX. "user SET nomination_count = nomination_count + 1 WHERE userid = $nominated[userid]");
	}	// users can not be mass updated since a user may have more than 1 nominated entry per contest;
	
	if ($pollid)				// remove old contest, remove new cached post, and add new contest;
		$vbulletin->db->query_write("DELETE FROM " .TABLE_PREFIX. "nomination_thread WHERE pollid = $pollid");
	$vbulletin->db->query_write("DELETE FROM " .TABLE_PREFIX. "postparsed WHERE postid = $new_postid");
	$vbulletin->db->query_write("INSERT INTO " .TABLE_PREFIX. "nomination_thread (threadid, postid, pollid, polltype) 
		VALUES ($new_threadid, $new_postid, $new_pollid, '$polltype')");
	$cron_phrase = construct_phrase($vbphrase['nomination_cron_finished'], $polltype, $new_threadid, $new_postid, $new_pollid);
	log_cron_action($cron_phrase, $nextitem);
	echo $cron_phrase;
}

?>