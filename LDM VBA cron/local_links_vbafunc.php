<?php

/* ===========================================================================*/
//
// This code is provided free on the basis that you do not claim that
// it is your own, sell it or use it as the basis for other products that you
// sell. By all means extend it, modify it, upgrade it, correct it,
// suggest improvements, call me an idiot, etc.
//
// (c) 2004/08
// Andrew Dearing
// European Industrial Research Management Association
// www.eirma.org
//
// v2.3.0, 01.09.2008
// For VB 3.7.x
// see changes.txt for history
// v1.00, 1.3.2004
//
/* ===========================================================================*/

function ldm_vba_links($filter, $hitssince, $order, $template, $catsee, $linksee, $ncols=1, $showsubcats=0, $showcatname=0, $pagenumber=1) {
	global $vbulletin;
	global $links_defaults, $links_permissions, $linkscat;

	$can_bypass = $links_permissions['can_see_protected_links_on_portal'] | $links_permissions['can_bypass_forumperms'];
	$limitfids = ldm_lookup_forum_protections($can_bypass);
	$filter[] = 'link.linkforum NOT IN ('.implode(',', $limitfids).') ';

	$pagenumber = max($pagenumber, 1);

	if ($showsubcats and $catsee) {
		$catparents = explode(',', $catsee);
		foreach ($linkscat as $thiscat) {
			$cplist = explode(',', $thiscat['parentlist']);
			foreach ($catparents as $cp) {
				if (in_array($cp, $cplist)) {
					$catsee .= ", ".$thiscat['catid'];
				}
			}
		}
	}

	if ($catsee) {
		$filter[] = 'ltoc.catid IN ('.$vbulletin->db->escape_string($catsee).')';
	}
	$query = ldm_get_mainsql($filter, $hitssince, 0, $order, 0, 0);
	
	// NEW CODE: use ldm_get_entrybits_brief && ldm_layout_listbit instead of ldm_get_entrybits && ldm_map_listbit_to_grid;
	list($linkbits, $nhits) = ldm_get_entrybits_brief($query, $linksee, $template, 0, true);
	$links = ldm_layout_listbit($linkbits, $ncols, 1);
	return array($links, $nhits, $query);
}

/**
* Construct the requested 'hit parades'
*
* @param	int		Categoryid
* @return	str		Displaybit
*/

function ldm_get_hitparades($catid, $template="links_hitparade", $ishot = false) {
	global $vbulletin, $vbphrase, $stylevar;		// NEW CODE: added $ishot argument for 2nd hits parade column;
	global $links_defaults, $links_permissions;
	global $FAVS_CAT, $HIDE_CAT, $AVL_CAT, $BRKN_CAT, $INVD_CAT, $HOT_CAT, $FHOT_CAT, $BASE_CAT;
	global $NEW_CAT, $RND_CAT, $MY_CAT, $UPLD_CAT, $FEAT_CAT, $NEW_RATE, $NEW_FRATE, $NOM_CAT, $CNOM_CAT;
	global $LINK_ACCEPTED, $LINK_TO_MODERATE;		// NEW CODE: globalize $NEW_FRATE, $FHOT_CAT, $BASE_CAT;
	global $SEARCH_SCRIPT;

	$linksee = (isset($links_defaults['links_seen_on_portal']) ? $links_defaults['links_seen_on_portal'] : 5);
	$newsince = $hitsince = ($links_defaults['days_seen_on_portal'] ? TIMENOW-intval($links_defaults['days_seen_on_portal'])*24*60*60 : $vbulletin->userinfo['lastvisit']);
	$catids	= $catid > 0 ? array($catid) : 0;
	$showme = 0;

	if ($links_defaults['show_hp_new_summary']) {
		$mquery = "";
		if (!$links_permissions["can_moderate_links"] and !$links_permissions["can_moderate_forums"]) {
			if (!$vbulletin->userinfo['userid']) {
				$mquery = "AND link.linkmoderate = ".$LINK_ACCEPTED;
			}
			else {
				$mquery = "AND (link.linkmoderate = ".$LINK_ACCEPTED." OR
						(link.linkmoderate = ".$LINK_TO_MODERATE." AND link.linkuserid = ".$vbulletin->userinfo['userid'].")
							)";
			}
		}
		$can_bypass = $links_permissions['can_see_protected_links_on_portal'] | $links_permissions['can_bypass_forumperms'];
		$limitfids = ldm_lookup_forum_protections($can_bypass);
		$query = "
			SELECT COUNT(linkid) AS newlinks
			FROM ".THIS_TABLE."linkslink AS link
			WHERE link.linkforum NOT IN (".implode(',', $limitfids).")
			AND linkdate > ".$hitsince."
			$mquery
			";
		$count = $vbulletin->db->query_first("$query");
		$hitdate = ldm_date($vbulletin->options['dateformat'], $hitsince);
		$links['new_summary']	= $count['newlinks'];
		$showme = 1;
	}
	
	if ($links_defaults['show_hp_new']) {		// NEW CODE: added $newsince for $NEW_CAT;
		$query = ldm_get_specialsearchsql($NEW_CAT, $linksee, 'd', $catids, $newsince);
		list($links['show_hp_new'], $hits) = ldm_get_entrybits_brief($query, $linksee, $template);
		$showme = ($hits ? 1 : $showme );
	}

	if ($links_defaults['show_hp_nominate']) {
		$query = ldm_get_specialsearchsql($NOM_CAT, $linksee, "", $catids);
		list($links['show_hp_nominate'], $hits) = ldm_get_entrybits_brief($query, $linksee, $template);
		$showme = ($hits ? 1 : $showme );
	}
	
	// NEW CODE: changed $HOT_CAT to $FHOT_CAT, added $ishot and WHERE condition (link.linkimgthumb != '');
	if ($links_defaults['show_hp_top'] && $ishot) {
		$links['show_hp_top'] = $catid < 0 ? $vbulletin->ldm_FHOT_CAT : '';
		if (!$links['show_hp_top']) {
			$query = ldm_get_specialsearchsql($FHOT_CAT, $linksee, $links_defaults['days_seen_on_portal'], $catids, 0, $hitsince, 0, array("link.linkimgthumb != ''"));
			list($links['show_hp_top'], $hits) = ldm_get_entrybits_brief($query, $linksee, $template);
			$showme = ($hits ? 1 : $showme );
			if ($catid < 0)
				build_datastore('ldm_FHOT_CAT', $links['show_hp_top']);
		}
	}

	if ($links_defaults['show_hp_topall'] && $ishot) {
		$links['show_hp_topall'] = $catid < 0 ? $vbulletin->ldm_HOT_CAT : '';
		if (!$links['show_hp_topall']) {
			$query = ldm_get_specialsearchsql($HOT_CAT, $linksee, 'h', $catids);
			list($links['show_hp_topall'], $hits) = ldm_get_entrybits_brief($query, $linksee, $template);
			$showme = ($hits ? 1 : $showme );
			if ($catid < 0)
				build_datastore('ldm_HOT_CAT', $links['show_hp_topall']);
		}
	}
	
	// NEW CODE: changed $RND_CAT TO $NEW_FRATE, added !$ishot and WHERE condition (link.linkimgthumb != '');
	if ($links_defaults['show_hp_random'] && !$ishot) {
		$links['show_hp_random'] = $catid < 0 ? $vbulletin->ldm_NEW_FRATE : '';
		if (!$links['show_hp_random']) {
			$query = ldm_get_specialsearchsql($NEW_FRATE, $linksee, 'r', $catids, $newsince, 0, 0, array("link.linkimgthumb != ''"));
			list($links['show_hp_random'], $hits) = ldm_get_entrybits_brief($query, $linksee, $template);
			$showme = ($hits ? 1 : $showme );
			if ($catid < 0)
				build_datastore('ldm_NEW_FRATE', $links['show_hp_random']);
		}
	}
	
	if ($links_defaults['show_hp_newcomment'] && !$ishot) {
		$links['show_hp_newcomment'] = $catid < 0 ? $vbulletin->ldm_NEW_RATE : '';
		if (!$links['show_hp_newcomment']) {
			$query = ldm_get_specialsearchsql($NEW_RATE, $linksee, 'r', $catids);
			list($links['show_hp_newcomment'], $hits) = ldm_get_entrybits_brief($query, $linksee, $template);
			$showme = ($hits ? 1 : $showme );
			if ($catid < 0)
				build_datastore('ldm_NEW_RATE', $links['show_hp_newcomment']);
		}
	}

	$showme = true;		// NEW CODE: always return results even if empty so that 3 columns are always shown;
	if ($showme) {		// NEW CODE: use links_hitparade_film templates;
		if ($ishot)
			eval("\$hitbits  = \"".fetch_template('links_hitparade_film2')."\";");
		else
			eval("\$hitbits  = \"".fetch_template('links_hitparade_film')."\";");
		return $hitbits;
	}
	return "";
}

/**
* Simplified linkbit constructor used by portal interfaces and hit parades
*
* @param	str		Search query
* @param	int		Maximum number of entries to display
* @param	str		Linkbit
* @param	array	Optional, additional attribute values to patch into linkbits
* @return	str		Displaybit
*/

function ldm_get_entrybits_brief($query, $linksee, $template, $addvars=0, $isvba=0) {
	global $vbulletin, $stylevar;
	global $LINKS_SCRIPT, $SEARCH_SCRIPT, $ADMIN_SCRIPT, $ACTION_SCRIPT, $RESIZE_SCRIPT;
	global $links_defaults, $links_permissions, $linkscat, $ldm_icon_cache;
	global $show_catname, $bgclass, $LINK_OK, $LINK_HIDDEN, $vbphrase;
	// NEW CODE: need to globalize $bgclass, $LINK_OK, $LINK_HIDDEN, $vbphrase;

	cache_LDMicons();
	$nhits = 0;
	$hits = array();
	$linkbit = array();
	$expire = 0;
	if ($links_defaults['links_expiry_days'] > 0)
		$expire = $links_defaults['links_expiry_days']*86400;

	// NEW CODE: use < instead of <= for $nhits <= $linksee condition;
	$asb = $vbulletin->db->query_read($query);
	while ($myrow=$vbulletin->db->fetch_array($asb) and $nhits < $linksee) {
		$linkid	= $myrow["linkid"];
		if (is_array($addvars)) {
			if (isset($addvars[$linkid])) {
				foreach ($addvars[$linkid] as $thiskey=>$thisval) {
					$myrow[$thiskey] = $thisval;
				}
			}
		}
		if (isset($hits[$linkid])) {
			continue;
		}

		$linkexpired = 0;
		$linkdatebin = $myrow["linkdate"];
		$linkstatus = $myrow["linkstatus"];
		if ($expire) {
			if ($linkdatebin>TIMENOW+60) {
				$linkexpired = -1;  // post-dated, so just invisible
			}
			elseif ($linkdatebin+$expire<TIMENOW) {
				$linkexpired = +1;
			}
		}
		if (($linkstatus==$LINK_HIDDEN or $linkexpired) and !$links_permissions["can_view_hidden"]) {
			continue;
		}
		
		$nhits++;
		$hits[$linkid] = 1;								// NEW CODE: set $linkcatid and trim $linkname;
		$linkname = fetch_trimmed_title($myrow["linkname"], 30);
		$linkdesc = $myrow["linkdesc"];					// may be used for $linkshortdesc;
		$catid = $linkcatid = $myrow["linkcatid"];		// templates use $linkcatid;
		if (array_key_exists("linkrecenthits", $myrow))
			$linkhits = $myrow["linkrecenthits"];
		else
			$linkhits = $myrow["linkhits"];
		exec_switch_bg();
		
		$linksize = ldm_format_bytes($myrow["linksize"]);
		$linkurllink = ldm_get_url_atag(-1, $catid, $linkid, $linkname);
		$linkimg  = $myrow["linkimg"];
		$linkimgthumb = $myrow["linkimgthumb"];
		$linkimgthumbsize = $myrow["linkimgthumbsize"];
		$linkdate = vbdate($vbulletin->options['dateformat'], $myrow["linkdate"], true);
		$linkusername = $myrow["linkusername"];
		$linkuserid = $myrow["linkuserid"];
		$linkurl = $myrow["linkurl"];
		$linkimgshow = $linkimgsrc = '';
		
		$urlInfo = ldm_parse_url($linkurl);
		$urlType = file_extension($urlInfo['path']);
		$linkfiletype = strtolower($urlType);
		$linkcatname = $linkscat[$catid]["catname_clean"];
		$linktypebit = '<img border="0" src="' . ($urlType && array_key_exists($urlType, $ldm_icon_cache) ? $ldm_icon_cache[$urlType] : $ldm_icon_cache['link']) . '" alt="' . $urlType . '" />';
		
		// NEW CODE: set $linkimgsrc, $linkimgshow, $linkimgjump, $linkurljump;
		if ($linkimgthumb and $linkimgthumbsize == $links_defaults['link_imagesize'] and !$urlInfo['user']) {
			$linkimgsrc = create_full_url(($linkimgthumb[0] != '/' ? "/" : "") . $linkimgthumb);
		}	// NEW CODE: check 'mp3' filetype since LDM will repeated try to create thumbnails for mp3's wo. ID3 image tags;
		elseif ($linkfiletype != 'mp3') {
			$linkimgsrc = ldm_make_filename($vbulletin->options['bburl'],$RESIZE_SCRIPT).'.php?'.
				$vbulletin->session->vars['sessionurl'].'linkid='.$linkid.'&amp;catid='.$linkcatid.'&amp;size='.$linkimgsize;
		}
		if ($linkimg && $linkimgsrc)
			$linkimgshow = ldm_get_imagebit($linkimgsrc);
		$linkimgjump = ldm_get_url_atag(-1, $linkcatid, $linkid, $linkimgshow);
		$linkurljump = ldm_get_url_atag(-1, $linkcatid, $linkid, $linkname, $linkurl, $vbphrase['ll_visiturl']);
		
		// NEW CODE: set $is_musicbox, $linknewbit, $linkvoteavg, $linkimgmag, and added ldm_linkbit_create_brief hook;
		$linknewbit = $linkrate = "";
		require_once(DIR . '/includes/local_links_players.php');
		if (get_ldm_playerid($linkid, $linkurl, $linkfiletype)>=0)
			$is_musicbox = 1;
		if ($vbulletin->userinfo['lastvisit'] < $myrow['linkdate'])
			$linknewbit = (isset($ldm_icon_cache['new']) ? ldm_get_imagebit($ldm_icon_cache['new'], $vbphrase['ll_new']) : $vbphrase['ll_new']);
		$linkvoteavg = $myrow["linkvoteavg"];
		$linkraters = $myrow["numrate"];
		if ($linkraters > 0) {
			$linkvoteavg = vb_number_format($linkvoteavg, 2);
			$linkrate = intval(round($linkvoteavg));
		}
		if ($linkimg)		// needed by Shadowbox (DO NOT restrict with permissions)
			eval("\$linkimgmag = \"".fetch_template('links_imgmag')."\";");
		($hook = vBulletinHook::fetch_hook('ldm_linkbit_create_brief')) ? eval($hook) : false;
		
		eval("\$linkbit[$linkid] = \"".fetch_template($template)."\";");
	}
	$vbulletin->db->free_result($asb);
	unset($hits, $myrow);
	
	// NEW CODE: VBA mods require linkbit to be an array instead of a concatenated string;
	if (is_array($addvars) && !$isvba) {
	// return results sorted in same order as addvars
		$sortbit = array();
		foreach ($addvars as $thisid=>$thisvar) {
			$sortbit[] = $linkbit[$thisid];
		}
		$linkbit = implode(" ", $sortbit);
	}
	elseif (!$isvba) {
		$linkbit = implode(" ", $linkbit);
	}
	
	return array($linkbit, $nhits);
}

/**
* Build the sql query used for special searches (hot, new, etc)
*
* @param	int		Search type, index into special categories defined in ldm_general_init
* @param	int		Optional, maximum number of entries to return
* @param	str		Optional, sort type, character or number of days
* @param	int		Optional, limit search to entries in these categories
* @param	int		Optional, limit search to entries created since this time
* @param	int		Optional, limit search to entries visited since this time
* @param	int		Optional, limit search to entries by userid
* @param	array	Optional, additional query terms
* @return	str		SQL query
*/

function ldm_get_specialsearchsql($searchtype, $showmax=0, $sort="", $catids=0, $newsince=0, $hitsince=0, $userid=0, $specialqueries=array()) {
	global $vbulletin;
	global $links_defaults, $links_permissions;
	global $links_starred;								// NEW CODE: globalize $NEW_FRATE, $FHOT_CAT, $LINK_OK;
	global $FAVS_CAT, $HIDE_CAT, $AVL_CAT, $BRKN_CAT, $INVD_CAT, $HOT_CAT, $FHOT_CAT;
	global $NEW_CAT, $RND_CAT, $MY_CAT, $UPLD_CAT, $FEAT_CAT, $NEW_RATE, $NEW_FRATE, $NOM_CAT, $CNOM_CAT;
	global $LINK_HIDDEN, $LINK_BROKEN, $LINK_UPLOAD, $LINK_NO_ACCESS, $LINK_ACCEPTED, $LINK_TO_MODERATE, $LINK_OK;

	$can_bypass = $links_permissions['can_see_protected_links_on_portal'] | $links_permissions['can_bypass_forumperms'];
	$limitfids = ldm_lookup_forum_protections($can_bypass);
	$joinusers = $joinfavs = 0;
	$query = $specialqueries;

	$query[] = "link.linkforum NOT IN (".implode(',', $limitfids).")";
	// $newsince should be set for $NEW_CAT, $FHOT_CAT, && $NEW_FRATE (potentially others) in both hits parade & find pages;
	if ($newsince) {
		$query[] = "link.linkdate > ".$newsince;
	}
	if ($userid) {
		$query[] = "link.linkuserid='".$userid."'";
	}
	
	if (!$links_permissions["can_moderate_links"] and !$links_permissions["can_moderate_forums"]) {
		if ($vbulletin->userinfo['userid'] == 0)
			$query[] = "link.linkmoderate = $LINK_ACCEPTED";
		else
			$query[] = "(link.linkmoderate = $LINK_ACCEPTED OR
				(link.linkmoderate = $LINK_TO_MODERATE AND link.linkuserid = ".$vbulletin->userinfo['userid']."))";
	}
	if (!in_array($searchtype, array($LINK_HIDDEN, $LINK_BROKEN, $LINK_NO_ACCESS))) {
		$query[] = "link.linkstatus > 0";	// NEW CODE: do not show hidden or broken entries;
	}
	
	if (!is_array($catids)) {
		$catids = array();					// $catids == 0 for base && search categories;
	}
	elseif (count($catids)) {
		$joinkeys = true;
		$catid = intval($catids[0]);
		if (!$links_defaults['upload_enabled'] || $searchtype == $HOT_CAT || $searchtype == $NEW_RATE)
			$catids = array();
		else		// NEW CODE: reset $catids to show file entries in links categories, or link entries in files categories;
			$query[] = "ltoc.catid IN (". implode(',', $catids) .")";
	}
	
	switch ($catid) {						// NEW CODE: set $doctype keyword for links hits parade in files categories;
		case 1:
		case 4:
			$doctype = 'video'; break;
		case 5:
		case 8:
			$doctype = 'photo'; break;
		case 9:
		case 12:
			$doctype = 'audio'; break;
		case 13:
		case 16:
			$doctype = 'document'; break;
		default:
			$doctype = 'link'; break;
	}
	
	switch ($searchtype) {
	case $FAVS_CAT:
		$joinfavs = 1;
		$query[] = "lfav.userid = '".$vbulletin->userinfo['userid']."'";
		break;

	case $HIDE_CAT:
		$query[] = "link.linkstatus='".$LINK_HIDDEN."'";
		break;

	case $AVL_CAT:		// NEW CODE: show only linked entries for $AVL_CAT;
		$query[] = "link.linkstatus='".$LINK_OK."'";
		break;

	case $BRKN_CAT:
		$query[] = "link.linkstatus='".$LINK_BROKEN."'";
		break;

	case $INVD_CAT:
		$query[] = "link.linkstatus='".$LINK_NO_ACCESS."'";
		break;

	case $UPLD_CAT:
		$query[] = "link.linkstatus='".$LINK_UPLOAD."'";
		break;

	case $MY_CAT:
		$query[] = "link.linkuserid=".$vbulletin->userinfo['userid'];
		break;

	case $NOM_CAT:

		cache_LDMnominations();
		$ids = array(-1);
		if (is_array($links_starred)) {
			foreach ($links_starred as $thisperiod=>$thesestars) {
				if (is_array($thesestars['scores'])) {
					foreach ($thesestars['scores'] as $thisstar) {
						$ids[] = $thisstar['linkid'];
					}
				}
			}
		}
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

	case $CNOM_CAT:

		$lquery = "
			SELECT star.linkid AS linkid, COUNT(star.linkid) AS stars
			FROM ".THIS_TABLE."linkstarred AS star
			WHERE star.usertime>=".intval($links_defaults['starred_entries_periodstart'])."
			GROUP BY star.linkid
			ORDER BY stars DESC
			";
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow=$vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

	case $RND_CAT:

		$lquery = "
			SELECT link.linkid AS linkid
			FROM ".THIS_TABLE."linkslink AS link
			";
		if (count($catids)) {
			$lquery .= "
				LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
				ON link.linkid=ltoc.linkid";
		}
		$lquery .= "
			WHERE " . implode(' AND ', $query);
		if (count($catids)) {
			$lquery .= "
				AND ltoc.catid IN (" . implode(',', $catids) . ")";
		}
		$lquery .= "
			ORDER BY RAND()
			LIMIT " . ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow=$vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

// #################################	ALL CODE BELOW THIS LINE HAS BEEN CUSTOMIZED!		################################

	case $FEAT_CAT:
		$joinfavs = 1;
		$feat_users = unserialize($links_defaults['featured_userid_favs']);
		$feat_users[] = -1;
		$feat_users = "lfav.userid IN (". implode(',', $feat_users) .")";
		if ($links_defaults['featured_usergroupid_favs']) {
			$query[] = "(". $feat_users . " OR user.usergroupid IN (". $links_defaults['featured_usergroupid_favs'] ."))";
			$joinusers = 1;
			/*	// uncomment to allow featured membergroups;
			$feat_groups = array();
			$feat_usergroups = explode(',', $links_defaults['featured_usergroupid_favs']);
			foreach ($feat_usergroups as $feat_usergroupid) {
				$feat_groups[] = "FIND_IN_SET('".intval($feat_usergroupid)."', user.membergroupids)";
			}
			$query[] = '('. implode(' OR ', $feat_groups) .')';
			*/
		}
		break;

	case $NEW_CAT:

		$lquery = "
			SELECT link.linkid AS linkid
			FROM ".THIS_TABLE."linkslink AS link
			";
		if (count($catids)) {
			$lquery .= "
				LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
				ON link.linkid=ltoc.linkid";
		}		// NOTE: $newsince query condition was added at line 322;
		$lquery .= "
			WHERE " . implode(' AND ', $query);
		if (count($catids)) {
			$lquery .= "
				AND ltoc.catid IN (" . implode(',', $catids) . ")";
		}
		$lquery .= "
			ORDER BY link.linkdate DESC
			LIMIT " . ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow=$vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$vbulletin->db->free_result($asb);
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

	case $FHOT_CAT:		// code below was extracted from the ORIGINAL $HOT_CAT case;

		if ($hitsince) {
			$lquery = "
				SELECT link.linkforum, lhits.linkid AS linkid, COUNT(lhits.linkid) AS linkhits
				FROM ".THIS_TABLE."linkslink AS link
				LEFT JOIN ".THIS_TABLE."linksdownloads AS lhits
				ON link.linkid = lhits.linkid
				";
			if (count($catids)) {
				$lquery .= "
					LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
					ON link.linkid=ltoc.linkid";
			}
			$lquery .= "
				WHERE usertime >= $hitsince
				AND " . implode(' AND ', $query);
			if (count($catids)) {
				$lquery .= "
					AND ltoc.catid IN (" . implode(',', $catids) . ")";
			}		// DO NOT need to filter file categories since only file entries are recorded in linksdownloads;
			$lquery .= "
				GROUP BY linkid
				ORDER BY linkhits DESC
				LIMIT " . ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		}
		else {
			$lquery = "
				SELECT link.linkid AS linkid
				FROM ".THIS_TABLE."linkslink AS link
				LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
					ON link.linkid=ltoc.linkid";
			$lquery .= "
				WHERE link.linkhits>0
				AND " . implode(' AND ', $query);
			$lquery .= "
				AND ltoc.catid != 2 AND ltoc.catid != 24";
			// hard code link categories so don't have to determine uploadable categories;
			
			$lquery .= "
				ORDER BY linkhits DESC
				LIMIT " . ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		}
		
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow=$vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$vbulletin->db->free_result($asb);
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

	case $HOT_CAT:		// NEW CODE: get linked entries only because $FHOT_CAT shows file entries only;
		// links can not be time filtered because links are not saved in linksdownloads and hit times are not saved in linkslink;
		$lquery = "
			SELECT link.linkid AS linkid
			FROM ".THIS_TABLE."linkslink AS link
			LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
				ON link.linkid=ltoc.linkid";
		
		// NEW CODE: join keyword tables for links category;
		if ($joinkeys) {
		$lquery .= "
			LEFT JOIN ".THIS_TABLE."linksltok AS ltok
				ON link.linkid = ltok.linkid
			LEFT JOIN ".THIS_TABLE."linkskeys AS lkeys
				ON ltok.keyid = lkeys.keyid";
		}
		$catids[] = 2;
		$catids[] = 24;
		$lquery .= "
			WHERE link.linkhits > 0
			AND ltoc.catid IN (" . implode(',', $catids) . ")
			AND " . implode(' AND ', $query);
		// hard code link categories so don't have to determine linked categories;
		
		if ($joinkeys)		// add keyword query for links category;
			$lquery .= " AND lkeys.keyword = '$doctype'";
		$lquery .= "
			ORDER BY linkhits DESC
			LIMIT " . ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow=$vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$vbulletin->db->free_result($asb);
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

	case $NEW_RATE:			// NEW CODE: changed to get top rated linked entries without using linksrate table;

		$limit = ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		$lquery = "
			SELECT link.linkid, IF(numrate > 0, (totrate / numrate), numrate) AS rating
			FROM ".THIS_TABLE."linkslink AS link
			LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
				ON link.linkid=ltoc.linkid";
		
		// NEW CODE: join keyword tables for links category;
		if ($joinkeys) {
		$lquery .= "
			LEFT JOIN ".THIS_TABLE."linksltok AS ltok
				ON link.linkid = ltok.linkid
			LEFT JOIN ".THIS_TABLE."linkskeys AS lkeys
				ON ltok.keyid = lkeys.keyid";
		}
		$catids[] = 2;
		$catids[] = 24;
		$lquery .= "
			WHERE numrate > 0
			AND ltoc.catid IN (" . implode(',', $catids) . ")
			AND " . implode(' AND ', $query);			// added $query condition;
		// hard code link categories so don't have to determine linked categories;
		
		if ($joinkeys)		// add keyword query for links category;
			$lquery .= " AND lkeys.keyword = '$doctype'";
		$lquery .= "
			ORDER BY rating DESC
			LIMIT $limit";
		
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow = $vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$vbulletin->db->free_result($asb);
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;
		
	case $NEW_FRATE:		// NEW CODE: added case to get top rated file entries; 

		$limit = ($showmax ? intval($showmax) : intval($links_defaults["links_seen_on_portal"]));
		$lquery = "
			SELECT link.linkid, IF(vbb_votenum > 0, (vbb_votetotal / vbb_votenum), vbb_votenum) AS rating
			FROM ".THIS_TABLE."linkslink AS link
			LEFT JOIN ".THIS_TABLE."linksltoc AS ltoc
				ON link.linkid=ltoc.linkid";
		$lquery .= "
			WHERE vbb_votenum > 0
			AND ltoc.catid != 2 AND ltoc.catid != 24
			AND " . implode(' AND ', $query);			// added $query condition;
		// hard code link categories so don't have to determine uploadable categories;
		
		$lquery .= "
			ORDER BY rating DESC
			LIMIT $limit";
		
		$asb = $vbulletin->db->query_read($lquery);
		$ids = array(-1);
		while ($myrow = $vbulletin->db->fetch_array($asb)) {
			$ids[] = $myrow['linkid'];
		}
		$vbulletin->db->free_result($asb);
		$ids = implode(',', $ids);
		$query[] = "link.linkid IN (".$ids.")";
		break;

	}	// END OF switch statement;

	if ($links_defaults['links_show_othercats'])
		$query[] = "ltoc.catid <= ltoc2.catid";

	$sorder = "";
	switch ($searchtype) {

	case $HIDE_CAT:
	case $AVL_CAT:
	case $BRKN_CAT:
	case $INVD_CAT:
	case $UPLD_CAT:
	case $FAVS_CAT:
	case $FEAT_CAT:
	case $MY_CAT:
		$sorder = ldm_get_sortsql($sort, 1);
		break;
	case $RND_CAT:
	case $NEW_CAT:
	case $HOT_CAT:
	case $FHOT_CAT:
	case $NEW_RATE:
	case $NEW_FRATE:		// NEW CODE: added $NEW_FRATE and $FHOT_CAT cases;
	case $CNOM_CAT:			// NEW CODE: moved $NEW_RATE case here;
	case $NOM_CAT:
		$sorder = $sort ? ($searchtype == $NEW_FRATE ? ldm_get_sortsql($sort, 1, 1) : ldm_get_sortsql($sort, 1)) : "FIELD(link.linkid, ".$ids.")";
		break;				// NEW CODE: sort specified in select control has priority over default sort;
	}
	($hook = vBulletinHook::fetch_hook('ldm_specialsql_complete')) ? eval($hook) : false;
	
	//						// MUST leave in $hitsince for nonpopular categories sorted by hits;
	$query = ldm_get_mainsql($query, $hitsince, $joinfavs, $sorder, 0, $links_defaults['links_show_othercats'], 1, $joinusers);
	return $query;			// NEW CODE: set limit to 0 (5th argument) for all queries;
}

?>