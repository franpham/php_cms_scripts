<?php
/*
// MYSQL datastore query: SELECT * FROM datastore WHERE title NOT IN ('pluginlist', 'pluginlistadmin') ORDER BY title
// NOTE: EACH cron task requires a page to be accessed AT ITS SPECIFIC TIME in order for the task to run;
// NOTE: the Task Manager will show when a task was SUPPOSED to run, but only the Task Logger can verify if it actually ran;
// NOTE: to log an entry, logging has to be enabled in the task setting AND log_cron_action needs to be called; by default, logging is DISABLED for hourly tasks, and enabled for daily && all other tasks;

// get special phrase groups
$phrasegroups = array('cron');

// get special data templates from the datastore
$specialtemplates = array('smiliecache', 'bbcodecache');

// pre-cache templates used by specific actions
$actiontemplates = array();

require_once('./global.php');								// UNCOMMENT to test outside of cron;
require_once(DIR . '/includes/functions_cron.php');

$nextitem = array();										// array for log variables;
$nextitem['loglevel'] = 1;
$nextitem['varname'] = 'ldm_vbamodules';
*/

error_reporting(E_ALL & ~E_NOTICE);
$globaltemplates = array(
'adv_portal_custom_ldm_hot_audio', 'adv_portal_custom_ldm_hot_audio_one', 
'adv_portal_custom_ldm_hot_media', 'adv_portal_custom_ldm_hot_media_one', 
'adv_portal_custom_ldm_hot_photo', 'adv_portal_custom_ldm_hot_photo_one', 
'adv_portal_custom_ldm_hot_link', 'adv_portal_custom_ldm_hot_link_one', 
'adv_portal_custom_ldm_rated_audio', 'adv_portal_custom_ldm_rated_audio_one', 
'adv_portal_custom_ldm_rated_media', 'adv_portal_custom_ldm_rated_media_one', 
'adv_portal_custom_ldm_rated_photo', 'adv_portal_custom_ldm_rated_photo_one', 
'adv_portal_custom_ldm_rated_link', 'adv_portal_custom_ldm_rated_link_one', 
'links_JWplayer_header', 'links_JWwmplayer_header', 
'links_playerbit_JWPlayer', 'links_playerbit_JWwmPlayer', 
'links_imgmag', 'links_imgrate', );

require_once(DIR . '/includes/local_links_init.php');
require_once(DIR . '/includes/local_links_include.php');
require_once(DIR . '/includes/local_links_vbafunc.php');

global $links_defaults, $style, $stylevar;
$vbulletin->datastore->fetch(array('adv_portal_opts'));
$cmps_options =& $vbulletin->adv_portal_opts;

// if cron is run manually, the script runs globally in cronadmin.php and $style is not set (since adminCP has separate $style);
// if cron is run automatically, the script runs within a function in cron.php and $style is set but needs to be globalized;
if (!is_array($style)) {				// MUST check $style for manual cron; query simplified && extracted from global.php;
	
	$style = $vbulletin->db->query_first_slave("
		SELECT * FROM " . TABLE_PREFIX . "style 
		WHERE styleid = " . $vbulletin->options['styleid'] . " LIMIT 1");
	
	define('STYLEID', $style['styleid']);
	$stylevar = fetch_stylevars($style, $vbulletin->userinfo);
}
cache_templates($globaltemplates, $style['templatelist']);
// need to add to $specialtemplates in index.php: 'ldm_hot_audio', 'ldm_hot_media', 'ldm_hot_photo', 'ldm_hot_link', 'ldm_rated_audio', 'ldm_rated_media', 'ldm_rated_photo', 'ldm_rated_link'

$hitssince = $links_defaults['days_seen_on_portal'] ? time() - intval($links_defaults['days_seen_on_portal'])*24*60*60 : 0;
$ldm_aspect_ratio = $links_defaults['inlineJWplayer_video_width'] ? $links_defaults['inlineJWplayer_video_height'] / $links_defaults['inlineJWplayer_video_width'] : 1;

$mod_options = $cmps_options['adv_portal_ldm_rated_media'];
$links_defaults['xmlJWplayer_display_height'] = $mod_options['portal_ldm_rated_media_showthumbsize']*$ldm_aspect_ratio;
$links_defaults['xmlJWplayer_display_width'] = $mod_options['portal_ldm_rated_media_showthumbsize'];

$links_defaults['inlineJWplayer_video_height'] = $mod_options['portal_ldm_rated_media_showthumbsize']*$ldm_aspect_ratio;
$links_defaults['inlineJWplayer_video_width'] = $mod_options['portal_ldm_rated_media_showthumbsize'];

$links_defaults['inlineJWplayer_audio_height'] = $mod_options['portal_ldm_rated_media_showthumbsize']*$ldm_aspect_ratio;	
$links_defaults['inlineJWplayer_audio_width'] = $mod_options['portal_ldm_rated_media_showthumbsize'];

//	-------------------------------------------------------------------------------------------------------

$filetypes = $mod_options["portal_ldm_hot_audio_showfiletypes"] ? $vbulletin->db->escape_string($mod_options["portal_ldm_hot_audio_showfiletypes"]) : "xml|m4a|mp3|wma";
$filter	= array("link.linkmoderate = 0", "link.linkhits > 0", "link.linkurl REGEXP '(" . $filetypes . ")$'");
$order	= "linkrecenthits DESC";

$mod_options = $cmps_options['adv_portal_ldm_hot_audio'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, $hitssince, $order, "adv_portal_custom_ldm_hot_audio_one",
		$mod_options["portal_ldm_hot_audio_showcategories"],
		$mod_options["portal_ldm_hot_audio_showentries"],
		$mod_options["portal_ldm_hot_audio_showperrow"],
		$mod_options["portal_ldm_hot_audio_showsubcats"],
		0, 1);
build_datastore('ldm_hot_audio', $links);

$filetypes = $mod_options["portal_ldm_hot_media_showfiletypes"] ? $vbulletin->db->escape_string($mod_options["portal_ldm_hot_media_showfiletypes"]) : "flv|m4v|mp4|wmv";
$filter	= array("link.linkmoderate = 0", "link.linkhits > 0", "link.linkurl REGEXP '(" . $filetypes . ")$'");
$order	= "linkrecenthits DESC";

$mod_options = $cmps_options['adv_portal_ldm_hot_media'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, $hitssince, $order, "adv_portal_custom_ldm_hot_media_one",
		$mod_options["portal_ldm_hot_media_showcategories"],
		$mod_options["portal_ldm_hot_media_showentries"],
		$mod_options["portal_ldm_hot_media_showperrow"],
		$mod_options["portal_ldm_hot_media_showsubcats"],
		0, 1);
build_datastore('ldm_hot_media', $links);

// linkimg must == linkurl to filter out files with attached photos, and linkimgstatus must == 2 to filter out mp3 ID3 images;
$filter	= array("link.linkmoderate = 0", "link.linkimgthumb != ''", "link.linkimg = link.linkurl", "link.linkimgstatus = 2", "link.linkhits > 0");
$order	= "linkrecenthits DESC ";

$mod_options = $cmps_options['adv_portal_ldm_hot_photo'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, $hitssince, $order, "adv_portal_custom_ldm_hot_photo_one",
		$mod_options["portal_ldm_hot_photo_showcategories"],
		$mod_options["portal_ldm_hot_photo_showentries"],
		$mod_options["portal_ldm_hot_photo_showperrow"],
		$mod_options["portal_ldm_hot_photo_showsubcats"],
		0, 1);
build_datastore('ldm_hot_photo', $links);

$filter	= array("link.linkmoderate = 0", "link.linkstatus = 1");
$order	= "linkhits DESC ";

// only overall hits are recorded for links, so can not limit by $hitssince;
$mod_options = $cmps_options['adv_portal_ldm_hot_link'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_hot_link_one",
		$mod_options["portal_ldm_hot_link_showcategories"],
		$mod_options["portal_ldm_hot_link_showentries"],
		$mod_options["portal_ldm_hot_link_showperrow"],
		$mod_options["portal_ldm_hot_link_showsubcats"],
		0, 1);
build_datastore('ldm_hot_link', $links);

//	----------------------------------------------------------------------

$filetypes = $mod_options["portal_ldm_rated_audio_showfiletypes"] ? $vbulletin->db->escape_string($mod_options["portal_ldm_rated_audio_showfiletypes"]) : "xml|m4a|mp3|wma";
$filter	= array("link.linkmoderate = 0", "link.linkurl REGEXP '(" . $filetypes . ")$'",);
$order	= "vbb_voteavg DESC ";

$mod_options = $cmps_options['adv_portal_ldm_rated_audio'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_rated_audio_one",
		$mod_options["portal_ldm_rated_audio_showcategories"],
		$mod_options["portal_ldm_rated_audio_showentries"],
		$mod_options["portal_ldm_rated_audio_showperrow"],
		$mod_options["portal_ldm_rated_audio_showsubcats"],
		0, 1);
build_datastore('ldm_rated_audio', $links);

$filetypes = $mod_options["portal_ldm_rated_media_showfiletypes"] ? $vbulletin->db->escape_string($mod_options["portal_ldm_rated_media_showfiletypes"]) : "flv|m4v|mp4|wmv";
$filter	= array("link.linkmoderate = 0", "link.linkurl REGEXP '(" . $filetypes . ")$'",);
$order	= "vbb_voteavg DESC ";

$mod_options = $cmps_options['adv_portal_ldm_rated_media'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_rated_media_one",
		$mod_options["portal_ldm_rated_media_showcategories"],
		$mod_options["portal_ldm_rated_media_showentries"],
		$mod_options["portal_ldm_rated_media_showperrow"],
		$mod_options["portal_ldm_rated_media_showsubcats"],
		0, 1);
build_datastore('ldm_rated_media', $links);

// linkimg must == linkurl to filter out files with attached photos, and linkimgstatus must == 2 to filter out mp3 ID3 images;
$filter	= array("link.linkmoderate = 0", "link.linkimgthumb != ''", "link.linkimg = link.linkurl", "link.linkimgstatus = 2");
$order	= "vbb_voteavg DESC ";

$mod_options = $cmps_options['adv_portal_ldm_rated_photo'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_rated_photo_one",
		$mod_options["portal_ldm_rated_photo_showcategories"],
		$mod_options["portal_ldm_rated_photo_showentries"],
		$mod_options["portal_ldm_rated_photo_showperrow"],
		$mod_options["portal_ldm_rated_photo_showsubcats"],
		0, 1);
build_datastore('ldm_rated_photo', $links);

$filter	= array("link.linkmoderate = 0", "link.linkstatus = 1");
$order	= "linkvoteavg DESC ";

$mod_options = $cmps_options['adv_portal_ldm_rated_link'];
list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_rated_link_one",
		$mod_options["portal_ldm_rated_link_showcategories"],
		$mod_options["portal_ldm_rated_link_showentries"],
		$mod_options["portal_ldm_rated_link_showperrow"],
		$mod_options["portal_ldm_rated_link_showsubcats"],
		0, 1);
build_datastore('ldm_rated_link', $links);

log_cron_action('Updated LDM VBA modules cache.', $nextitem);

?>