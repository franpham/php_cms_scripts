<?php

require_once('./global.php');
require_once(DIR . '/includes/local_links_init.php');
require_once(DIR . '/includes/local_links_include.php');
require_once(DIR . '/includes/local_links_vbafunc.php');

$pagenumber = max(intval($_REQUEST['ldmnew']), 1); 
$show_catname = $mod_options['portal_ldm_new_link_showcatname'];

$filter	= array("link.linkmoderate = 0", "link.linkstatus = 1");
$order	= "linkdate DESC ";

list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_new_link_one",
		$mod_options["portal_ldm_new_link_showcategories"],
		$mod_options["portal_ldm_new_link_showentries"],
		$mod_options["portal_ldm_new_link_showperrow"],
		$mod_options["portal_ldm_new_link_showsubcats"],
		$pagenumber);

if ($mod_options["portal_ldm_new_link_showpagenav"]) {
	$ldm_thesegot = array();
	foreach ($_GET as $thisget=>$thisgot) {
		if ($thisget!="ldmnew" and preg_match("/^(pageid|ldm)/", $thisget)) {
			$ldm_thesegot[] = "$thisget=$thisgot";
		}
	}
	$ldm_script = "";
	if (count($ldm_thesegot)) {
		$ldm_script = "&amp;".implode('&amp;', $ldm_thesegot);
	}
	$ldm_pagenavpages = $GLOBALS['vbulletin']->options['pagenavpages'];
	$GLOBALS['vbulletin']->options['pagenavpages'] = 2;
	$ldm_new_link_pagenav = construct_page_nav($pagenumber, $mod_options["portal_ldm_new_link_showentries"], $nhits, $_SERVER['PHP_SELF'].'?', $ldm_script);
	$ldm_new_link_pagenav = preg_replace("/page=/", "ldmnew=", $ldm_new_link_pagenav);
	$ldm_new_link_pagenav = preg_replace("/<td.*?<a name=\"PageNav\"><.a><.td>/", "", $ldm_new_link_pagenav);
	$GLOBALS['vbulletin']->options['pagenavpages'] = $ldm_pagenavpages;
}

$collapseobj_custom_ldm_new_link = $vbcollapse['collapseobj_custom_ldm_new_link'];
$collapseimg_custom_ldm_new_link = $vbcollapse['collapseimg_custom_ldm_new_link'];

eval('$home[$mods[\'modid\']][\'content\'] .= "' . fetch_template('adv_portal_custom_ldm_new_link') . '";');
unset($filter, $order, $links, $nhits, $q, $collapseobj_custom_ldm_new_link, $collapseimg_custom_ldm_new_link);

?>
