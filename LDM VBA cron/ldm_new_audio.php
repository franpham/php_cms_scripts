<?php

require_once('./global.php');
require_once(DIR . '/includes/local_links_init.php');
require_once(DIR . '/includes/local_links_include.php');
require_once(DIR . '/includes/local_links_vbafunc.php');

$pagenumber = max(intval($_REQUEST['ldmnew']), 1); 
$show_catname = $mod_options['portal_ldm_new_audio_showcatname'];

// upload_filetypes for VIP Audios category;
$filetypes = "aac|m3u|m4a|mid|mp3|ra|ram|wma|xml";

$filter	= array("link.linkmoderate = 0", "link.linkurl REGEXP '(" . $filetypes . ")$'",);
$order	= "linkdate DESC ";

list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_new_audio_one",
		$mod_options["portal_ldm_new_audio_showcategories"],
		$mod_options["portal_ldm_new_audio_showentries"],
		$mod_options["portal_ldm_new_audio_showperrow"],
		$mod_options["portal_ldm_new_audio_showsubcats"],
		$pagenumber);

if ($mod_options["portal_ldm_new_audio_showpagenav"]) {
	$ldm_thesegot = array();
	foreach ($_GET as $thisget=>$thisgot) {
		if ($thisget!="ldmnew" and preg_match("/^(pageid|ldm)/", $thisget)) {
			$ldm_thesegot[] = "$thisget=$thisgot";
		}
	}
	$ldm_script = "";
	if (count($ldm_thesegot)) {
		$ldm_script = "&amp;".implode('&amp;', $ldm_thesegot);
	}
	$ldm_pagenavpages = $GLOBALS['vbulletin']->options['pagenavpages'];
	$GLOBALS['vbulletin']->options['pagenavpages'] = 2;
	$ldm_new_audio_pagenav = construct_page_nav($pagenumber, $mod_options["portal_ldm_new_audio_showentries"], $nhits, $_SERVER['PHP_SELF'].'?', $ldm_script);
	$ldm_new_audio_pagenav = preg_replace("/page=/", "ldmnew=", $ldm_new_audio_pagenav);
	$ldm_new_audio_pagenav = preg_replace("/<td.*?<a name=\"PageNav\"><.a><.td>/", "", $ldm_new_audio_pagenav);
	$GLOBALS['vbulletin']->options['pagenavpages'] = $ldm_pagenavpages;
}

$collapseobj_custom_ldm_new_audio = $vbcollapse['collapseobj_custom_ldm_new_audio'];
$collapseimg_custom_ldm_new_audio = $vbcollapse['collapseimg_custom_ldm_new_audio'];

eval('$home[$mods[\'modid\']][\'content\'] .= "' . fetch_template('adv_portal_custom_ldm_new_audio') . '";');
unset($filter, $order, $links, $nhits, $q, $collapseobj_custom_ldm_new_audio, $collapseimg_custom_ldm_new_audio);

?>
