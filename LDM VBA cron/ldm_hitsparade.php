<?php
/*
// MYSQL datastore query: SELECT * FROM datastore WHERE title NOT IN ('pluginlist', 'pluginlistadmin') ORDER BY title
// NOTE: EACH cron task requires a page to be accessed AT ITS SPECIFIC TIME in order for the task to run;
// NOTE: the Task Manager will show when a task was SUPPOSED to run, but only the Task Logger can verify if it actually ran;
// NOTE: to log an entry, logging has to be enabled in the task setting AND log_cron_action needs to be called; by default, logging is DISABLED for hourly tasks, and enabled for daily && all other tasks;

// get special phrase groups
$phrasegroups = array('cron');

// get special data templates from the datastore
$specialtemplates = array('smiliecache', 'bbcodecache');

// pre-cache templates used by specific actions
$actiontemplates = array();

require_once('./global.php');								// UNCOMMENT to test outside of cron;
require_once(DIR . '/includes/functions_cron.php');

$nextitem = array();										// array for log variables;
$nextitem['loglevel'] = 1;
$nextitem['varname'] = 'ldm_hitsparade';
*/

error_reporting(E_ALL & ~E_NOTICE);
$globaltemplates = array('links_linkbit_photo', 'links_linkbit_photo2', 'links_imgmag');

require_once(DIR . '/includes/local_links_init.php');
require_once(DIR . '/includes/local_links_include.php');
require_once(DIR . '/includes/local_links_vbafunc.php');
global $FHOT_CAT, $HOT_CAT, $NEW_FRATE, $NEW_RATE, $links_defaults, $style, $stylevar;

// if cron is run manually, the script runs globally in cronadmin.php and $style is not set (since adminCP has separate $style);
// if cron is run automatically, the script runs within a function in cron.php and $style is set but needs to be globalized;
if (!is_array($style)) {				// MUST check $style for manual cron; query simplified && extracted from global.php;

	$style = $vbulletin->db->query_first_slave("
		SELECT * FROM " . TABLE_PREFIX . "style 
		WHERE styleid = " . $vbulletin->options['styleid'] . " LIMIT 1");
	
	define('STYLEID', $style['styleid']);
	$stylevar = fetch_stylevars($style, $vbulletin->userinfo);
}
cache_templates($globaltemplates, $style['templatelist']);
// need to add to $specialtemplates in index.php: 'ldm_tagcloud', 'ldm_admin', 'ldm_cats'
// need to add to $specialtemplates in local_links.php: 'ldm_FHOT_CAT', 'ldm_HOT_CAT', 'ldm_NEW_FRATE', 'ldm_NEW_RATE'

	$links = array();
	$linksee = (isset($links_defaults['links_seen_on_portal']) ? $links_defaults['links_seen_on_portal'] : 5);
	$newsince = $hitsince = ($links_defaults['days_seen_on_portal'] ? TIMENOW-intval($links_defaults['days_seen_on_portal'])*24*60*60 : $vbulletin->userinfo['lastvisit']);
		
	$query = ldm_get_specialsearchsql($FHOT_CAT, $linksee, $links_defaults['days_seen_on_portal'], 0, 0, $hitsince, 0, array("link.linkimgthumb != ''"));
	list($links['show_hp_top'], $hits) = ldm_get_entrybits_brief($query, $linksee, 'links_linkbit_photo2');
	build_datastore('ldm_FHOT_CAT', $links['show_hp_top']);

	$query = ldm_get_specialsearchsql($HOT_CAT, $linksee, 'h', 0);
	list($links['show_hp_topall'], $hits) = ldm_get_entrybits_brief($query, $linksee, 'links_linkbit_photo2');
	build_datastore('ldm_HOT_CAT', $links['show_hp_topall']);

	$query = ldm_get_specialsearchsql($NEW_FRATE, $linksee, 'r', 0, $newsince, 0, 0, array("link.linkimgthumb != ''"));
	list($links['show_hp_random'], $hits) = ldm_get_entrybits_brief($query, $linksee, 'links_linkbit_photo');
	build_datastore('ldm_NEW_FRATE', $links['show_hp_random']);

	$query = ldm_get_specialsearchsql($NEW_RATE, $linksee, 'r', 0);
	list($links['show_hp_newcomment'], $hits) = ldm_get_entrybits_brief($query, $linksee, 'links_linkbit_photo');
	build_datastore('ldm_NEW_RATE', $links['show_hp_newcomment']);

	if ($links_defaults['tagcloud_active']) {		// rebuild the tagcloud;
		ldm_get_tagcloud($links_defaults['tagcloud_entries'],
			$links_defaults['tagcloud_logcurve'], $links_defaults['tagcloud_minfont'],
			$links_defaults['tagcloud_maxfont'], $links_defaults['tagcloud_byhits'], '', true);
	}

log_cron_action("Updated LDM hits parade cache.", $nextitem);

?>