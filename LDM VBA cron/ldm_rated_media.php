<?php
	
require_once('./global.php');
require_once(DIR . '/includes/local_links_init.php');
require_once(DIR . '/includes/local_links_include.php');
require_once(DIR . '/includes/local_links_vbafunc.php');
global $vbulletin;

$pagenumber = max(intval($_REQUEST['ldmnewflashthumb']), 1); 
$show_catname = $mod_options['portal_ldm_rated_media_showcatname'];

$filetypes = $mod_options["portal_ldm_rated_media_showfiletypes"] ? $vbulletin->db->escape_string($mod_options["portal_ldm_rated_media_showfiletypes"]) : "flv|m4v|mp4|wmv";

$filter	= array("link.linkmoderate = 0", "link.linkurl REGEXP '(" . $filetypes . ")$'",);
$order	= "vbb_voteavg DESC ";

$GLOBALS['links_defaults']['vba_link_imagesize'] = $mod_options['portal_ldm_rated_media_showthumbsize'] ? $mod_options['portal_ldm_rated_media_showthumbsize'] : $GLOBALS['links_defaults']['link_imagesize'];

$ldm_aspect_ratio = $GLOBALS['links_defaults']['inlineJWplayer_video_width'] ? $GLOBALS['links_defaults']['inlineJWplayer_video_height'] / $GLOBALS['links_defaults']['inlineJWplayer_video_width'] : 1;

// NEW CODE: wmp && xml players also uses inlineJWplayer width && height; xmlJWplayer_display_width && height are unique;
$GLOBALS['links_defaults']['xmlJWplayer_display_height'] = $mod_options['portal_ldm_rated_media_showthumbsize']* $ldm_aspect_ratio;
$GLOBALS['links_defaults']['xmlJWplayer_display_width'] = $mod_options['portal_ldm_rated_media_showthumbsize'];

$GLOBALS['links_defaults']['inlineJWplayer_video_height'] = $mod_options['portal_ldm_rated_media_showthumbsize']*$ldm_aspect_ratio;
$GLOBALS['links_defaults']['inlineJWplayer_video_width'] = $mod_options['portal_ldm_rated_media_showthumbsize'];

$GLOBALS['links_defaults']['inlineJWplayer_audio_height'] = $mod_options['portal_ldm_rated_media_showthumbsize']*$ldm_aspect_ratio;	
$GLOBALS['links_defaults']['inlineJWplayer_audio_width'] = $mod_options['portal_ldm_rated_media_showthumbsize'];

$links = $vbulletin->ldm_rated_media;
if ($links == '') {
	list ($links, $nhits, $q) =
	ldm_vba_links($filter, 0, $order, "adv_portal_custom_ldm_rated_media_one",
		$mod_options["portal_ldm_rated_media_showcategories"],
		$mod_options["portal_ldm_rated_media_showentries"],
		$mod_options["portal_ldm_rated_media_showperrow"],
		$mod_options["portal_ldm_rated_media_showsubcats"],
		0, $pagenumber);
}

if ($mod_options["portal_ldm_rated_media_showpagenav"] && false) {	// MUST SAVE $nhits (in settings like rating mod) if showing;
	$ldm_thesegot = array();
	foreach ($_GET as $thisget=>$thisgot) {
		if ($thisget!="ldmnewflashthumb" and preg_match("/^(pageid|ldm)/", $thisget)) {
			$ldm_thesegot[] = "$thisget=$thisgot";
		}
	}
	$ldm_script = "";
	if (count($ldm_thesegot)) {
		$ldm_script = "&amp;".implode('&amp;', $ldm_thesegot);
	}
	$ldm_pagenavpages = $GLOBALS['vbulletin']->options['pagenavpages'];
	$GLOBALS['vbulletin']->options['pagenavpages'] = 2;
	$ldm_rated_media_pagenav = construct_page_nav($pagenumber, $mod_options["portal_ldm_rated_media_showentries"], $nhits, $_SERVER['PHP_SELF'].'?', $ldm_script);
	$ldm_rated_media_pagenav = preg_replace("/page=/", "ldmnewflashthumb=", $ldm_rated_media_pagenav);
	$ldm_rated_media_pagenav = preg_replace("/<td.*?<a name=\"PageNav\"><.a><.td>/", "", $ldm_rated_media_pagenav);
	$GLOBALS['vbulletin']->options['pagenavpages'] = $ldm_pagenavpages;
}

$collapseobj_custom_ldm_rated_media = $vbcollapse['collapseobj_custom_ldm_rated_media'];
$collapseimg_custom_ldm_rated_media = $vbcollapse['collapseimg_custom_ldm_rated_media'];

if ($GLOBALS['links_defaults']['inlineJWplayer_active']) {
	eval('$ldm_flash_js = "' . fetch_template('links_JWplayer_header') . '";');
	eval('$ldm_flash_js .= "' . fetch_template('links_JWwmplayer_header') . '";');
}
eval('$home[$mods[\'modid\']][\'content\'] .= "' . fetch_template('adv_portal_custom_ldm_rated_media') . '";');
unset($filter, $order, $links, $nhits, $q, $collapseobj_custom_ldm_rated_media, $collapseimg_custom_ldm_rated_media);

?>