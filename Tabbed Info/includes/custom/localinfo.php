<?php

	$profilefields = $db->query_read_slave("SELECT *
		FROM " . TABLE_PREFIX . "profilefield
		WHERE searchable = 1 AND form = 0
			AND profilefieldid = 11
			" . iif(!($permissions['genericpermissions'] & $vbulletin->bf_ugp_genericpermissions['canseehiddencustomfields']), " AND hidden = 0") . "
		ORDER BY displayorder
	");
	$field11 = $vbulletin->input->clean_gpc('r', 'field11', TYPE_STR);
	
	// DO NOT check !$field11 or empty($field11) since 'Metro' option value == '0' allows users to see all results when selected, while filtering results initially;
	// if ($field11 == '')				// disabled since forums will be initially sparse;	------------------------	UNCOMMENT LATER			--------------------
	//	$field11 = $vbulletin->GPC['field11'] = $vbulletin->userinfo['metrolocale'];
	
	while ($profilefield = $db->fetch_array($profilefields)) {
		$profilefieldid = $profilefield['profilefieldid'];
		$profilefieldname = "field$profilefieldid";
		if ($profilefield['type'] == 'select') {
			$data = unserialize($profilefield['data']);
			$profilefield['def'] = 0;
			$selectbits = '';
			
			// IMPORTANT: both metro && service fields can not have single or double quotes since they are in option's value="" attribute,
			// which can use either single or double quotes; also since they belong to a form, the browser will urlencode the string;
			foreach ($data AS $optionvalue => $optiontitle) {
				$optionvalue = $optiontitle;			// set value == title;
				$optionselected = ($profilefieldid == 11 && $field11 == $optionvalue) ? 'selected="selected"' : '';
				if ($optionselected)
					$profilefield['def'] = 2;			// memberlist uses value 2 to not select default option;
				eval('$selectbits .= "' . fetch_template('option') . '";');
			}
			if ($profilefieldid == 11) {
				$profilefield['title'] = $vbphrase['metrolocale'];
				eval('$metroselect = "' . fetch_template('memberlist_searchmenu_select') . '";');
			}
		}
	}
	$db->free_result($profilefields);

if (strpos($vbphrase['tabinfo_your_groups'], 'My ') === 0)
	$vbphrase['tabinfo_your_groups'] = substr($vbphrase['tabinfo_your_groups'], 3);
if (strpos($vbphrase['tabinfo_your_media'], 'My ') === 0)
	$vbphrase['tabinfo_your_media'] = substr($vbphrase['tabinfo_your_media'], 3);
if (strpos($vbphrase['tabinfo_your_last_albums'], 'My ') === 0)
	$vbphrase['tabinfo_your_last_albums'] = substr($vbphrase['tabinfo_your_last_albums'], 3);
if (strpos($vbphrase['tabinfo_your_last_threads'], 'My ') === 0)
	$vbphrase['tabinfo_your_last_threads'] = substr($vbphrase['tabinfo_your_last_threads'], 3);

// max number of x to show 
$threadscount= intval($vbulletin->options['latest_threads_count']);  
$postscount  = intval($vbulletin->options['latest_posts_count']);  
$groupscount = intval($vbulletin->options['latest_groups_count']);  
$albumscount = intval($vbulletin->options['latest_albums_count']);  
$userid = $vbulletin->userinfo['userid'];				// used by footer links;
$username = $vbulletin->userinfo['username'];			// shown for no entries;

$metrolocale = $field11;								// renamed for convenience;
$metrolocale = $metrolocale ? " AND userfield.field11 = '" . $vbulletin->db->escape_string($metrolocale) . "' " : '';

// max character title to be cut 
$threads_posts_max_title = intval($vbulletin->options['threads_posts_max_title']); 
$forums_name_max_title = intval($vbulletin->options['latest_forums_max_title']); 
$albums_name_max_title = $groups_name_max_title = intval($vbulletin->options['groups_name_max_title']); 
$max_description = intval($vbulletin->options['groups_description_max_title']); 

$excludedforums = '';   
if ($vbulletin->options['latest_ignore_forumids'] != '') {
	$excludedforums = ',' . $vbulletin->options['latest_ignore_forumids'];   
}
$forumpermissions = array();   
foreach($vbulletin->forumcache AS $forum) {
	$forumpermissions[$forum["forumid"]] = fetch_permissions($forum['forumid']);   
	if (!($forumpermissions[$forum["forumid"]] & $vbulletin->bf_ugp_forumpermissions['canview']) AND !$vbulletin->options['showprivateforums'])
		$excludedforums = $excludedforums . ',' . $forum['forumid'];
}
unset($forum, $forumpermissions);		// unset to prevent accidental usage; 
if ($excludedforums != '') {
	$excludedforums = substr($excludedforums, 1);
	$excludedforums = " AND thread.forumid NOT IN ($excludedforums) ";
}

// get last X threads; NEW CODE: do not show sensitive forums (parentid != 29);
if ($vbulletin->options['enable_latest_threads']) {
	$lastthreads = $vbulletin->db->query_read_slave("SELECT  
		thread.title, thread.taglist, thread.prefixid, thread.taglist as tag, thread.threadid, 
		thread.attach, icon.title AS icontitle, icon.iconpath, thread.lastpost, thread.lastpostid, 
		thread.forumid, thread.replycount, thread.lastposter, thread.dateline, thread.iconid, 
		IF(thread.views<=thread.replycount, thread.replycount+1, thread.views) AS views, 
		thread.visible, user.username, user.userid, user.usergroupid, 
		IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, forum.title_clean as forum_title  
		FROM " . TABLE_PREFIX . "thread AS thread  
		INNER JOIN " . TABLE_PREFIX . "forum as forum ON (thread.forumid = forum.forumid)  
		LEFT JOIN " . TABLE_PREFIX . "user AS user ON (user.username = thread.lastposter)  
		LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
		LEFT JOIN " . TABLE_PREFIX . "icon AS icon ON(icon.iconid = thread.iconid)  
		WHERE thread.visible = '1' 
		AND forum.parentid != 29 $excludedforums $metrolocale
		ORDER BY thread.dateline DESC 
		LIMIT 0, $threadscount 
	"); 
  $lastthreadsbit = ''; 
   
  while ($lastthread = $vbulletin->db->fetch_array($lastthreads))  
  { 
	$lastthread['title'] = substr($lastthread['title'], 0, $threads_posts_max_title); 
	$lastthread['forum_title'] = substr($lastthread['forum_title'], 0, $forums_name_max_title);  
	$lastthread['musername'] = fetch_musername($lastthread);
	if ($lastthread['lastpost'] > $vbulletin->userinfo['lastvisit']) 
		$lastthread['newpost'] = true;  
	
	$show['threadicon'] = false;	
	$lastthread['iconpath'] = '';	
	$lastthread['icontitle'] = '';	
	if ($lastthread['iconid'])	
	{	
	  $show['threadicon'] = true;	
	  $lastthread['iconpath'] = $vbulletin->iconcache["$lastthread[iconid]"]['iconpath'];	
	  $lastthread['icontitle'] = $vbulletin->iconcache["$lastthread[iconid]"]['title'];	
	}
	if ($lastthread['pollid'] != 0)	
	{	
	  $show['threadicon'] = true;	
	  $lastthread['iconpath'] = "$stylevar[imgdir_misc]/poll_posticon.gif";	
	  $lastthread['icontitle'] = $vbphrase['poll'];	
	}	
	elseif (!empty($vbulletin->options['showdeficon']))	
	{ 
	  $show['threadicon'] = true;	
	  $lastthread['iconpath'] = $vbulletin->options['showdeficon'];	
	  $lastthread['icontitle'] = '';	
	}	
	if ($lastthread['attach'] > 0)   
	{   
	  $show['paperclip'] = true;   
	  $lastthread['checkbox_value'] += THREAD_FLAG_ATTACH;   
	}
	$show['taglist'] = ($vbulletin->options['threadtagging'] AND !empty($lastthread['tag']));  
	$gnpdate = vbdate($vbulletin->options['dateformat'], $lastthread[lastpost], true);   
	$gnptime = vbdate($vbulletin->options['timeformat'], $lastthread[lastpost]);  
	eval("\$lastthreadsbit .= \"".fetch_template('tabinfo_lastthreadsbit')."\";"); 
  }
  $vbulletin->db->free_result($lastthreads);
  eval("\$tabinfolastthreads = \"".fetch_template('tabinfo_lastthreads')."\";");
}


// get last X posts 
if ($vbulletin->options['enable_latest_posts']) {
	$lastposts = $vbulletin->db->query_read_slave(" 
	SELECT thread.threadid, thread.title AS threadtitle, thread.forumid, thread.replycount, thread.pollid, thread.iconid, 
	icon.title AS icontitle, icon.iconpath, thread.postusername, thread.postuserid, thread.dateline AS threaddate, thread.views, thread.visible,  
	post.threadid, post.username, post.userid, post.dateline AS lastpostdate, forum.forumid, forum.title AS forumtitle,  
	user.userid, user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid 
	FROM " . TABLE_PREFIX . "post AS post 
	INNER JOIN " . TABLE_PREFIX . "thread AS thread ON(thread.threadid = post.threadid) 
	INNER JOIN " . TABLE_PREFIX . "forum AS forum ON(forum.forumid = thread.forumid) 
	LEFT JOIN " . TABLE_PREFIX . "user AS user ON(user.userid = thread.postuserid) 
	LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
	LEFT JOIN " . TABLE_PREFIX . "icon AS icon ON(icon.iconid = thread.iconid)  
	WHERE thread.visible = '1' 
	AND forum.parentid != 29 $excludedforums $metrolocale
	ORDER BY lastpostdate DESC 
	LIMIT 0, $postscount 
	"); 
	$lastpostsbit = ''; 
	while($lastpost = $vbulletin->db->fetch_array($lastposts)) 
	{ 
		$show['threadicon'] = false;  
		$lastpost['iconpath'] = '';  
		$lastpost['icontitle'] = '';  
		if ($lastpost['iconid'])  
		{   // get icon from icon cache  
			$show['threadicon'] = true;  
			$lastpost['iconpath'] = $vbulletin->iconcache["$lastpost[iconid]"]['iconpath'];  
			$lastpost['icontitle'] = $vbulletin->iconcache["$lastpost[iconid]"]['title'];  
		}  
		if ($lastpost['pollid'] != 0)  
		{   // show poll icon  
			$show['threadicon'] = true;  
			$lastpost['iconpath'] = "$stylevar[imgdir_misc]/poll_posticon.gif";  
			$lastpost['icontitle'] = $vbphrase['poll'];  
		}  
		else if (!empty($vbulletin->options['showdeficon']))  
		{   // show default icon  
			$show['threadicon'] = true;  
			$lastpost['iconpath'] = $vbulletin->options['showdeficon'];  
			$lastpost['icontitle'] = '';  
		}  
		$threadtitle = htmlspecialchars_uni(substr($lastpost['threadtitle'], 0, $threads_posts_max_title)); 
		$forumtitle = htmlspecialchars_uni(substr($lastpost['forumtitle'], 0, $forums_name_max_title)); 
		$lastpostedate = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastpost['lastpostdate'], true); 
		$replies = vb_number_format($lastpost['replycount']); 
		$views = vb_number_format($lastpost['views']); 
		$threadby = $lastpost['postusername']; 
		$threaddate = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastpost['threaddate'], true); 
		$lastpost['musername'] = fetch_musername($lastpost);  
		eval('$lastpostsbit .= "' . fetch_template('tabinfo_lastpostsbit') . '";'); 
	}
	$vbulletin->db->free_result($lastposts);
	eval('$tabinfolastposts = "' . fetch_template('tabinfo_lastposts') . '";');
}

// get last X groups 
if ($vbulletin->options['enable_latest_groups']) {
	require_once(DIR . '/includes/functions_socialgroup.php');
	$lastgroups = $vbulletin->db->query_read_slave(" 
	SELECT socialgroup.*, socialgroup.dateline AS createdate, 
	socialgroup.groupid, socialgroup.name, socialgroup.description, socialgroup.dateline, sgicon.dateline AS icondateline,
	sgicon.thumbnail_width AS iconthumb_width, sgicon.thumbnail_height AS iconthumb_height,
	user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid  
	FROM " . TABLE_PREFIX . "socialgroup AS socialgroup 
	LEFT JOIN " . TABLE_PREFIX ."socialgroupicon AS sgicon ON (sgicon.groupid = socialgroup.groupid)
	LEFT JOIN " . TABLE_PREFIX . "user AS user ON(user.userid = socialgroup.creatoruserid)  
	LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
	WHERE socialgroup.type IN ('public', 'moderated') $metrolocale
	ORDER BY socialgroup.dateline DESC 
	LIMIT 0, $groupscount 
	"); 
	$lastgroupsbit = ''; 
	while($lastgroup = $vbulletin->db->fetch_array($lastgroups)) 
	{ 
		$groupname = substr($lastgroup['name'], 0, $groups_name_max_title); 
		$description = htmlspecialchars_uni(fetch_censored_text(fetch_trimmed_title(
			strip_bbcode($lastgroup['description'], true, true, false), $max_description)));
		$dateline = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastgroup['lastupdate'], true); 
		$members = vb_number_format($lastgroup['members']); 
		$messages = vb_number_format($lastgroup['visible']); 
		$pictures = vb_number_format($lastgroup['picturecount']); 
		$lastgroup['musername'] = fetch_musername($lastgroup);  
		$lastgroup['iconurl'] = fetch_socialgroupicon_url($lastgroup, true);
		eval('$lastgroupsbit .= "' . fetch_template('tabinfo_lastgroupsbit') . '";'); 
	}
	$vbulletin->db->free_result($lastgroups);
	eval('$tabinfolastgroups = "' . fetch_template('tabinfo_lastgroups') . '";');
}

// get latest media entries;
if ($vbulletin->options['tabinfo_media_count']) {
	require_once(DIR . '/includes/local_links_init.php');
	require_once(DIR . '/includes/local_links_include.php');
	
	$media = $vbulletin->db->query_read("
		SELECT ltoc.catid, link.linkid, linkurl, linkname, linkdesc, linkthread, linkimgthumb, linkhits, linkdate, linksize, link.vbb_votenum,
		IF(link.vbb_votenum > 0, (link.vbb_votetotal / link.vbb_votenum), 0) AS vbb_voteavg,
		user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid  
		FROM " .THIS_TABLE. "linkslink AS link 
		INNER JOIN " .THIS_TABLE. "linksltoc AS ltoc ON (link.linkid = ltoc.linkid) 
		LEFT JOIN " . TABLE_PREFIX . "user AS user ON (link.linkuserid = user.userid) 
		LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
		WHERE linkstatus > 0 $metrolocale 
		ORDER BY linkstatus DESC, linkdate DESC LIMIT " . $vbulletin->options['tabinfo_media_count']);
		// sort by linkstatus first so that file entries have priority over links;
	
	$lastmediabit = '';
	while ($entry = $vbulletin->db->fetch_array($media)) {
		$linkimgrate = $linkurlstw = '';
		$vbb_rating = array();
		if ($entry['vbb_votenum'] > 0) {
			$vbb_rating['votenum'] = $entry['vbb_votenum'];
			$vbb_rating['voteavg'] = $entry['vbb_voteavg'];
			$vbb_rating['voteavg'] = vb_number_format($vbb_rating['voteavg'], 2);
			$vbb_rating['rating']  = intval(round($vbb_rating['voteavg']));
		}
		eval('$linkimgrate = "' . fetch_template('tabinfo_imgrate') . '";');
		$linkurljump = ldm_get_url_atag(-1, $entry['catid'], $entry['linkid'], $entry['linkname']);
		$linkhits = vb_number_format($entry['linkhits']);
		$linkimgthumb = $entry['linkimgthumb'];
		if ($linkimgthumb) {
			$lf = ldm_get_local_filename($linkimgthumb,0);
			$linkimgsrc = create_full_url(($linkimgthumb[0] != '/' ? "/" : "") . $linkimgthumb);
			$linkimgsrc = '<img src="' . $linkimgsrc . '" border="0" alt="" class="inlineimg" />';
			$linkimgjump = ldm_get_url_atag(-1, $entry['catid'], $entry['linkid'], $linkimgsrc);	// do not set thumb width (may be < 200px);
		}
		elseif ($entry['linksize'] == 0) {
			$stwimgkey = $vbulletin->options['tabinfo_stwimgkey'];
			$linkstwimg = "<img alt='' src=\"http://www.shrinktheweb.com/xino.php?embed=1&STWAccessKeyId=$stwimgkey&rndm=$entry[linkid]&Size=lg&Url=" . urlencode($entry['linkurl']) . '" />';
			$linkurlstw = preg_replace('#(<a.*>).*</a>#siU', '\1'. $linkstwimg .'</a>', $linkurljump);
		}
		else {
			$linkimgsrc = '<img src="' . $stylevar['imgdir_misc'] . '/unknown_sg.gif" border="0" alt="" class="inlineimg" width="120" />';
			$linkimgjump = ldm_get_url_atag(-1, $entry['catid'], $entry['linkid'], $linkimgsrc);
		}
		$createdby = fetch_musername($entry);
		$dateline = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $entry['linkdate'], true);
		$description = htmlspecialchars_uni(fetch_censored_text(fetch_trimmed_title(
			strip_bbcode($entry['linkdesc'], true, true, false), $max_description)));
		$linkthread = $entry['linkthread'];
		if ($linkthread) {
			eval('$linkthread = "' . $vbulletin->templatecache['links_linkthread'] . '";');
			$description .= ' (' . $linkthread . ')';
		}
		eval('$lastmediabit .= "' . fetch_template('tabinfo_lastmediabit') . '";'); 
	}
	$vbulletin->db->free_result($media);
	eval('$tabinfolastmedia = "' . fetch_template('tabinfo_lastmedia') . '";');
}

// get last X events
if ($vbulletin->options['tabinfo_events_count']) {
	require_once(DIR . '/includes/functions_misc.php');
	require_once(DIR . '/includes/functions_calendar.php');
	require_once(DIR . '/includes/functions_customcalendar.php');
	
	$lastevents = $vbulletin->db->query_read_slave(" 
	SELECT event.*, socialgroup.name,
	IF(event.vbb_votenum > 0, (event.vbb_votetotal / event.vbb_votenum), 0) AS vbb_voteavg,
	user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid  
	FROM " . TABLE_PREFIX . "event AS event 
	LEFT JOIN " . TABLE_PREFIX . "socialgroup AS socialgroup ON (event.groupid = socialgroup.groupid)
	LEFT JOIN " . TABLE_PREFIX . "user AS user ON (user.userid = event.userid)  
	LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
	WHERE event.groupid != 0 AND event.visible = 1 $metrolocale
	ORDER BY event.dateline_from DESC 
	LIMIT " . $vbulletin->options['tabinfo_events_count']);
	 
	$lasteventsbit = ''; 
	while($lastevent = $vbulletin->db->fetch_array($lastevents)) 
	{ 
		$recurcriteria = '';
		$groupname = substr($lastevent['name'], 0, $groups_name_max_title); 
		$lastevent['musername'] = fetch_musername($lastevent);  
		$eventname = '<a href="calendar.php?' . $vbulletin->session->vars['sessionurl'] .'do=getinfo&amp;e='. $lastevent['eventid'] .'">'. $lastevent['title'] .'</a>';
		fetch_user_date_time($lastevent);
		if (!$recurcriteria)
			$recurcriteria = $lastevent['dateline_to'] ? $vbphrase['ranged_event'] : $vbphrase['single_all_day_event'];
		$vbb_rating = array();
		if ($lastevent['vbb_votenum'] > 0) {
			$vbb_rating['votenum'] = $lastevent['vbb_votenum'];
			$vbb_rating['voteavg'] = $lastevent['vbb_voteavg'];
			$vbb_rating['voteavg'] = vb_number_format($vbb_rating['voteavg'], 2);
			$vbb_rating['rating']  = intval(round($vbb_rating['voteavg']));
		}
		eval('$linkimgrate = "' . fetch_template('tabinfo_imgrate') . '";');
		eval('$lasteventsbit .= "' . fetch_template('tabinfo_lasteventsbit') . '";'); 
	}
	$vbulletin->db->free_result($lastevents);
	eval('$tabinfolastevents = "' . fetch_template('tabinfo_lastevents') . '";');
}

// get last X albums 
if ($vbulletin->options['enable_latest_albums'] && THIS_SCRIPT == 'localarea') {
	require_once(DIR . '/includes/functions_album.php'); 
	require_once(DIR . '/includes/functions_user.php');
	$albums = $vbulletin->db->query_read(" 
		SELECT album.*, 
			picture.pictureid, picture.extension, picture.idhash, 
			picture.thumbnail_dateline, picture.thumbnail_width, picture.thumbnail_height, 
			IF(album.vbb_votenum > 0, (album.vbb_votetotal / album.vbb_votenum), 0) AS vbb_voteavg,
			user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid  
		FROM " . TABLE_PREFIX . "album AS album 
		INNER JOIN " . TABLE_PREFIX . "picture AS picture ON (album.coverpictureid = picture.pictureid AND picture.thumbnail_filesize > 0) 
		LEFT JOIN " . TABLE_PREFIX . "user AS user ON (album.userid = user.userid) 
		LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
		WHERE album.state = 'public' $metrolocale
		ORDER BY album.lastpicturedate DESC 
		LIMIT 0, $albumscount 
	"); 
	$lastalbumsbit = '';
	while ($album = $vbulletin->db->fetch_array($albums)) 
	{ 
		$show['personalalbum'] = $albumtype = '';		// private albums are not allowed since they EACH require an extra query;
		$album['musername'] = fetch_musername($album); 
		$album['picturecount'] = vb_number_format($album['visible']); 
		$album['picturedate'] = vbdate($vbulletin->options['dateformat'], $album['lastpicturedate'], true); 
		$album['picturetime'] = vbdate($vbulletin->options['timeformat'], $album['lastpicturedate']); 
		
		$album['title_html'] = substr($album['title'], 0, $albums_name_max_title); 
		$album['description_html'] = htmlspecialchars_uni(fetch_censored_text(fetch_trimmed_title(
			strip_bbcode($album['description'], true, true, false), $max_description)));
		$album['coverthumburl'] = ($album['pictureid'] ? fetch_picture_url($album, $album, true) : ''); 
		$album['coverdimensions'] = ($album['thumbnail_width'] ? "width=\"$album[thumbnail_width]\" height=\"$album[thumbnail_height]\"" : ''); 
		
		$vbb_rating = array();
		if ($album['vbb_votenum'] > 0) {
			$vbb_rating['votenum'] = $album['vbb_votenum'];
			$vbb_rating['voteavg'] = $album['vbb_voteavg'];
			$vbb_rating['voteavg'] = vb_number_format($vbb_rating['voteavg'], 2);
			$vbb_rating['rating']  = intval(round($vbb_rating['voteavg']));
		}
		eval('$linkimgrate = "' . fetch_template('tabinfo_imgrate') . '";');
		eval('$lastalbumsbit .= "' . fetch_template('tabinfo_lastalbumsbit') . '";'); 
	}
	$vbulletin->db->free_result($albums);
	eval('$tabinfolastalbums = "' . fetch_template('tabinfo_lastalbums') . '";');
}


// get last X users; reputation is not shown since each user can hide his reputation, which complicates the query (see memberlist.php);
if ($vbulletin->options['tabinfo_users_count']) {
	require_once(DIR . '/includes/functions_reputation.php');
	$hiderepids = -1;
	$hidereparray = array();
	
	foreach ($vbulletin->usergroupcache AS $ugroupid => $usergroup) {
		if ($usergroup['genericpermissions'] & $vbulletin->bf_ugp_genericpermissions['canhiderep']) {
			$hiderepids .= ",$ugroupid";
			$hidereparray[] = $ugroupid;
		}
	}
	$repcondition = "IF((NOT (options & " . $vbulletin->bf_misc_useroptions['showreputation']. ") AND (user.usergroupid IN ($hiderepids)";
	if (!empty($hidereparray)) {
		foreach($hidereparray AS $memberid) {
			$repcondition .= " OR FIND_IN_SET('$memberid', membergroupids)";
		}
	}
	$repcondition .= ")), 0, reputation) AS reputationscore";
	
	$lastusers = $vbulletin->db->query_read_slave(" 
	SELECT user.*, user.userid AS postuserid, user.username AS postusername, userfield.field2, avatar.avatarpath, 
	IF(vbb_votenum > 0, (vbb_votetotal / vbb_votenum), 0) AS vbb_voteavg, $repcondition,
	customprofilepic.userid AS profilepic, customprofilepic.dateline AS profiledateline, customprofilepic.width AS ppwidth, customprofilepic.height AS ppheight,
	customavatar.userid AS avatarpic, customavatar.dateline AS avatardateline, customavatar.width AS avwidth, customavatar.height AS avheight
	FROM " . TABLE_PREFIX . "user AS user 
	LEFT JOIN " . TABLE_PREFIX . "userfield AS userfield ON (userfield.userid = user.userid) 
	LEFT JOIN " . TABLE_PREFIX . "customprofilepic AS customprofilepic ON (customprofilepic.userid = user.userid) 
	LEFT JOIN " . TABLE_PREFIX . "customavatar AS customavatar ON (customavatar.userid = user.userid) 
	LEFT JOIN " . TABLE_PREFIX . "avatar AS avatar ON (avatar.avatarid = user.avatarid) 
	LEFT JOIN " . TABLE_PREFIX . "reputationlevel AS reputationlevel ON (user.reputationlevelid = reputationlevel.reputationlevelid) 
	WHERE usergroupid != 4 AND usergroupid != 8 $metrolocale
	ORDER BY usergroupid DESC, username ASC 
	LIMIT " . $vbulletin->options['tabinfo_users_count']);
	 
	$lastusersbit = ''; 
	while($lastuser = $vbulletin->db->fetch_array($lastusers)) 
	{ 
		$lastuser['musername'] = fetch_musername($lastuser);  
		$lastusername = '<a href="member.php?' . $vbulletin->session->vars['sessionurl'] .'u='. $lastuser['userid'] .'">'. $lastuser['musername'] .'</a>';
		$lastvisit = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastuser['lastvisit'], true);
		$joindate  = $vbphrase['joined'] .': '. vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastuser['joindate'], true);
		$lastuser['profilevisits'] = vb_number_format($lastuser['profilevisits']);
		$lastuser['friendcount'] = vb_number_format($lastuser['friendcount']);
		$postcount = vb_number_format($lastuser['posts']);
		$vbb_rating = array();
		if ($lastuser['vbb_votenum'] > 0) {
			$vbb_rating['votenum'] = $lastuser['vbb_votenum'];
			$vbb_rating['voteavg'] = $lastuser['vbb_voteavg'];
			$vbb_rating['voteavg'] = vb_number_format($vbb_rating['voteavg'], 2);
			$vbb_rating['rating']  = intval(round($vbb_rating['voteavg']));
		}
		eval('$linkimgrate = "' . fetch_template('tabinfo_imgrate') . '";');
		$userinfo =& $lastuser;										// activity && reputation templates reference $userinfo;
		$checkperms = cache_permissions($userinfo, false);
		fetch_reputation_image($userinfo, $checkperms);
		$userinfo['relactivity'] = round(($userinfo['totalactivity'] / $vbulletin->options['useract_topactivity']) * 100);
		$userinfo['gifactivity'] = round($userinfo['relactivity'] / 10);
		$userinfo['altactivity'] = construct_phrase($vbphrase['useract_altactivity'], $userinfo['relactivity'], $userinfo['totalactivity']);
		eval('$userinfo[activity_imagebit] = "' . fetch_template('useract_userimgbit') . '";');
		
		if ($lastuser['profilepic']) {
			if ($vbulletin->options['usefileavatar']) {
				$lastuser['profilepicurl'] = $vbulletin->options['profilepicurl'] . '/profilepic' . $lastuser['postuserid'] . '_' . $lastuser['profilepicrevision'] . '.gif';
			}
			else {
				$lastuser['profilepicurl'] = 'image.php?' . $vbulletin->session->vars['sessionurl'] . "u=$lastuser[postuserid]&amp;dateline=$lastuser[profiledateline]" . "&amp;type=profile"; 
			}
			$lastuser['profilepic'] = "<img src=\"" . $lastuser['profilepicurl'] . "\" alt=\"\" title=\"" . construct_phrase($vbphrase['xs_picture'], $lastuser['postusername']) . "\" border=\"0\"";
			$lastuser['ppwidth']  = $lastuser['ppwidth']  ? ' width="' . ($lastuser['ppwidth'] / 2) . '"' : '';
			$lastuser['ppheight'] = $lastuser['ppheight'] ? ' height="'. ($lastuser['ppheight']/ 2) . '"' : '';
			$lastuser['profilepic'] .=  $lastuser['ppwidth'] . $lastuser['ppheight'];
			$lastuser['profilepic'] .= ' />';
		}
		elseif (($lastuser['avatarid'] || $lastuser['avatarpic']) AND $vbulletin->options['avatarenabled']) {
			if ($lastuser['avatarid']) {
				$lastuser['avatarpicurl'] = $lastuser['avatarpath'];
			}
			elseif ($vbulletin->options['usefileavatar']) {
				$lastuser['avatarpicurl'] = $vbulletin->options['avatarurl'] . '/avatar' . $lastuser['postuserid'] . '_' . $lastuser['avatarrevision'] . '.gif';
			}
			else {
				$lastuser['avatarpicurl'] = 'image.php?' . $vbulletin->session->vars['sessionurl'] . "u=$lastuser[postuserid]&amp;dateline=$lastuser[avatardateline]";
			} 
			$lastuser['profilepic'] = "<img src=\"" . $lastuser['avatarpicurl'] . "\" alt=\"\" title=\"" . construct_phrase($vbphrase['xs_picture'], $lastuser['postusername']) . "\" border=\"0\"";
			$lastuser['avwidth']  = $lastuser['avwidth']  ? ' width="' . $lastuser['avwidth']  . '"' : '';
			$lastuser['avheight'] = $lastuser['avheight'] ? ' height="'. $lastuser['avheight'] . '"' : '';
			$lastuser['profilepic'] .=  $lastuser['avwidth'] . $lastuser['avheight'];
			$lastuser['profilepic'] .= ' />';
		}
		else {
			$lastuser['profilepic'] = '<img src="' . $stylevar['imgdir_misc'] . '/nobody.gif" border="0" alt="" class="inlineimg" width="120" />';
		}
		eval('$lastusersbit .= "' . fetch_template('tabinfo_lastusersbit') . '";'); 
	}
	$vbulletin->db->free_result($lastusers);
	eval('$tabinfolastusers = "' . fetch_template('tabinfo_lastusers') . '";');
}

?>