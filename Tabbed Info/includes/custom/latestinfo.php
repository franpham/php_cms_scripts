<?php

// max number of x to show 
$threadscount= intval($vbulletin->options['latest_threads_count']);  
$postscount  = intval($vbulletin->options['latest_posts_count']);  
$groupscount = intval($vbulletin->options['latest_groups_count']);  
$albumscount = intval($vbulletin->options['latest_albums_count']);  
$userid = $userinfo['userid'];				// used by footer links;
$username = $userinfo['username'];			// shown for no entries;

// max character title to be cut 
$threads_posts_max_title = intval($vbulletin->options['threads_posts_max_title']); 
$forums_name_max_title = intval($vbulletin->options['latest_forums_max_title']); 
$albums_name_max_title = $groups_name_max_title = intval($vbulletin->options['groups_name_max_title']); 
$max_description = intval($vbulletin->options['groups_description_max_title']); 

$excludedforums = '';   
if ($vbulletin->options['latest_ignore_forumids'] != '') {
	$excludedforums = ',' . $vbulletin->options['latest_ignore_forumids'];   
}
$forumpermissions = array();   
foreach($vbulletin->forumcache AS $forum) {
	$forumpermissions[$forum["forumid"]] = fetch_permissions($forum['forumid']);   
	if (!($forumpermissions[$forum["forumid"]] & $vbulletin->bf_ugp_forumpermissions['canview']) AND !$vbulletin->options['showprivateforums'])
		$excludedforums = $excludedforums . ',' . $forum['forumid'];
}
unset($forum, $forumpermissions);		// unset to prevent accidental usage; 
if ($excludedforums != '') {
	$excludedforums = substr($excludedforums, 1);
	$excludedforums = " AND thread.forumid NOT IN ($excludedforums) ";
}

// get last X threads; NEW CODE: do not show sensitive forums (parentid != 29);
if ($vbulletin->options['enable_latest_threads']) {
	$lastthreads = $vbulletin->db->query_read_slave("SELECT  
		thread.title, thread.taglist, thread.prefixid, thread.taglist as tag, thread.threadid, 
		thread.attach, icon.title AS icontitle, icon.iconpath, thread.lastpost, thread.lastpostid, 
		thread.forumid, thread.replycount, thread.lastposter, thread.dateline, thread.iconid,
		IF(thread.views<=thread.replycount, thread.replycount+1, thread.views) AS views, 
		thread.visible, user.username, user.userid, user.usergroupid, 
		IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, forum.title_clean as forum_title  
		FROM " . TABLE_PREFIX . "thread AS thread  
		INNER JOIN " . TABLE_PREFIX . "forum as forum on (thread.forumid = forum.forumid)  
		LEFT JOIN  " . TABLE_PREFIX . "user AS user ON (user.username = thread.lastposter)  
		LEFT JOIN " . TABLE_PREFIX . "icon AS icon ON(icon.iconid = thread.iconid)  
		WHERE thread.postuserid = $userid AND thread.visible = '1' 
		AND forum.parentid != 29 $excludedforums
		ORDER BY thread.dateline DESC 
		LIMIT 0, $threadscount 
	"); 	  
  $lastthreadsbit = ''; 
   
  while ($lastthread = $vbulletin->db->fetch_array($lastthreads))  
  { 
	$lastthread['title'] = substr($lastthread['title'], 0, $threads_posts_max_title); 
	$lastthread['forum_title'] = substr($lastthread['forum_title'], 0, $forums_name_max_title);  
	$lastthread['musername'] = fetch_musername($lastthread);
	if ($lastthread['lastpost'] > $vbulletin->userinfo['lastvisit']) 
		$lastthread['newpost'] = true;  
	
	$show['threadicon'] = false;	
	$lastthread['iconpath'] = '';	
	$lastthread['icontitle'] = '';	
	if ($lastthread['iconid'])	
	{	
	  $show['threadicon'] = true;	
	  $lastthread['iconpath'] = $vbulletin->iconcache["$lastthread[iconid]"]['iconpath'];	
	  $lastthread['icontitle'] = $vbulletin->iconcache["$lastthread[iconid]"]['title'];	
	}
	if ($lastthread['pollid'] != 0)	
	{	
	  $show['threadicon'] = true;	
	  $lastthread['iconpath'] = "$stylevar[imgdir_misc]/poll_posticon.gif";	
	  $lastthread['icontitle'] = $vbphrase['poll'];	
	}	
	elseif (!empty($vbulletin->options['showdeficon']))	
	{ 
	  $show['threadicon'] = true;	
	  $lastthread['iconpath'] = $vbulletin->options['showdeficon'];	
	  $lastthread['icontitle'] = '';	
	}	
	if ($lastthread['attach'] > 0)   
	{   
	  $show['paperclip'] = true;   
	  $lastthread['checkbox_value'] += THREAD_FLAG_ATTACH;   
	}
	$show['taglist'] = ($vbulletin->options['threadtagging'] AND !empty($lastthread['tag']));  
	$gnpdate = vbdate($vbulletin->options['dateformat'], $lastthread[lastpost], true);   
	$gnptime = vbdate($vbulletin->options['timeformat'], $lastthread[lastpost]);  
	eval("\$lastthreadsbit .= \"".fetch_template('tabinfo_lastthreadsbit')."\";"); 
  }
  $vbulletin->db->free_result($lastthreads);
  eval("\$tabinfolastthreads = \"".fetch_template('tabinfo_lastthreads')."\";");
}


// get last X posts 
if ($vbulletin->options['enable_latest_posts']) {
	$lastposts = $vbulletin->db->query_read_slave(" 
	SELECT thread.threadid, thread.title AS threadtitle, thread.forumid, thread.replycount, thread.pollid, thread.iconid,
	icon.title AS icontitle, icon.iconpath, thread.postusername, thread.postuserid, thread.dateline AS threaddate, thread.views, thread.visible,  
	post.threadid, post.username, post.userid, post.dateline AS lastpostdate, forum.forumid, forum.title AS forumtitle,  
	user.userid, user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid 
	FROM " . TABLE_PREFIX . "post AS post 
	INNER JOIN " . TABLE_PREFIX . "thread AS thread ON(thread.threadid = post.threadid) 
	INNER JOIN " . TABLE_PREFIX . "forum AS forum ON(forum.forumid = thread.forumid) 
	LEFT JOIN " . TABLE_PREFIX . "user AS user ON(user.userid = thread.postuserid) 
	LEFT JOIN " . TABLE_PREFIX . "icon AS icon ON(icon.iconid = thread.iconid)  
	WHERE post.userid = $userid AND thread.visible = '1' 
	AND forum.parentid != 29 $excludedforums
	ORDER BY lastpostdate DESC 
	LIMIT 0, $postscount 
	"); 
	$lastpostsbit = ''; 
	while($lastpost = $vbulletin->db->fetch_array($lastposts)) 
	{ 
		$show['threadicon'] = false;  
		$lastpost['iconpath'] = '';  
		$lastpost['icontitle'] = '';  
		if ($lastpost['iconid'])  
		{   // get icon from icon cache  
			$show['threadicon'] = true;  
			$lastpost['iconpath'] = $vbulletin->iconcache["$lastpost[iconid]"]['iconpath'];  
			$lastpost['icontitle'] = $vbulletin->iconcache["$lastpost[iconid]"]['title'];  
		}  
		if ($lastpost['pollid'] != 0)  
		{   // show poll icon  
			$show['threadicon'] = true;  
			$lastpost['iconpath'] = "$stylevar[imgdir_misc]/poll_posticon.gif";  
			$lastpost['icontitle'] = $vbphrase['poll'];  
		}  
		else if (!empty($vbulletin->options['showdeficon']))  
		{   // show default icon  
			$show['threadicon'] = true;  
			$lastpost['iconpath'] = $vbulletin->options['showdeficon'];  
			$lastpost['icontitle'] = '';  
		}  
		$threadtitle = htmlspecialchars_uni(substr($lastpost['threadtitle'], 0, $threads_posts_max_title)); 
		$forumtitle = htmlspecialchars_uni(substr($lastpost['forumtitle'], 0, $forums_name_max_title)); 
		$lastpostedate = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastpost['lastpostdate'], true); 
		$replies = vb_number_format($lastpost['replycount']); 
		$views = vb_number_format($lastpost['views']); 
		$threadby = $lastpost['postusername']; 
		$threaddate = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastpost['threaddate'], true); 
		$lastpost['musername'] = fetch_musername($lastpost);  
		eval('$lastpostsbit .= "' . fetch_template('tabinfo_lastpostsbit') . '";'); 
	}
	$vbulletin->db->free_result($lastposts);
	eval('$tabinfolastposts = "' . fetch_template('tabinfo_lastposts') . '";');
}

// get last X groups
if ($vbulletin->options['enable_latest_groups']) {
	require_once(DIR . '/includes/functions_socialgroup.php');
	$lastgroups = $vbulletin->db->query_read_slave(" 
	SELECT socialgroup.*, socialgroup.dateline AS createdate, 
	socialgroup.groupid, socialgroup.name, socialgroup.description, socialgroup.dateline, sgicon.dateline AS icondateline,
	sgicon.thumbnail_width AS iconthumb_width, sgicon.thumbnail_height AS iconthumb_height,
	user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid  
	FROM " . TABLE_PREFIX . "socialgroup AS socialgroup 
	INNER JOIN " . TABLE_PREFIX ."socialgroupmember AS socialgroupmember ON (socialgroupmember.groupid = socialgroup.groupid) 
	LEFT JOIN " . TABLE_PREFIX ."socialgroupicon AS sgicon ON (sgicon.groupid = socialgroup.groupid)
	LEFT JOIN " . TABLE_PREFIX . "user AS user ON(user.userid = socialgroup.creatoruserid)  
	WHERE socialgroupmember.userid = $userid AND socialgroupmember.type IN ('member', 'moderated')
	ORDER BY socialgroup.dateline DESC 
	LIMIT 0, $groupscount 
	"); 
	$lastgroupsbit = ''; 
	while($lastgroup = $vbulletin->db->fetch_array($lastgroups)) 
	{ 
		$groupname = substr($lastgroup['name'], 0, $groups_name_max_title); 
		$description = htmlspecialchars_uni(fetch_censored_text(fetch_trimmed_title(
			strip_bbcode($lastgroup['description'], true, true, false), $max_description)));
		$dateline = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $lastgroup['lastupdate'], true); 
		$members = vb_number_format($lastgroup['members']); 
		$messages = vb_number_format($lastgroup['visible']); 
		$pictures = vb_number_format($lastgroup['picturecount']); 
		$lastgroup['musername'] = fetch_musername($lastgroup);  
		$lastgroup['iconurl'] = fetch_socialgroupicon_url($lastgroup, true);
		eval('$lastgroupsbit .= "' . fetch_template('tabinfo_lastgroupsbit') . '";'); 
	}
	$vbulletin->db->free_result($lastgroups);
	eval('$tabinfolastgroups = "' . fetch_template('tabinfo_lastgroups') . '";');
}

// get latest media entries;
if ($vbulletin->options['tabinfo_media_count']) {
	require_once(DIR . '/includes/local_links_init.php');
	require_once(DIR . '/includes/local_links_include.php');
	
	$media = $vbulletin->db->query_read("
		SELECT ltoc.catid, link.linkid, linkurl, linkname, linkdesc, linkthread, linkimgthumb, linkhits, linkdate, linksize, vbb_votenum,
			IF(vbb_votenum > 0, (vbb_votetotal / vbb_votenum), 0) AS vbb_voteavg
			FROM " .THIS_TABLE. "linkslink AS link 
			INNER JOIN " .THIS_TABLE. "linksltoc AS ltoc ON (link.linkid = ltoc.linkid) 
			WHERE linkuserid = $userid AND linkstatus > 0 
			ORDER BY linkstatus DESC, linkdate DESC LIMIT " . $vbulletin->options['tabinfo_media_count']);
			// sort by linkstatus first so that file entries have priority over links;
	
	$lastmediabit = '';
	while ($entry = $vbulletin->db->fetch_array($media)) {
		$linkimgrate = $linkurlstw = '';
		$vbb_rating = array();
		if ($entry['vbb_votenum'] > 0) {
			$vbb_rating['votenum'] = $entry['vbb_votenum'];
			$vbb_rating['voteavg'] = $entry['vbb_voteavg'];
			$vbb_rating['voteavg'] = vb_number_format($vbb_rating['voteavg'], 2);
			$vbb_rating['rating']  = intval(round($vbb_rating['voteavg']));
		}
		eval('$linkimgrate = "' . fetch_template('tabinfo_imgrate') . '";');
		$linkurljump = ldm_get_url_atag(-1, $entry['catid'], $entry['linkid'], $entry['linkname']);
		$linkhits = vb_number_format($entry['linkhits']);
		$linkimgthumb = $entry['linkimgthumb'];
		if ($linkimgthumb) {
			$lf = ldm_get_local_filename($linkimgthumb,0);
			$linkimgsrc = create_full_url(($linkimgthumb[0] != '/' ? "/" : "") . $linkimgthumb);
			$linkimgsrc = '<img src="' . $linkimgsrc . '" border="0" alt="" class="inlineimg" width="120" />';
			$linkimgjump = ldm_get_url_atag(-1, $entry['catid'], $entry['linkid'], $linkimgsrc);	// set width to 120 to match STW images;
		}
		elseif ($entry['linksize'] == 0) {
			$stwimgkey = $vbulletin->options['tabinfo_stwimgkey'];
			$linkstwimg = "<img alt='' src=\"http://www.shrinktheweb.com/xino.php?embed=1&STWAccessKeyId=$stwimgkey&rndm=$entry[linkid]&Size=sm&Url=" . urlencode($entry['linkurl']) . '" />';
			$linkurlstw = preg_replace('#(<a.*>).*</a>#siU', '\1'. $linkstwimg .'</a>', $linkurljump);
		}
		else {
			$linkimgsrc = '<img src="' . $stylevar['imgdir_misc'] . '/unknown_sg.gif" border="0" alt="" class="inlineimg" width="120" />';
			$linkimgjump = ldm_get_url_atag(-1, $entry['catid'], $entry['linkid'], $linkimgsrc);	// set width to 120 to match STW images;
		}
		$linksize = $entry['linksize'] ? ldm_format_bytes($entry['linksize'], 2) : '';
		$dateline = vbdate($vbulletin->options['dateformat'] . ' ' . $vbulletin->options['timeformat'], $entry['linkdate'], true);
		$description = htmlspecialchars_uni(fetch_censored_text(fetch_trimmed_title(
			strip_bbcode($entry['linkdesc'], true, true, false), $max_description)));
		$linkthread = $entry['linkthread'];
		if ($linkthread) {
			eval('$linkthread = "' . $vbulletin->templatecache['links_linkthread'] . '";');
			$description .= ' (' . $linkthread . ')';
		}
		eval('$lastmediabit .= "' . fetch_template('tabinfo_lastmediabit') . '";'); 
	}
	$vbulletin->db->free_result($media);
	eval('$tabinfolastmedia = "' . fetch_template('tabinfo_lastmedia') . '";');
}

// get last X albums; remove THIS_SCRIPT == 'localarea' to show albums in Profile;
if ($vbulletin->options['enable_latest_albums'] && THIS_SCRIPT == 'localarea') {
	require_once(DIR . '/includes/functions_album.php'); 
	require_once(DIR . '/includes/functions_user.php');
	/*	// NOT NEEDED;
	$state = array('public'); 
	$albumcount = $vbulletin->db->query_first(" 
		SELECT COUNT(*) AS total 
		FROM " . TABLE_PREFIX . "album 
		WHERE state IN ('" . implode("', '", $state) . "') 
	"); */
	$albums = $vbulletin->db->query_read(" 
		SELECT album.*, 
			picture.pictureid, picture.extension, picture.idhash, 
			picture.thumbnail_dateline, picture.thumbnail_width, picture.thumbnail_height, 
			user.username, user.usergroupid, IF(displaygroupid=0, user.usergroupid, displaygroupid) AS displaygroupid, infractiongroupid  
		FROM " . TABLE_PREFIX . "album AS album 
		INNER JOIN " . TABLE_PREFIX . "picture AS picture ON (album.coverpictureid = picture.pictureid AND picture.thumbnail_filesize > 0) 
		LEFT JOIN " . TABLE_PREFIX . "user AS user ON (album.userid = user.userid) 
		WHERE album.userid = $userid AND album.state IN ('public', 'private') 
		ORDER BY album.lastpicturedate DESC 
		LIMIT 0, $albumscount 
	"); 
	$lastalbumsbit = ''; 
	while ($album = $vbulletin->db->fetch_array($albums)) 
	{ 
		$show['personalalbum'] = $albumtype = '';
		if ($album['state'] == 'private') { 
			$show['personalalbum'] = true; 
			$albumtype = "($vbphrase[private])"; 
			if (!can_view_private_albums($album['userid']))
				continue;
		} 
		$album['musername'] = fetch_musername($album); 
		$album['picturecount'] = vb_number_format($album['visible']); 
		$album['picturedate'] = vbdate($vbulletin->options['dateformat'], $album['lastpicturedate'], true); 
		$album['picturetime'] = vbdate($vbulletin->options['timeformat'], $album['lastpicturedate']); 
		
		$album['title_html'] = substr($album['title'], 0, $albums_name_max_title); 
		$album['description_html'] = htmlspecialchars_uni(fetch_censored_text(fetch_trimmed_title(
			strip_bbcode($album['description'], true, true, false), $max_description)));
		$album['coverthumburl'] = ($album['pictureid'] ? fetch_picture_url($album, $album, true) : ''); 
		$album['coverdimensions'] = ($album['thumbnail_width'] ? "width=\"$album[thumbnail_width]\" height=\"$album[thumbnail_height]\"" : ''); 
		
		eval('$lastalbumsbit .= "' . fetch_template('tabinfo_lastalbumsbit') . '";'); 
	}
	$vbulletin->db->free_result($albums);
	eval('$tabinfolastalbums = "' . fetch_template('tabinfo_lastalbums') . '";');
}

?>