<?php

// ####################### SET PHP ENVIRONMENT ###########################
error_reporting(E_ALL & ~E_NOTICE);

// #################### DEFINE IMPORTANT CONSTANTS #######################
define('THIS_SCRIPT', 'localarea');
define('CSRF_PROTECTION', true);

// ################### PRE-CACHE TEMPLATES AND DATA ######################
// get special phrase groups
$phrasegroups = array('global', 'user', 'calendar', 'reputationlevel');

// get special data templates from the datastore
$specialtemplates = array('smiliecache', 'bbcodecache', 'iconcache');

// pre-cache templates used by all actions
$globaltemplates = array('bbcode_code', 'bbcode_quote', 'localarea');

// pre-cache templates used by specific actions
$actiontemplates = array();

// ######################### REQUIRE BACK-END ############################
require_once('./global.php');
require_once(DIR . '/includes/custom/localinfo.php');

eval('$headinclude .= "' . fetch_template('tabinfo_userinfo_header') . '";');
eval('$navbar = "' . fetch_template('navbar') . '";');
eval('print_output("' . fetch_template('localarea') . '");');

?>