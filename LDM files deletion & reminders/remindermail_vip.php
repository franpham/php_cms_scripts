<?php

// IMPORTANT: when changing files deletion based on last visit to based on expired subscriptions:
// 1. change the reminder2_message_vip text to notify users about the files deletion policy change in VB Options settings, 
// 2. comment out the 'continue;' statement below AND in ldm_expirefiles.php, and then run the cron manually; this will find accounts that did not renew their memberships after 91 days WHILE the cron was disabled;
// 3. immediately uncomment the 'continue;' statement and revert the reminder2_message_vip text; enable automatic 'delete files of expired subscriptions' and enable 'send notices based on expired subscriptions' in 'File Deletion Notices' settings;
// 4. run the ldm_expirefiles.php cron manually after the notice period (15 days);

if (!is_object($vbulletin->db)){
	exit;
}
error_reporting(E_ALL & ~E_NOTICE);						// this cron uses same settings as remindermail_ldm.php except for reminder2_message;
	
	$now = time();
	$oneday = 60*60*24;
	$notifydate = TIMENOW - ($oneday * 76);				// notify 15 days before file deletion (91 days);
	$checkdate = TIMENOW - ($oneday * 75);				// $checkdate must be one day prior to catch times that occur between consecutive daily crons;
	
	$reminderCount = $vbulletin->options['reminder2_count'];
	if($reminderCount) {
		if ($vbulletin->options['reminder2_expirednotice']) {
			// get the MAX of expirydate since old subscriptions are NOT deleted; check sub_fileinfo, NOT vbb_fileinfo, since deletion is based on subscriptions;
			
			$result = $vbulletin->db->query_read("SELECT user.*, MAX(expirydate) AS expirydate FROM " .TABLE_PREFIX. "user AS user 
			LEFT JOIN " .TABLE_PREFIX. "subscriptionlog AS logfile ON (user.userid = logfile.userid) 
			WHERE expirydate < $checkdate AND sub_fileinfo = '' GROUP BY user.userid");
			// user table must be updated per user below;
		}
		else {
			$condition = "lastvisit < $checkdate AND usergroupid IN (2,4,8) AND (NOT FIND_IN_SET('9', user.membergroupids))";
			$result = $vbulletin->db->query_read("SELECT * FROM " . TABLE_PREFIX . "user WHERE $condition");
			$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "user SET notifyDate = '$now', notifyCount = notifyCount + 1 WHERE $condition");
		}
	}
	print("Found " . $vbulletin->db->found_rows($result) . " VIP file deletion notices.<br/><br/>");
			
	$sentlist = $failedlist = '';
	$from = "From: " . $vbulletin->options['bbtitle'] . " Reminder Service <" . $vbulletin->options['webmasteremail'] . ">" . "\r\n";
	while($row = $vbulletin->db->fetch_array($result)){		
		print("Sent To: $row[username]<br/>");
		
		if ($vbulletin->options['reminder2_expirednotice']) {
			if (round($row['expirydate'] / $oneday) != round($notifydate / $oneday))
				continue;
			$vbulletin->db->query_write("UPDATE " .TABLE_PREFIX. "subscriptionlog SET notifyDate = '$now', notifyCount = notifyCount + 1  
				WHERE expirydate = $row[expirydate] AND userid = $row[userid]");		// update subscriptionlog, NOT user table!
		}
		$username = $row['username'];
		$toemail = $row['email'];
		$userid = $row['userid'];
		$bbtitle = $vbulletin->options['bbtitle'];
		$homeurl = $vbulletin->options['homeurl'];
		$forumurl = $vbulletin->options['bburl'];
		$hometitle = $vbulletin->options['hometitle'];		

		//$headers  = "MIME-Version: 1.0" . "\r\nContent-type: text/html; charset=iso-8859-1" . "\r\n";
		eval('$message = "' . addslashes($vbulletin->options['reminder2_message_vip']) . '";');
		$message = stripslashes($message);
			
		eval('$subject = "' . addslashes($vbulletin->options['reminder2_subject']) . '";');
		$subject = stripslashes($subject);
			
		if(is_valid_email($toemail)){				
			$sentlist .= "$username ";
			vbmail($toemail, $subject, $message, false, $from);
		} else {
			$failedlist .= "$username ";
		}
	}
	$vbulletin->db->free_result($result);
	if($sentlist == ""){
		log_cron_action("No file deletion email notices to send.", $nextitem);
		if ($vbulletin->options['reminder2_sendadmin'])
			vbmail($vbulletin->options['webmasteremail'], "File Deletion (3 months) Email Report", "No file deletion email notices to send.", false, $from);
	} else {
		log_cron_action("File deletion notices were sent to inactive users. Emails were sent to: $sentlist.\r\n The following users had invalid emails: $failedlist", $nextitem);
		if ($vbulletin->options['reminder2_sendadmin'])
			vbmail($vbulletin->options['webmasteremail'], "File Deletion (3 months) Email Report", "File deletion notices were sent to inactive users. Emails were sent to: $sentlist.\r\n The following users had invalid emails: $failedlist", false, $from);
	}

?>