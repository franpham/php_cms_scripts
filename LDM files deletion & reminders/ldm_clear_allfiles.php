<?php

/*
$nextitem = array();
$nextitem['loglevel'] = 1;
$nextitem['varname'] = 'ldm_clear_allfiles';				// UNCOMMENT TO TEST DIRECTLY outside of cron;
*/
global $vbphrase;											// MUST globalize $vbphrase since automatic cron runs within a function;
require_once(DIR . '/includes/local_links_init.php');		// required for THIS_TABLE CONSTANT;
require_once(DIR . '/includes/local_links_include.php');	// required for $links_defaults && functions;

error_reporting(E_ALL & ~E_NOTICE);
if (!is_object($vbulletin->db)) {
	exit;
}
$cronphrase = array();
$oneday = 60*60*24;
$notifydate = TIMENOW - ($oneday * 182);
$checkdate = TIMENOW - ($oneday * 181);			// $checkdate must be one day prior to catch times that occur between consecutive daily crons;

$users = $vbulletin->db->query_read("SELECT user.userid, lastvisit, SUM(linksize + linkimgsize) AS usedsize FROM " .TABLE_PREFIX. "user AS user 
	LEFT JOIN " .THIS_TABLE. "linkslink AS link ON (link.linkuserid = user.userid) 
	WHERE lastvisit < $checkdate AND usergroupid IN (2,4,8) AND (NOT FIND_IN_SET('9', user.membergroupids)) GROUP BY user.userid");

// TO TEST: remove NOT before FIND_IN_SET in SQL; also change $notifydate && $checkdate as needed;
// ONLY delete files if it's exactly 182 days since less than 182 is too early, and greater than 182 already had files deleted (assume server was running at crontime);
// MUST round && divide by $oneday so that date comparison is not affected by time and since cron runs once daily;

while ($user = $vbulletin->db->fetch_array($users)) {
	if (round($user['lastvisit'] / $oneday) == round($notifydate / $oneday)) {
		$freespace = $filecount = 0;			// delete all LDM files if user's lastvisit is 6 months ago;
		$files = $vbulletin->db->query_read(
			"SELECT linkid, linkurl, linkimg, linkimgthumb, linksize + linkimgsize AS totalsize FROM " .THIS_TABLE. "linkslink 
			WHERE linkuserid = $user[userid] AND (linksize > 0 OR linkimgsize > 0) ORDER BY totalsize DESC");
		
		while ($file = $vbulletin->db->fetch_array($files)) {
			$freespace += $file['totalsize'];
			$filecount++;
			if ($file['linkurl'])
				ldm_delete_upload($file['linkurl']);
			if ($file['linkimg'])
				ldm_delete_upload($file['linkimg']);
			if ($file['linkimgthumb'])
				ldm_delete_thumb($file['linkimgthumb']);
			// MUST DELETE linkimgthumb because LDM tries to create a Shadowbox image link, but the full-size image was deleted;
			
			$linkurl = $vbulletin->options['bburl'] . '/member.php?' . $vbulletin->session->vars['sessionurl'] .'u='. $user['userid'];
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linksltoc SET displayorder = 10 WHERE linkid = $file[linkid]");
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linkslink 
			SET linkurl = '$linkurl', linkfile = '', linkimg = '', linkimgthumb = '', linksize = 0, linkimgsize = 0, linkimgthumbsize = 0, linkimgstatus = 0, linkstatus = 1 WHERE linkid = $file[linkid]");		// set $linkurl since empty entries are not allowed in LDM;
		}
		$vbulletin->db->free_result($files);
		$cronphrase[] = $user['userid'] .' ID: '. $filecount .' files';
		$fileinfo = vbdate($vbulletin->options['dateformat'], TIMENOW) .': '. $filecount .' files, '. ldm_encode_bytes($freespace);
		$vbulletin->db->query_write("UPDATE " .TABLE_PREFIX. "user SET vbb_fileinfo = '$fileinfo' WHERE userid = $user[userid]");
	}
}
$vbulletin->db->free_result($users);
$cronphrase = 'Files Deleted: ' . implode(', ', $cronphrase);
log_cron_action($cronphrase, $nextitem);
echo $cronphrase . '<br />';

?>