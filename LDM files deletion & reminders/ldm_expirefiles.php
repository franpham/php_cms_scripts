<?php

/*
$nextitem = array();
$nextitem['loglevel'] = 1;
$nextitem['varname'] = 'ldm_expirefiles';					// UNCOMMENT TO TEST DIRECTLY outside of cron;
*/
global $vbphrase;											// MUST globalize $vbphrase since automatic cron runs within a function;
require_once(DIR . '/includes/local_links_init.php');		// required for THIS_TABLE CONSTANT;
require_once(DIR . '/includes/local_links_include.php');	// required for $links_defaults && functions;

error_reporting(E_ALL & ~E_NOTICE);
if (!is_object($vbulletin->db)) {
	exit;
}
$oneday = 60*60*24;
$notifydate = TIMENOW - ($oneday * 91);
$checkdate = TIMENOW - ($oneday * 90);				// $checkdate must be one day prior to catch times that occur between consecutive daily crons;
$cronphrase = array();
$userids = array();
$userdata = array();
// get the MAX of expirydate since old subscriptions are NOT deleted; check sub_fileinfo, NOT vbb_fileinfo, since deletion is based on subscriptions;

$users = $vbulletin->db->query_read("SELECT user.userid, MAX(expirydate) AS expirydate FROM " .TABLE_PREFIX. "user AS user 
	LEFT JOIN " .TABLE_PREFIX. "subscriptionlog AS logfile ON (user.userid = logfile.userid) 
	WHERE expirydate < $checkdate AND sub_fileinfo = '' GROUP BY user.userid");
while ($user = $vbulletin->db->fetch_array($users)) {
	$userids[] = $user['userid'];
	$userdata[$user['userid']] = $user;				// MUST SET to the whole array!
}
$userids = implode(',', $userids);
$vbulletin->db->free_result($users);
// IMPORTANT: DO NOT JOIN linkslink table with subscriptionlog query because users can have many subscriptions (old & new), which causes repeated summing of usedsize;

$users = $vbulletin->db->query_read("SELECT linkuserid AS userid, SUM(linksize + linkimgsize) AS usedsize FROM " .THIS_TABLE. "linkslink 
	WHERE linkuserid IN (0$userids) GROUP BY linkuserid");
while ($user = $vbulletin->db->fetch_array($users)) {
	$userdata[$user['userid']]['usedsize'] = $user['usedsize'];
}
$vbulletin->db->free_result($users);

// TO TEST: change $notifydate to 0 and 1st query WHERE to (do not test ID #1 so that test files are not deleted): WHERE user.userid IN (2,3)
// ONLY delete files if it's exactly 91 days since less than 91 is too early, and greater than 91 already had files deleted (assume server was running at crontime);
// MUST round && divide by $oneday so that date comparison is not affected by time and since cron runs once daily;
// DO NOT compare by registered date since a subscription can be continuous, so when it's expired, the files are deleted immediately wo. any wait period;

foreach ($userdata as $user) {
	if (round($user['expirydate'] / $oneday) != round($notifydate / $oneday))
		continue;
	$limit = unserialize($links_defaults['bandwidth_limit']);
	$uploadlimit = ldm_decode_bytes($limit[2]['uploadlimit']);
	$usedsize = $user['usedsize'];
	if ($usedsize > $uploadlimit) {				// delete extra files (VIP content) if subscription expired 3 months ago;
		$freespace = $filecount = 0;
		$files = $vbulletin->db->query_read(
			"SELECT linkid, linkurl, linkimg, linkimgthumb, linksize + linkimgsize AS totalsize FROM " .THIS_TABLE. "linkslink 
			WHERE linkuserid = $user[userid] AND (linksize > 0 OR linkimgsize > 0) ORDER BY totalsize DESC");
		
		while ($usedsize > $uploadlimit) {
			$file = $vbulletin->db->fetch_array($files);
			$usedsize  -= $file['totalsize'];
			$freespace += $file['totalsize'];
			$filecount++;						// must log fileinfo in this cron since it has expirydate data;
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linksltoc SET displayorder = 10 WHERE linkid = $file[linkid]");
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linkslink SET linkmoderate = " . $LINK_NO_ACCESS . " WHERE linkid = $file[linkid]");
			// update linkmoderate instead of linkstatus because LDM code changes linkstatus whenever the entry is edited; REMOVE THIS SQL after beta phase;
		}
		$vbulletin->db->free_result($files);
		$cronphrase[] = $user['userid'] .' ID: '. $filecount .' files';
		$fileinfo = vbdate($vbulletin->options['dateformat'], TIMENOW) .': '. $filecount .' files, '. ldm_encode_bytes($freespace);
		$vbulletin->db->query_write("UPDATE " .TABLE_PREFIX. "subscriptionlog SET sub_fileinfo = '$fileinfo' 
			WHERE expirydate = $user[expirydate] AND userid = $user[userid]");
	}
}
$vbulletin->db->free_result($users);
$cronphrase = 'Files Deleted: ' . implode(', ', $cronphrase);
log_cron_action($cronphrase, $nextitem);
echo $cronphrase . '<br />';

$cronphrase = array();
$timenow = TIMENOW;
// compare regdate to one day prior to catch times that occur between consecutive daily crons;
$users = $vbulletin->db->query_read("SELECT userid, regdate FROM " .TABLE_PREFIX. "subscriptionlog WHERE regdate > $timenow - $oneday");
while ($user = $vbulletin->db->fetch_array($users)) {
	if (round($user['regdate'] / $oneday) == round($timenow / $oneday)) {
		$files = $vbulletin->db->query_read("SELECT linkid FROM " .THIS_TABLE. "linkslink WHERE linkuserid = $user[userid] AND (linksize > 0 OR linkimgsize > 0)");
		$linkids = array();
		$filecount = 0;
		while ($file = $vbulletin->db->fetch_array($files)) {
			$linkids[] = $file['linkid'];
			$filecount++;
		}
		$linkids = implode(',', $linkids);
		if ($linkids)
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linksltoc SET displayorder = 1 WHERE linkid IN (0$linkids)");
		$vbulletin->db->free_result($files);
		$cronphrase[] = $user['userid'] .' ID: '. $filecount .' entries';
	}
}
$vbulletin->db->free_result($users);
$cronphrase = 'Entries Updated: ' . implode(', ', $cronphrase);
log_cron_action($cronphrase, $nextitem);
echo $cronphrase . '<br />';

?>