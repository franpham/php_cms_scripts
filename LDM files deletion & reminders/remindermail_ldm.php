<?php

if (!is_object($vbulletin->db)){
	exit;
}
error_reporting(E_ALL & ~E_NOTICE);

	$now = time();
	$oneday = 60*60*24;
	$checkdate = TIMENOW - ($oneday * 152);						// notify 30 days before file deletion (182 days);
	$condition = "lastvisit < $checkdate AND usergroupid IN (2,4,8) AND (NOT FIND_IN_SET('9', user.membergroupids))";
	
	$reminderCount = $vbulletin->options['reminder2_count'];
	if($reminderCount) {
		$result = $vbulletin->db->query_read("SELECT * FROM " . TABLE_PREFIX . "user WHERE $condition");
		
		$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "user SET notifyDate = '$now', notifyCount = notifyCount + 1 WHERE $condition");
	}
	print("Found " . $vbulletin->db->found_rows($result) . " file deletion notices.<br/><br/>");
			
	$sentlist = $failedlist = '';
	$from = "From: " . $vbulletin->options['bbtitle'] . " Reminder Service <" . $vbulletin->options['webmasteremail'] . ">" . "\r\n";
	while($row = $vbulletin->db->fetch_array($result)){		
		print("Sent To: $row[username]<br/>");

		$username = $row['username'];
		$toemail = $row['email'];
		$userid = $row['userid'];
		$bbtitle = $vbulletin->options['bbtitle'];
		$homeurl = $vbulletin->options['homeurl'];
		$forumurl = $vbulletin->options['bburl'];
		$hometitle = $vbulletin->options['hometitle'];		

		//$headers  = "MIME-Version: 1.0" . "\r\nContent-type: text/html; charset=iso-8859-1" . "\r\n";
		eval('$message = "' . addslashes($vbulletin->options['reminder2_message']) . '";');
		$message = stripslashes($message);
			
		eval('$subject = "' . addslashes($vbulletin->options['reminder2_subject']) . '";');
		$subject = stripslashes($subject);
			
		if(is_valid_email($toemail)){				
			$sentlist .= "$username ";
			vbmail($toemail, $subject, $message, false, $from);
		} else {
			$failedlist .= "$username ";
		}
	}
	$vbulletin->db->free_result($result);
	if($sentlist == ""){
		log_cron_action("No file deletion email notices to send.", $nextitem);
		if ($vbulletin->options['reminder2_sendadmin'])
			vbmail($vbulletin->options['webmasteremail'], "File Deletion (6 months) Email Report", "No file deletion email notices to send.", false, $from);
	} else {
		log_cron_action("File deletion notices were sent to inactive users. Emails were sent to: $sentlist.\r\n The following users had invalid emails: $failedlist", $nextitem);
		if ($vbulletin->options['reminder2_sendadmin'])
			vbmail($vbulletin->options['webmasteremail'], "File Deletion (6 months) Email Report", "File deletion notices were sent to inactive users. Emails were sent to: $sentlist.\r\n The following users had invalid emails: $failedlist", false, $from);
	}

?>