<?php

// BETA PHASE: this cron enables ldm_expirefiles.php to mark files to be deleted so that the admin can verify the selection is correct before deleting via this cron;
/*
$nextitem = array();
$nextitem['loglevel'] = 1;
$nextitem['varname'] = 'ldm_clear_oldfiles';				// UNCOMMENT TO TEST DIRECTLY outside of cron;
*/
global $vbphrase;											// MUST globalize $vbphrase since automatic cron runs within a function;
require_once(DIR . '/includes/local_links_init.php');		// required for THIS_TABLE CONSTANT;
require_once(DIR . '/includes/local_links_include.php');	// required for $links_defaults && functions;
error_reporting(E_ALL & ~E_NOTICE);
if (!is_object($vbulletin->db)) {
	exit;
}

$vipgroups = array(9, 10);
$filecount = 0;												// can not log fileinfo in this cron since it does not have expirydate data;
$files = $vbulletin->db->query_read("SELECT linkid, linkurl, linkimg, linkimgthumb, membergroupids, usergroupid, user.userid FROM " .THIS_TABLE. "linkslink AS link 
	LEFT JOIN " .TABLE_PREFIX. "user AS user ON (user.userid = link.linkuserid) WHERE linkmoderate = ". $LINK_NO_ACCESS);
while ($file = $vbulletin->db->fetch_array($files)) {
		$filecount++;
		$user = array('userid' => $file['userid']);			// set this for compatibility with spliced code block below;
		// after beta phase, remove code between DELETION CODE tags and insert it in ldm_expirefiles.php, and remove the UPDATE SQL for linkmoderate in that file;
		if (!is_member_of($file, $vipgroups)) {
		
			// !!!!!!!!!!!!!!!!!!!!!!					START OF FILE DELETION CODE				!!!!!!!!!!!!!!!!!!!!
			if ($file['linkurl'])
				ldm_delete_upload($file['linkurl']);
			if ($file['linkimg'])
				ldm_delete_upload($file['linkimg']);
			if ($file['linkimgthumb'])
				ldm_delete_thumb($file['linkimgthumb']);
			// MUST DELETE linkimgthumb because LDM tries to create a Shadowbox image link, but the full-size image was deleted;
			
			$linkurl = $vbulletin->options['bburl'] . '/member.php?' . $vbulletin->session->vars['sessionurl'] .'u='. $user['userid'];
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linkslink 
			SET linkurl = '$linkurl', linkfile = '', linkimg = '', linkimgthumb = '', linksize = 0, linkimgsize = 0, linkimgthumbsize = 0, linkimgstatus = 0, linkmoderate = 0, linkstatus = 1 WHERE linkid = $file[linkid]");		// set $linkurl since empty entries are not allowed in LDM;
			// !!!!!!!!!!!!!!!!!!!!!					END OF FILE DELETION CODE				!!!!!!!!!!!!!!!!!!!!
		
		}
		else {		// user renewed VIP membership between marking vip subscriptions expired and running delete files of expired subscriptions;
			$vbulletin->db->query_write("UPDATE " .THIS_TABLE. "linkslink SET linkmoderate = " . $LINK_NO_ACCESS . " WHERE linkid = $file[linkid]");
		}
}
$vbulletin->db->free_result($files);
$cronphrase = 'Finished manual file deletions: ' . $filecount;
log_cron_action($cronphrase, $nextitem);
echo $cronphrase . '<br />';

?>