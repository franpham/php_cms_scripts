<?php

if (!is_object($vbulletin->db)){
	exit;
}
error_reporting(E_ALL & ~E_NOTICE);

	$now = time();
	$datetime = array();
	$usergroups = explode(',', $vbulletin->options['reminder_usergroups']);
	$datetime['lastpost'] = $now - (60 * 60 * 24 * $vbulletin->options['reminder_inactivity']);
	$datetime['lastemail'] = $now - (60 * 60 * 24 * $vbulletin->options['reminder_frequency']);

	$conditionparam = $vbulletin->options['reminder_posts'] ? 'lastactivity' : 'lastvisit';
	$reminderCount = $vbulletin->options['reminder_count'];
	if ($reminderCount) {
		$result = $vbulletin->db->query_read("SELECT * FROM " . TABLE_PREFIX . "user 
		WHERE $conditionparam < $datetime[lastpost] AND emailDate < $datetime[lastemail] AND emailCount < $reminderCount");
		
		$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "user SET emailDate = '$now', emailCount = emailCount + 1 
		WHERE $conditionparam < $datetime[lastpost] AND emailDate < $datetime[lastemail] AND emailCount < $reminderCount");
	}
	print("Found " . $vbulletin->db->found_rows($result) . " Inactive Users.<br/><br/>");
	
	$sentlist = $failedlist = '';
	$from = "From: " . $vbulletin->options['bbtitle'] . " Reminder Service <" . $vbulletin->options['webmasteremail'] . ">" . "\r\n";
	while($row = $vbulletin->db->fetch_array($result)){		
		if(is_member_of($row, $usergroups)){
			print("Sent To: $row[username]<br/>");

			$username = $row['username'];
			$toemail = $row['email'];
			$userid = $row['userid'];
			$bbtitle = $vbulletin->options['bbtitle'];
			$homeurl = $vbulletin->options['homeurl'];
			$forumurl = $vbulletin->options['bburl'];
			$hometitle = $vbulletin->options['hometitle'];		

			//$headers  = "MIME-Version: 1.0" . "\r\nContent-type: text/html; charset=iso-8859-1" . "\r\n";
			eval('$message = "' . addslashes($vbulletin->options['reminder_message']) . '";');
			$message = stripslashes($message);
			
			eval('$subject = "' . addslashes($vbulletin->options['reminder_subject']) . '";');
			$subject = stripslashes($subject);
			
			if(is_valid_email($toemail)){				
				$sentlist .= "$username ";
				vbmail($toemail, $subject, $message, false, $from);
			} else {
				$failedlist .= "$username ";
			}
		}
	}
	$vbulletin->db->free_result($result);
	if($sentlist == ""){
		log_cron_action("No inactivity reminder emails to send.", $nextitem);
		if ($vbulletin->options['reminder_sendadmin'])
			vbmail($vbulletin->options['webmasteremail'], "Inactivity Reminder Email Report", "No inactivity reminder emails to send.", false, $from);
	} else {
		log_cron_action("Reminders were sent to inactive users. Emails sent to:$sentlist. The following users had invalid emails: $failedlist", $nextitem);
		if ($vbulletin->options['reminder_sendadmin'])
			vbmail($vbulletin->options['webmasteremail'], "Inactivity Reminder Email Report", "Reminders were sent to inactive users. Emails sent to:$sentlist. The following users had invalid emails: $failedlist", false, $from);
	}

?>