<?php

class vb_albumrating
{
	static $votedcache = array();
	static $vboptimise;

	function fetch_rating($profileajax = false)
	{
		global $vbulletin, $albumrating, $vbphrase, $albuminfo, $stylevar, $vote;

		if ($vbulletin->options['_vb_pr_active'])
		{
			$stars = 0;
			$starbits = '';
			$albuminfo['vbb_rating'] = 0;
			$albuminfo['vbb_voteavg'] = '0.00';

			if ($albuminfo['vbb_votetotal'] > 0 && $albuminfo['vbb_votenum'] > 0)
			{
				$albuminfo['vbb_voteavg'] = vb_number_format($albuminfo['vbb_votetotal'] / $albuminfo['vbb_votenum'], 2);
				$albuminfo['vbb_rating'] = intval(round($albuminfo['vbb_votetotal'] / $albuminfo['vbb_votenum']));
			}

			while ($stars++ < 5)
			{
				$padding = $stars > 0;
				$type = $stars > $albuminfo['vbb_rating'] ? '_blank' : '';
				eval('$starbits .= "' . fetch_template('rating_albumrating_star') . '";');
			}

			$vote = $profileajax ? false : self::can_vote($albuminfo['albumid']);

			eval('$albumrating = "' . fetch_template('rating_albumrating') . '";');
		}
	}

	function can_vote($profileid = 0)
	{
		global $vbulletin;

		if ($vbulletin->userinfo['userid'] > 0 && ($vbulletin->userinfo['userid'] != $profileid || $vbulletin->options['_vb_pr_voteself']))
		{
			return $vbulletin->options['_vb_pr_changevote'] || !self::has_voted($profileid);
		}
		return false;
	}

	function has_voted($profileid = 0)
	{
		global $vbulletin;

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_fetch_cache($profileid);
		}

		if (isset(self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']]))
		{
			return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
		}

		$voted = $vbulletin->db->query_first("select vote from " . TABLE_PREFIX . "albumrate where profileid=$profileid and userid=" . $vbulletin->userinfo['userid']);

		self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']] = $voted['vote'] ? true : false;

		return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
	}

	function rebuild_votes($profileid = 0)
	{
		global $vbulletin, $albuminfo;

		$vbb_votetotal = 0;
		$vbb_votenum = 0;

		$votes = $vbulletin->db->query_read("select vote from " . TABLE_PREFIX . "albumrate where profileid=$profileid");
		while ($vote = $vbulletin->db->fetch_array($votes))
		{
			$vbb_votetotal += $vote['vote'];
			$vbb_votenum++;
		}

		$albuminfo['vbb_votetotal'] = $vbb_votetotal;
		$albuminfo['vbb_votenum'] = $vbb_votenum;

		$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "album SET vbb_votetotal=$vbb_votetotal, vbb_votenum=$vbb_votenum where albumid=$profileid");

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vote($userid = 0, $profileid = 0, $vote = 0)
	{
		global $vbulletin, $albumrating, $albuminfo, $vbphrase;

		require_once(DIR . '/includes/class_xml.php');

		if (!function_exists('fetch_phrase'))
		{
			require_once(DIR . '/includes/functions_misc.php');
		}

		$xml = new vB_AJAX_XML_Builder($vbulletin, 'text/xml');

		if (self::can_vote($profileid) && $vbulletin->options['_vb_pr_active'])
		{
			if (self::has_voted($profileid))
			{
				$vbulletin->db->query_write("update " . TABLE_PREFIX . "albumrate set vote=$vote where profileid=$profileid and userid=$userid");
			}
			else
			{
				$vbulletin->db->query_write("insert into " . TABLE_PREFIX . "albumrate (userid, profileid, vote) values ($userid, $profileid, $vote)");
			}

			self::rebuild_votes($profileid);
			self::process_top_rated();

			$vbphrase['album_rating_x_votes_y_average'] = fetch_phrase('album_rating_x_votes_y_average', 'user');

			self::fetch_rating(true);

			$xml->add_group('albumrating');
			$xml->add_tag('rating', $albumrating);
			$xml->close_group();
			$xml->print_xml();
		}

		$vbphrase['vbprofilerating_error'] = fetch_phrase('vbprofilerating_error', 'user');

		$xml->add_group('albumrating');
		$xml->add_tag('error', $vbphrase['vbprofilerating_error']);
		$xml->close_group();
		$xml->print_xml();
	}

	function process_top_rated()
	{
		global $vbulletin;
		$isAve = $vbulletin->options['_vb_pr_toprated_calc'] == 'ave';
		$isCount = $vbulletin->options['_vb_pr_toprated_calc'] == 'voted';
		$field = $isAve ? 'vbb_average' : ($isCount ? 'album.vbb_votenum' : 'album.vbb_votetotal');

		$top = $vbulletin->db->query_first("select user.username, user.userid, album.albumid, " . 
		($isAve ? "(album.vbb_votetotal / IF (album.vbb_votenum = 0, 1, album.vbb_votenum)) AS $field, " : "$field, ") . "
		IF (user.displaygroupid=0, user.usergroupid, user.displaygroupid) AS displaygroupid FROM " . 
		TABLE_PREFIX . "album AS album INNER JOIN " . TABLE_PREFIX . "user AS user ON (user.userid = album.userid) 
		order by $field desc, " . ($isCount ? 'album.vbb_votetotal' : 'album.vbb_votenum') . 
		(($isAve or $isCount) ? ' desc ' : ' asc ') . 'limit 1');
		// if sorting by vote counts, 2nd sort field is vote totals, descending order;
		// else 2nd sort field is vote counts, but descending order for averages and ascending order for totals (smaller count gives higher average);

		if (($isAve ? $top[$field] : $top[substr($field, strpos($field, '.') + 1)]) > 0)
		{
			$top = fetch_musername($top) . "|$top[albumid]|{$vbulletin->options['_vb_pr_toprated_calc']}";
			$toprated = $top;
		}
		else
		{
			$toprated = 'none';
		}

		if ($toprated != $vbulletin->options['_vb_album_toprated'])
		{
			require_once(DIR . '/includes/adminfunctions.php');
			require_once(DIR . '/includes/adminfunctions_options.php');

			save_settings(array(
				'_vb_album_toprated' => $toprated,
			));
		}

		$vbulletin->options['_vb_album_toprated'] = $toprated;
	}

	function top_rated()
	{
		global $vbulletin, $template_hook, $vbphrase;

		if (!$vbulletin->options['_vb_pr_toprated_enable'])
		{
			return false;
		}

		if (trim($vbulletin->options['_vb_album_toprated']) == '')
		{
			self::process_top_rated();
		}

		if ($vbulletin->options['_vb_album_toprated'] != 'none')
		{
			$top = explode('|', $vbulletin->options['_vb_album_toprated']);

			if ($top[2] != $vbulletin->options['_vb_pr_toprated_calc'])
			{
				self::process_top_rated();
			}

			eval('$template_hook[forumhome_wgo_stats] .= "' . fetch_template('rating_topratedalbum') . '";');
		}
	}

	function init_vboptimise()
	{
		global $vbulletin;

		if (!isset(self::$vboptimise))
		{
			require_once(DIR . '/includes/class_activecache.php');

			self::$vboptimise = vb_activecache::get_instance($vbulletin->options['_vboptimise_method']);
		}
	}

	function vboptimise_fetch_cache($profileid = 0)
	{
		self::init_vboptimise();

		$ratings = self::$vboptimise->fetch('albumratings_' . $profileid);

		if ($ratings)
		{
			$ratings = unserialize($ratings);

			foreach ($ratings as $rating)
			{
				self::$votedcache[$profileid . '_' . $rating['userid']] = true;
			}

			devdebug('vB Optimise: Fetched profile ratings cache.');
		}
		else
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vboptimise_save_cache($profileid = 0)
	{
		global $vbulletin;

		self::init_vboptimise();

		$cache = array();

		$ratings = $vbulletin->db->query_read("select userid from " . TABLE_PREFIX . "albumrate where profileid=$profileid");
		while ($rating = $vbulletin->db->fetch_array($ratings))
		{
			$cache[] = $rating;
		}

		self::$vboptimise->set('albumratings_' . $profileid, serialize($cache));

		devdebug('vB Optimise: Set profile ratings cache.');
	}
}
?>