<?php

class vb_imagerating
{
	static $votedcache = array();
	static $vboptimise;

	function fetch_rating($profileajax = false)
	{
		global $vbulletin, $imagerating, $vbphrase, $picture, $stylevar;

		if ($vbulletin->options['_vb_pr_active'])
		{
			$stars = 0;
			$starbits = '';
			$picture['vbb_rating'] = 0;
			$picture['vbb_voteavg'] = '0.00';

			if ($picture['vbb_votetotal'] > 0 && $picture['vbb_votenum'] > 0)
			{
				$picture['vbb_voteavg'] = vb_number_format($picture['vbb_votetotal'] / $picture['vbb_votenum'], 2);
				$picture['vbb_rating'] = intval(round($picture['vbb_votetotal'] / $picture['vbb_votenum']));
			}

			while ($stars++ < 5)
			{
				$padding = $stars > 0;
				$type = $stars > $picture['vbb_rating'] ? '_blank' : '';
				eval('$starbits .= "' . fetch_template('rating_ratestar') . '";');
			}

			$vote = $profileajax ? false : self::can_vote($picture['pictureid']);

			eval('$imagerating = "' . fetch_template('rating_imagerating') . '";');
		}
	}

	function can_vote($profileid = 0)
	{
		global $vbulletin;
		if ($profileid <= 0 || $vbulletin->uerinfo['usergroupid'] == 8)
			return false;
		elseif ($vbulletin->userinfo['usergroupid'] == 3 || $vbulletin->userinfo['usergroupid'] == 4)
			return false;						// disable voting for banned groups;
		elseif ($vbulletin->userinfo['userid'] == 0)
			return false;						// disable voting for guests;
		elseif ($vbulletin->userinfo['userid'] != $profileid || $vbulletin->options['_vb_pr_voteself'])
			return $vbulletin->options['_vb_pr_changevote'] || !self::has_voted($profileid);
		else									// not allowed to vote for one's entry;
			return false;
	}

	function has_voted($profileid = 0)
	{
		global $vbulletin;

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_fetch_cache($profileid);
		}

		if (isset(self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']]))
		{
			return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
		}

		$voted = $vbulletin->db->query_first("select vote from " . TABLE_PREFIX . "picrate where profileid=$profileid and userid=" . $vbulletin->userinfo['userid']);
		self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']] = $voted['vote'] ? true : false;
		return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
	}

	function rebuild_votes($profileid = 0)
	{
		global $vbulletin, $picture;

		$vbb_votetotal = 0;
		$vbb_votenum = 0;

		$votes = $vbulletin->db->query_read("select vote from " . TABLE_PREFIX . "picrate where profileid=$profileid");
		while ($vote = $vbulletin->db->fetch_array($votes))
		{
			$vbb_votetotal += $vote['vote'];
			$vbb_votenum++;
		}
		$vbulletin->db->free_result($votes);

		$picture['vbb_votetotal'] = $vbb_votetotal;
		$picture['vbb_votenum'] = $vbb_votenum;

		$vbulletin->db->query_write("UPDATE " . TABLE_PREFIX . "picture SET vbb_votetotal=$vbb_votetotal, vbb_votenum=$vbb_votenum where pictureid=$profileid");

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vote($userid = 0, $profileid = 0, $vote = 0)
	{
		global $vbulletin, $imagerating, $picture, $vbphrase;

		require_once(DIR . '/includes/class_xml.php');

		if (!function_exists('fetch_phrase'))
		{
			require_once(DIR . '/includes/functions_misc.php');
		}

		$xml = new vB_AJAX_XML_Builder($vbulletin, 'text/xml');

		if (self::can_vote($profileid) && $vbulletin->options['_vb_pr_active'])
		{
			$time = time();
			if (self::has_voted($profileid))
			{
				$vbulletin->db->query_write("update " . TABLE_PREFIX . "picrate SET vote=$vote, dateline=$time WHERE profileid=$profileid and userid=$userid");
			}
			else
			{
				$vbulletin->db->query_write("insert into " . TABLE_PREFIX . "picrate (userid, profileid, vote, dateline) VALUES ($userid, $profileid, $vote, $time)");
			}

			self::rebuild_votes($profileid);
			$vbphrase['image_rating_x_votes_y_average'] = fetch_phrase('image_rating_x_votes_y_average', 'user');
			self::fetch_rating(true);

			$xml->add_group('imagerating');
			$xml->add_tag('rating', $imagerating);
			$xml->close_group();
			$xml->print_xml();
		}

		$vbphrase['vbprofilerating_error'] = fetch_phrase('vbprofilerating_error', 'user');

		$xml->add_group('imagerating');
		$xml->add_tag('error', $vbphrase['vbprofilerating_error']);
		$xml->close_group();
		$xml->print_xml();
	}

	function init_vboptimise()
	{
		global $vbulletin;

		if (!isset(self::$vboptimise))
		{
			require_once(DIR . '/includes/class_activecache.php');

			self::$vboptimise = vb_activecache::get_instance($vbulletin->options['_vboptimise_method']);
		}
	}

	function vboptimise_fetch_cache($profileid = 0)
	{
		self::init_vboptimise();

		$ratings = self::$vboptimise->fetch('imageratings_' . $profileid);

		if ($ratings)
		{
			$ratings = unserialize($ratings);

			foreach ($ratings as $rating)
			{
				self::$votedcache[$profileid . '_' . $rating['userid']] = true;
			}

			devdebug('vB Optimise: Fetched profile ratings cache.');
		}
		else
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vboptimise_save_cache($profileid = 0)
	{
		global $vbulletin;

		self::init_vboptimise();

		$cache = array();

		$ratings = $vbulletin->db->query_read("select userid from " . TABLE_PREFIX . "picrate where profileid=$profileid");
		while ($rating = $vbulletin->db->fetch_array($ratings))
		{
			$cache[] = $rating;
		}
		$vbulletin->db->free_result($ratings);

		self::$vboptimise->set('imageratings_' . $profileid, serialize($cache));

		devdebug('vB Optimise: Set profile ratings cache.');
	}
}
?>