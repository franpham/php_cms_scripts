<?php

define('THIS_TABLE', 'local_');
class vb_filerating
{
	static $votedcache = array();
	static $vboptimise;

	function fetch_rating($profileajax = false)
	{
		global $vbulletin, $stylevar, $vbphrase, $filerating, $fileinfo, $header;

		if ($vbulletin->options['_vb_pr_active'])
		{
			$stars = 0;
			$starbits = '';
			$fileinfo['vbb_rating'] = 0;
			$fileinfo['vbb_voteavg'] = '0.00';

			if ($fileinfo['vbb_votetotal'] > 0 && $fileinfo['vbb_votenum'] > 0)
			{
				$fileinfo['vbb_voteavg'] = vb_number_format($fileinfo['vbb_votetotal'] / $fileinfo['vbb_votenum'], 2);
				$fileinfo['vbb_rating'] = intval(round($fileinfo['vbb_votetotal'] / $fileinfo['vbb_votenum']));
			}

			while ($stars++ < 5)
			{
				$padding = $stars > 0;
				$type = $stars > $fileinfo['vbb_rating'] ? '_blank' : '';
				eval('$starbits .= "' . fetch_template('rating_ratestar') . '";');
			}

			$vote = $profileajax ? false : self::can_vote($fileinfo['linkid']);

			eval('$filerating = "' . fetch_template('rating_filerating') . '";');
			if ($vote && !$profileajax) {
				eval('$header .=  "' . fetch_template('rating_fileratemenu') . '";');
			}
		}
	}

	function can_vote($profileid = 0)
	{
		global $vbulletin;
		if ($profileid <= 0 || $vbulletin->uerinfo['usergroupid'] == 8)
			return false;
		elseif ($vbulletin->userinfo['usergroupid'] == 3 || $vbulletin->userinfo['usergroupid'] == 4)
			return false;						// disable voting for banned groups;
		elseif ($vbulletin->userinfo['userid'] == 0)
			return false;						// disable voting for guests;
		elseif ($vbulletin->userinfo['userid'] != $profileid || $vbulletin->options['_vb_pr_voteself'])
			return $vbulletin->options['_vb_pr_changevote'] || !self::has_voted($profileid);
		else									// not allowed to vote for one's entry;
			return false;
	}

	function has_voted($profileid = 0)
	{
		global $vbulletin;

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_fetch_cache($profileid);
		}

		if (isset(self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']]))
		{
			return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
		}

		$voted = $vbulletin->db->query_first("select vote from " . THIS_TABLE . "filerate where profileid=$profileid and userid=" . $vbulletin->userinfo['userid']);

		self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']] = $voted['vote'] ? true : false;

		return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
	}

	function rebuild_votes($profileid = 0)
	{
		global $vbulletin, $fileinfo;

		$vbb_votetotal = 0;
		$vbb_votenum = 0;

		$votes = $vbulletin->db->query_read("select vote from " . THIS_TABLE . "filerate where profileid=$profileid");
		while ($vote = $vbulletin->db->fetch_array($votes))
		{
			$vbb_votetotal += $vote['vote'];
			$vbb_votenum++;
		}
		$vbulletin->db->free_result($votes);

		$fileinfo['vbb_votetotal'] = $vbb_votetotal;
		$fileinfo['vbb_votenum'] = $vbb_votenum;

		$vbulletin->db->query_write("UPDATE " . THIS_TABLE . "linkslink SET vbb_votetotal=$vbb_votetotal, vbb_votenum=$vbb_votenum where linkid=$profileid");

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vote($userid = 0, $profileid = 0, $vote = 0)
	{
		global $vbulletin, $filerating, $fileinfo, $vbphrase;

		require_once(DIR . '/includes/class_xml.php');

		if (!function_exists('fetch_phrase'))
		{
			require_once(DIR . '/includes/functions_misc.php');
		}

		$xml = new vB_AJAX_XML_Builder($vbulletin, 'text/xml');

		if (self::can_vote($profileid) && $vbulletin->options['_vb_pr_active'])
		{
			$time = time();
			if (self::has_voted($profileid))
			{
				$vbulletin->db->query_write("update " . THIS_TABLE . "filerate SET vote=$vote, dateline=$time WHERE profileid=$profileid and userid=$userid");
			}
			else
			{
				$vbulletin->db->query_write("insert into " . THIS_TABLE . "filerate (userid, profileid, vote, dateline) VALUES ($userid, $profileid, $vote, $time)");
			}

			self::rebuild_votes($profileid);
			self::process_top_rated();

			$vbphrase['file_rating_x_votes_y_average'] = fetch_phrase('file_rating_x_votes_y_average', 'user');

			self::fetch_rating(true);

			$xml->add_group('filerating');
			$xml->add_tag('rating', $filerating);
			$xml->close_group();
			$xml->print_xml();
		}

		$vbphrase['vbprofilerating_error'] = fetch_phrase('vbprofilerating_error', 'user');

		$xml->add_group('filerating');
		$xml->add_tag('error', $vbphrase['vbprofilerating_error']);
		$xml->close_group();
		$xml->print_xml();
	}

	function process_top_rated()
	{
		global $vbulletin;
		$isAve = $vbulletin->options['_vb_pr_toprated_calc'] == 'ave';
		$isCount = $vbulletin->options['_vb_pr_toprated_calc'] == 'voted';
		$field = $isAve ? 'vbb_average' : ($isCount ? 'linkslink.vbb_votenum' : 'linkslink.vbb_votetotal');

		$top = $vbulletin->db->query_first("select user.username, user.userid, linkslink.linkid, " . 
		($isAve ? "IF (linkslink.vbb_votenum = 0, 0, linkslink.vbb_votetotal / linkslink.vbb_votenum) AS $field, " : "$field, ") . "
		IF (user.displaygroupid=0, user.usergroupid, user.displaygroupid) AS displaygroupid FROM " . 
		THIS_TABLE . "linkslink AS linkslink LEFT JOIN " . TABLE_PREFIX . "user AS user ON (user.userid = linkslink.linkuserid) 
		order by $field desc, " . ($isCount ? 'linkslink.vbb_votetotal' : 'linkslink.vbb_votenum') . 
		(($isAve or $isCount) ? ' desc ' : ' asc ') . 'limit 1');
		// if sorting by vote counts, 2nd sort field is vote totals, descending order;
		// else 2nd sort field is vote counts, but descending order for averages and ascending order for totals (smaller value gives higher average);

		if (($isAve ? $top[$field] : $top[substr($field, strpos($field, '.') + 1)]) > 0)
		{
			$top = fetch_musername($top) . "|$top[linkid]|{$vbulletin->options['_vb_pr_toprated_calc']}";
			$toprated = $top;
		}
		else
		{
			$toprated = 'none';
		}

		if ($toprated != $vbulletin->options['_vb_file_toprated'])
		{
			require_once(DIR . '/includes/adminfunctions.php');
			require_once(DIR . '/includes/adminfunctions_options.php');

			save_settings(array(
				'_vb_file_toprated' => $toprated,
			));
		}

		$vbulletin->options['_vb_file_toprated'] = $toprated;
	}

	function top_rated()
	{
		global $vbulletin, $template_hook, $vbphrase;

		if (!$vbulletin->options['_vb_pr_toprated_enable'])
		{
			return false;
		}

		if (trim($vbulletin->options['_vb_file_toprated']) == '')
		{
			self::process_top_rated();
		}

		if ($vbulletin->options['_vb_file_toprated'] != 'none')
		{
			$top = explode('|', $vbulletin->options['_vb_file_toprated']);

			if ($top[2] != $vbulletin->options['_vb_pr_toprated_calc'])
			{
				self::process_top_rated();
			}

			eval('$template_hook[forumhome_wgo_stats] .= "' . fetch_template('rating_topratedfile') . '";');
		}
	}

	function init_vboptimise()
	{
		global $vbulletin;

		if (!isset(self::$vboptimise))
		{
			require_once(DIR . '/includes/class_activecache.php');

			self::$vboptimise = vb_activecache::get_instance($vbulletin->options['_vboptimise_method']);
		}
	}

	function vboptimise_fetch_cache($profileid = 0)
	{
		self::init_vboptimise();

		$ratings = self::$vboptimise->fetch('fileratings_' . $profileid);

		if ($ratings)
		{
			$ratings = unserialize($ratings);

			foreach ($ratings as $rating)
			{
				self::$votedcache[$profileid . '_' . $rating['userid']] = true;
			}

			devdebug('vB Optimise: Fetched profile ratings cache.');
		}
		else
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vboptimise_save_cache($profileid = 0)
	{
		global $vbulletin;

		self::init_vboptimise();

		$cache = array();

		$ratings = $vbulletin->db->query_read("select userid from " . THIS_TABLE . "filerate where profileid=$profileid");
		while ($rating = $vbulletin->db->fetch_array($ratings))
		{
			$cache[] = $rating;
		}
		$vbulletin->db->free_result($ratings);

		self::$vboptimise->set('fileratings_' . $profileid, serialize($cache));

		devdebug('vB Optimise: Set profile ratings cache.');
	}
}
?>