
_vB_Profile_Rating = function()
{
	this.ajax;
	this.ratetype;

	this.handle_form = function(form, rtype)
	{
		vote = 0;
		this.ratetype = rtype;

		for (i = 0; i < form.vote.length; i++)
		{
			if (form.vote[i].checked)
			{
				vote = form.vote[i].value;
				break;
			}
		}

		this.ajax = YAHOO.util.Connect.asyncRequest("POST", "ajax.php?do=" + this.ratetype, {
			success: this.handle_ajax_request,
			failure: vBulletin_AJAX_Error_Handler,
			timeout: vB_Default_Timeout,
			scope: this
		}, SESSIONURL + "securitytoken=" + SECURITYTOKEN + "&do=" + this.ratetype + "&profileid=" + form.profileid.value + "&vote=" + vote);

		return false;
	}

	this.handle_ajax_request = function(ajax)
	{
		if (ajax.responseXML)
		{
			if (vBmenu.activemenu == this.ratetype)
			{
				vBmenu.hide();
			}

			if (ajax.responseXML.getElementsByTagName("error")[0])
			{
				alert(ajax.responseXML.getElementsByTagName("error")[0].firstChild.nodeValue);
			}
			else
			{
				fetch_object('profilerating_stars').innerHTML = ajax.responseXML.getElementsByTagName("rating")[0].firstChild.nodeValue;
				alert('Your vote has been saved.');
			}
		}
	}
}

vB_Profile_Rating = new _vB_Profile_Rating();