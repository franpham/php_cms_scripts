<?php
/****
 * vB Profile Rating
 * Copyright 2008; Deceptor
 * All Rights Reserved
 * Code may not be copied, in whole or part without written permission
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

class vb_profilerating
{
	static $votedcache = array();
	static $vboptimise;

	function fetch_rating($profileajax = false)
	{
		global $vbulletin, $profilerating, $vbphrase, $userinfo, $stylevar, $vote;

		if ($vbulletin->options['_vb_pr_active'])
		{
			$stars = 0;
			$starbits = '';
			$userinfo['rating'] = 0;
			$userinfo['voteavg'] = '0.00';

			if ($userinfo['votetotal'] > 0 && $userinfo['votenum'] > 0)
			{
				$userinfo['voteavg'] = vb_number_format($userinfo['votetotal'] / $userinfo['votenum'], 2);
				$userinfo['rating'] = intval(round($userinfo['votetotal'] / $userinfo['votenum']));
			}

			while ($stars++ < 5)
			{
				$padding = $stars > 0;
				$type = $stars > $userinfo['rating'] ? '_blank' : '';
				eval('$starbits .= "' . fetch_template('memberinfo_profilerating_star') . '";');
			}

			$vote = $profileajax ? false : self::can_vote($userinfo['userid']);

			eval('$profilerating = "' . fetch_template('memberinfo_profilerating') . '";');
		}
	}

	function can_vote($profileid = 0)
	{
		global $vbulletin;

		if ($vbulletin->userinfo['userid'] > 0 && ($vbulletin->userinfo['userid'] != $profileid || $vbulletin->options['_vb_pr_voteself']))
		{
			if (self::allow_change($profileid))
			{
				return true;
			}
		}

		return false;
	}

	function allow_change($profileid)
	{
		global $vbulletin;

		return !self::has_voted($profileid) || (self::has_voted($profileid) && $vbulletin->options['_vb_pr_changevote']);
	}

	function has_voted($profileid = 0)
	{
		global $vbulletin;

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_fetch_cache($profileid);
		}

		if (isset(self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']]))
		{
			return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
		}

		$voted = $vbulletin->db->query_first("select userrateid from " . TABLE_PREFIX . "userrate where profileid=$profileid and userid=" . $vbulletin->userinfo['userid']);

		self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']] = $voted['userrateid'] ? true : false;

		return self::$votedcache[$profileid . '_' . $vbulletin->userinfo['userid']];
	}

	function rebuild_votes($profileid = 0)
	{
		global $vbulletin, $userinfo;

		$votetotal = 0;
		$votenum = 0;

		$votes = $vbulletin->db->query_read("select vote from " . TABLE_PREFIX . "userrate where profileid=$profileid");
		while ($vote = $vbulletin->db->fetch_array($votes))
		{
			$votetotal += $vote['vote'];
			$votenum++;
		}

		$userinfo['votetotal'] = $votetotal;
		$userinfo['votenum'] = $votenum;

		$vbulletin->db->query_write("update " . TABLE_PREFIX . "user set votetotal=$votetotal, votenum=$votenum where userid=$profileid");

		if ($vbulletin->options['_vb_pr_optimise'])
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vote($userid = 0, $profileid = 0, $vote = 0)
	{
		global $vbulletin, $profilerating, $userinfo, $vbphrase;

		require_once(DIR . '/includes/class_xml.php');

		if (!function_exists('fetch_phrase'))
		{
			require_once(DIR . '/includes/functions_misc.php');
		}

		$xml = new vB_AJAX_XML_Builder($vbulletin, 'text/xml');

		if (self::can_vote($profileid) && $vbulletin->options['_vb_pr_active'])
		{
			if (self::has_voted($profileid))
			{
				$vbulletin->db->query_write("update " . TABLE_PREFIX . "userrate set vote=$vote where profileid=$profileid and userid=$userid");
			}
			else
			{
				$vbulletin->db->query_write("insert into " . TABLE_PREFIX . "userrate (userid, profileid, vote) values ($userid, $profileid, $vote)");
			}

			self::rebuild_votes($profileid);
			self::process_top_rated();

			$vbphrase['profile_rating_x_votes_y_average'] = fetch_phrase('profile_rating_x_votes_y_average', 'user');

			self::fetch_rating(true);

			$xml->add_group('profilerating');
			$xml->add_tag('rating', $profilerating);
			$xml->close_group();
			$xml->print_xml();
		}

		$vbphrase['vbprofilerating_error'] = fetch_phrase('vbprofilerating_error', 'user');

		$xml->add_group('profilerating');
		$xml->add_tag('error', $vbphrase['vbprofilerating_error']);
		$xml->close_group();
		$xml->print_xml();
	}

	function process_top_rated()
	{
		global $vbulletin;

		$field = $vbulletin->options['_vb_pr_toprated_calc'] == 'total' ? 'votetotal' : 'votenum';

		$top = $vbulletin->db->query_first("select username, userid, $field, IF(displaygroupid=0, usergroupid, displaygroupid) AS displaygroupid from " . TABLE_PREFIX . "user order by $field desc limit 1");

		if ($top[$field] > 0)
		{
			$top = fetch_musername($top) . "|$top[userid]|{$vbulletin->options['_vb_pr_toprated_calc']}|{$vbulletin->options['_vb_pr_toprated_calc']}";

			$toprated = $top;
		}
		else
		{
			$toprated = 'none';
		}

		if ($toprated != $vbulletin->options['_vb_pr_toprated'])
		{
			require_once(DIR . '/includes/adminfunctions.php');
			require_once(DIR . '/includes/adminfunctions_options.php');

			save_settings(array(
				'_vb_pr_toprated' => $toprated,
			));
		}

		$vbulletin->options['_vb_pr_toprated'] = $toprated;
	}

	function top_rated()
	{
		global $vbulletin, $template_hook, $vbphrase;

		if (!$vbulletin->options['_vb_pr_toprated_enable'])
		{
			return false;
		}

		if (trim($vbulletin->options['_vb_pr_toprated']) == '')
		{
			self::process_top_rated();
		}

		if ($vbulletin->options['_vb_pr_toprated'] != 'none')
		{
			$top = explode('|', $vbulletin->options['_vb_pr_toprated']);

			if ($top[3] != $vbulletin->options['_vb_pr_toprated_calc'])
			{
				self::process_top_rated();
			}

			eval('$template_hook[forumhome_wgo_stats] = "' . fetch_template('forumhome_toprateduser') . '";');
		}
	}

	function init_vboptimise()
	{
		global $vbulletin;

		if (!isset(self::$vboptimise))
		{
			require_once(DIR . '/includes/class_activecache.php');

			self::$vboptimise = vb_activecache::get_instance($vbulletin->options['_vboptimise_method']);
		}
	}

	function vboptimise_fetch_cache($profileid = 0)
	{
		self::init_vboptimise();

		$ratings = self::$vboptimise->fetch('profileratings_' . $profileid);

		if (!($ratings == false && !$ratings = self::$vboptimise->fetch('profileratings_' . $profileid)))
		{
			$ratings = unserialize($ratings);

			foreach ($ratings as $rating)
			{
				self::$votedcache[$profileid . '_' . $rating['userid']] = true;
			}

			devdebug('vB Optimise: Fetched profile ratings cache.');
		}
		else
		{
			self::vboptimise_save_cache($profileid);
		}
	}

	function vboptimise_save_cache($profileid = 0)
	{
		global $vbulletin;

		self::init_vboptimise();

		$cache = array();

		$ratings = $vbulletin->db->query_read("select userid from " . TABLE_PREFIX . "userrate where profileid=$profileid");
		while ($rating = $vbulletin->db->fetch_array($ratings))
		{
			$cache[] = $rating;
		}

		self::$vboptimise->set('profileratings_' . $profileid, serialize($cache));

		devdebug('vB Optimise: Set profile ratings cache.');
	}
}
?>