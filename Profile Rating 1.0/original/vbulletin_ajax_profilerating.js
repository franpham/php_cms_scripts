/****
 * vB Profile Rating
 * Copyright 2008; Deceptor
 * All Rights Reserved
 * Code may not be copied, in whole or part without written permission
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

_vB_Profile_Rating = function()
{
	this.ajax;

	this.handle_form = function(form)
	{
		vote = 0;

		for (i = 0; i < form.vote.length; i++)
		{
			if (form.vote[i].checked)
			{
				vote = form.vote[i].value;
				break;
			}
		}

		this.ajax = YAHOO.util.Connect.asyncRequest("POST", "ajax.php?do=profilerating", {
			success: this.handle_ajax_request,
			failure: vBulletin_AJAX_Error_Handler,
			timeout: vB_Default_Timeout,
			scope: this
		}, SESSIONURL + "securitytoken=" + SECURITYTOKEN + "&do=profilerating&profileid=" + form.profileid.value + "&vote=" + vote);

		return false;
	}

	this.handle_ajax_request = function(ajax)
	{
		if (ajax.responseXML)
		{
			if (vBmenu.activemenu == 'profilerating')
			{
				vBmenu.hide();
			}

			if (ajax.responseXML.getElementsByTagName("error")[0])
			{
				alert(ajax.responseXML.getElementsByTagName("error")[0].firstChild.nodeValue);
			}
			else
			{
				fetch_object('profilerating_stars').innerHTML = ajax.responseXML.getElementsByTagName("rating")[0].firstChild.nodeValue;

				img = fetch_tags(fetch_object('profilerating_stars'), 'img');

				for (i = 0; i < img.length; i++)
				{
					img[i].title = img[i].alt;
				}
			}
		}
	}
}

vB_Profile_Rating = new _vB_Profile_Rating();