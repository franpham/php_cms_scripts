<?php

error_reporting(E_ALL & ~E_NOTICE);
define('THIS_SCRIPT', 'activitystats');
define('CSRF_PROTECTION', true);

// ################### PRE-CACHE TEMPLATES AND DATA ######################
// get special phrase groups
$phrasegroups = array('global', 'user');

// get special data templates from the datastore
$specialtemplates = array('smiliecache', 'bbcodecache');

// pre-cache templates used by all actions
$globaltemplates = array('bbcode_code', 'bbcode_quote', 'useract_activities', 'useract_userbit');

// pre-cache templates used by specific actions
$actiontemplates = array();

require_once('./global.php');
// ####################### END OF SET PHP ENVIRONMENT ###########################

$perpage = $vbulletin->input->clean_gpc('r', 'perpage', TYPE_UINT);
$pagenumber = $vbulletin->input->clean_gpc('r', 'pagenumber', TYPE_UINT);
$sortfield = $vbulletin->input->clean_gpc('r', 'sort', TYPE_STR);

$userscount = $db->query_first_slave("
	SELECT COUNT(*) AS users
	FROM " . TABLE_PREFIX . "user AS user
	WHERE usergroupid NOT IN (" . $vbulletin->options['useract_noactivity_groups'] . ') ');
$totalusers = $userscount['users'];

sanitize_pageresults($totalusers, $pagenumber, $perpage, 100, 20);		// call before setting $limitlower;
$limitlower = ($pagenumber - 1) * $perpage + 1;
$limitupper = ($pagenumber) * $perpage;
if ($limitupper > $totalusers) {
	$limitupper = $totalusers;
	if ($limitlower > $totalusers)
		$limitlower = $totalusers - $perpage;
}
if ($limitlower <= 0)
	$limitlower = 1;

// clean_gpc, get $totalusers, set $limitlower, and call sanitize_pageresults BEFORE including useractivity.php;
require_once(DIR . '/includes/cron/useractivity.php');

$query = "SELECT userid, field11 AS location, IF(field1 != '', field1, field12) AS occupation FROM " .TABLE_PREFIX. "userfield WHERE userid IN ($uids)";
$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userid = $user['userid'];
	$users[$userid]['location'] = $user['location'];
	$users[$userid]['occupation'] = $user['occupation'];
}
$vbulletin->db->free_result($userquery);

$activitybits = '';
foreach ($users as $user) {
	$user['relactivity'] = round(($user['totalactivity'] / $vbulletin->options['useract_topactivity']) * 100);
	$user['gifactivity'] = round($user['relactivity'] / 10);
	$user['posts'] = vb_number_format($user['posts']);
	$user['profilevisits'] = vb_number_format($user['profilevisits']);
	$user['musername'] = fetch_musername($user);
	$user['altactivity'] = construct_phrase($vbphrase['useract_altactivity'], $user['relactivity'], $user['totalactivity']);
	exec_switch_bg();
	eval('$activitybits .= "' . fetch_template('useract_userbit') . '";');
}

$pagenav = construct_page_nav($pagenumber, $perpage, $totalusers, 'activitystats.php?' . $vbulletin->session->vars['sessionurl'], ''
	. (!empty($vbulletin->GPC['perpage']) ? "&amp;pp=$perpage" : '') . (!empty($sortfield) ? "&amp;sort=$sortfield" : ''));

eval('$navbar = "' . fetch_template('navbar') . '";');
eval('print_output("' . fetch_template('useract_activities') . '");');

// 1.	Calculate activity daily by cron using album pics, group members, events, ldm entries, threads, (user: posts, friendcount, profilevisits, nominations, awards); save 3 activity types: total, user, && site; do not use reputation since viewing permissions complicate query;

?>