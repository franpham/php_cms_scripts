<?php

global $vbphrase, $perpage, $limitlower, $sortfield;				// MUST globalize in case if running cron manually, which executes in exec_cron() function;
error_reporting(E_ALL & ~E_NOTICE);
if (!is_object($vbulletin->db)) {
	exit;
}
require_once(DIR . '/includes/local_links_init.php');				// required for THIS_TABLE CONSTANT;

// profile visits is chosen as a criteria to encourage users to add profile content; also it doesn't require extra SQL processing (ie: group message requires SUM SQL);
$sortsql = $sortfield == 'nominations' ? 'nomination_count' : ($sortfield == 'awards' ? 'nomination_awards' : ($sortfield == 'friends' ? 'friendcount' : 'totalactivity'));

$users = array();
$userids = array();
$lastupdate = (int) $vbulletin->options['useract_lastupdate'];		// only main usergroups are checked;
$query = "SELECT * FROM " .TABLE_PREFIX. "user WHERE usergroupid NOT IN (" . $vbulletin->options['useract_noactivity_groups'] . ')' . 
		 (THIS_SCRIPT == 'activitystats' ? (($sortsql ? " ORDER BY $sortsql DESC " : '') . " LIMIT " . ($limitlower - 1) . ", $perpage ") : " AND (activitydate > $lastupdate OR lastvisit > $lastupdate)");
// update activities only for users whose last activity was AFTER the last update or login was AFTER the last update;

$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userids[] = $userid = $user['userid'];							// must set 'userid' in array;
	$users[$userid] = array('userid' => $userid);
	$users[$userid]['posts'] = (int) $user['posts'];
	$users[$userid]['friendcount'] = (int) $user['friendcount'];
	$users[$userid]['profilevisits'] = (int) $user['profilevisits'];
	$users[$userid]['nomination_count'] = (int) $user['nomination_count'];
	$users[$userid]['nomination_awards'] = (int) $user['nomination_awards'];
	$users[$userid]['oldactivity'] = $user['totalactivity'];
	
	if (THIS_SCRIPT == 'activitystats') {							// get last saved stats && musername fields;
		$users[$userid]['totalactivity'] = $user['totalactivity'];
		$users[$userid]['username'] = $user['username'];
		$users[$userid]['usergroupid']  = $user['usergroupid'];
		$users[$userid]['displaygroupid'] = $user['displaygroupid'];
		$users[$userid]['infractiongroupid'] = $user['infractiongroupid'];
	}
}
$vbulletin->db->free_result($userquery);

$uids = THIS_SCRIPT == 'activitystats' ? implode(',', $userids) : $vbulletin->options['useract_noactivity_groups'];
$query = "SELECT user.userid, SUM(album.visible) AS pictures FROM " .TABLE_PREFIX. "user AS user
	LEFT JOIN " .TABLE_PREFIX. "album AS album ON (album.userid = user.userid AND album.state = 'public') 
	WHERE " . (THIS_SCRIPT == 'activitystats' ? " user.userid IN ($uids)" : " user.usergroupid NOT IN ($uids) AND (activitydate > $lastupdate OR lastvisit > $lastupdate)") . " GROUP BY user.userid";

$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userid = $user['userid'];
	if (!isset($users[$userid]))
		continue;
	$users[$userid]['pictures'] = (int) $user['pictures'];
}
$vbulletin->db->free_result($userquery);

$query = "SELECT user.userid, SUM(socialgroup.members) AS members FROM " .TABLE_PREFIX. "user AS user
	LEFT JOIN " .TABLE_PREFIX. "socialgroup AS socialgroup ON (socialgroup.creatoruserid = user.userid AND socialgroup.type != 'inviteonly')
	WHERE " . (THIS_SCRIPT == 'activitystats' ? " user.userid IN ($uids)" : " user.usergroupid NOT IN ($uids) AND (activitydate > $lastupdate OR lastvisit > $lastupdate)") . " GROUP BY user.userid";

$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userid = $user['userid'];
	if (!isset($users[$userid]))
		continue;
	$users[$userid]['members'] = (int) $user['members'];
}
$vbulletin->db->free_result($userquery);

$query = "SELECT user.userid, COUNT(event.eventid) AS events FROM " .TABLE_PREFIX. "user AS user
	LEFT JOIN " .TABLE_PREFIX. "event AS event ON (event.userid = user.userid AND event.groupid != 0 AND event.visible = 1)
	WHERE " . (THIS_SCRIPT == 'activitystats' ? " user.userid IN ($uids)" : " user.usergroupid NOT IN ($uids) AND (activitydate > $lastupdate OR lastvisit > $lastupdate)") . " GROUP BY user.userid";

$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userid = $user['userid'];
	if (!isset($users[$userid]))
		continue;
	$users[$userid]['events'] = (int) $user['events'];
}
$vbulletin->db->free_result($userquery);

$query = "SELECT user.userid, COUNT(link.linkid) AS links FROM " .TABLE_PREFIX. "user AS user
	LEFT JOIN " .THIS_TABLE. "linkslink AS link ON (link.linkuserid = user.userid AND link.linkstatus > 0)
	WHERE " . (THIS_SCRIPT == 'activitystats' ? " user.userid IN ($uids)" : " user.usergroupid NOT IN ($uids) AND (activitydate > $lastupdate OR lastvisit > $lastupdate)") . " GROUP BY user.userid";

$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userid = $user['userid'];
	if (!isset($users[$userid]))
		continue;
	$users[$userid]['links'] = (int) $user['links'];
}
$vbulletin->db->free_result($userquery);

$query = "SELECT user.userid, COUNT(thread.threadid) AS threads FROM " .TABLE_PREFIX. "user AS user
	LEFT JOIN " .TABLE_PREFIX. "thread AS thread ON (thread.postuserid = user.userid AND thread.open = 1 AND thread.visible = 1)
	WHERE " . (THIS_SCRIPT == 'activitystats' ? " user.userid IN ($uids)" : " user.usergroupid NOT IN ($uids) AND (activitydate > $lastupdate OR lastvisit > $lastupdate)") . " GROUP BY user.userid";

$userquery = $vbulletin->db->query_read($query);
while ($user = $vbulletin->db->fetch_array($userquery)) {
	$userid = $user['userid'];
	if (!isset($users[$userid]))
		continue;
	$users[$userid]['threads'] = (int) $user['threads'];
}
$vbulletin->db->free_result($userquery);

$topuserid = 0;
foreach ($users as $user) {
	$userid = $user['userid'];					// need to set $users for 'activitystats' page and $user for cron calculations;
	$users[$userid]['posts_val'] = $user['posts_val'] = $user['posts'] * $vbulletin->options['useract_postsweight'];
	$users[$userid]['friendcount_val'] = $user['friendcount_val'] = $user['friendcount'] * $vbulletin->options['useract_friendsweight'];
	$users[$userid]['profilevisits_val'] = $user['profilevisits_val'] = $user['profilevisits'] * $vbulletin->options['useract_visitsweight'];
	$users[$userid]['nomination_count_val'] = $user['nomination_count_val'] = $user['nomination_count'] * $vbulletin->options['useract_nominationsweight'];
	$users[$userid]['nomination_awards_val'] = $user['nomination_awards_val'] = $user['nomination_awards'] * $vbulletin->options['useract_awardsweight'];
	$users[$userid]['members_val'] = $user['members_val'] = $user['members'] * $vbulletin->options['useract_membersweight'];
	$users[$userid]['pictures_val'] = $user['pictures_val'] = $user['pictures'] * $vbulletin->options['useract_picsweight'];
	$users[$userid]['events_val'] = $user['events_val'] = $user['events'] * $vbulletin->options['useract_eventsweight'];
	$users[$userid]['links_val'] = $user['links_val'] = $user['links'] * $vbulletin->options['useract_linksweight'];
	$users[$userid]['threads_val'] = $user['threads_val'] = $user['threads'] * $vbulletin->options['useract_threadsweight'];
	
	if (THIS_SCRIPT != 'activitystats') {		// DO NOT recalculate so that values in 'activitystats' are consistent with other pages;
		$users[$userid]['useractivity'] = $user['friendcount_val'] + $user['profilevisits_val'] + $user['nomination_count_val'] + $user['nomination_awards_val'] + $user['members_val'];
		$users[$userid]['siteactivity'] = $user['posts_val'] + $user['pictures_val'] + $user['events_val'] + $user['links_val'] + $user['threads_val'];
		$users[$userid]['totalactivity'] = $users[$userid]['useractivity'] + $users[$userid]['siteactivity'];
		if ($users[$userid]['totalactivity'] > $users[$topuserid]['totalactivity'])
			$topuserid = $userid;
	}
}

if (THIS_SCRIPT != 'activitystats') {
	foreach ($users as $user) {
		if ($user['totalactivity'] != $user['oldactivity'])
			$vbulletin->db->query_write("UPDATE " .TABLE_PREFIX. "user SET totalactivity = '$user[totalactivity]' WHERE userid = '$user[userid]'");
	}
	require_once(DIR . '/includes/adminfunctions.php');
	require_once(DIR . '/includes/adminfunctions_options.php');
	$useract_settings = array('useract_lastupdate' => TIMENOW);
	$topactivity = round($users[$topuserid]['totalactivity']);
	if ($topactivity > $vbulletin->options['useract_topactivity'])		// MUST CHECK in case no users were updated;
		$useract_settings['useract_topactivity'] = $topactivity;
	save_settings($useract_settings);
}

// 1.	Calculate activity daily by cron using album pics, group members, events, ldm entries, threads, (user: posts, friendcount, profilevisits, nominations, awards); save 3 activity types: total, user, && site; do not use reputation since viewing permissions complicate query;

?>